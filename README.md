# Published Documents

This repository is contains the latex source (or links to the source code) for various public documents I've written and published over the years. It wraps them into a build scipt to make sure that the tex still compiles and the tex build envrioment is documented and under revision control. Compiling and distribution is the primary utility of this repository, along with keeping a copy of the source code under my control and in a central location where my day to day operations are unlikely to mess things up too much.

Documents in this repository are supposed to be the copy of record. Documents that have full revision histories are stored elsewhere.

# Bibtex


