#!/usr/bin/perl

$xfig ="/usr/X11R6/bin/fig2dev";

open(figures,"figures") or die "could not find figures file\n";

while(<figures>) {
    $_=~ m/(.*).fig/;
    print `$xfig -L eps $1.fig $1.eps`;
    print `$xfig -L pdf $1.fig $1.pdf`;
    print `$xfig -L png $1.fig $1.png`;
}

