This chapter uses the results of Chapter ~\ref{s:methods} to provide a
complete classification of monotone path graphs in corank 2. Following
the logical progression of Chapter ~\ref{s:crank1}, we begin with a
classification of pointed irreducible hyperplane arrangements using
affine Gale diagrams, identifying two infinite families of universally
all-coherent pointed irreducible hyperplane arrangements and giving
minimal obstructions for all other arrangements. For both universally
all-coherent families we will give combinatorial descriptions of the
galleries of $(\sA,c)$ and find functionals making every
$\f{}$-monotone path coherent, for every $\f{}$. Outside the two
infinite families, we use Lemma ~\ref{l:incoherent-lifting} to reduce
all monotone path graphs to a finite set of minimal
obstructions. Eight minimal obstructions are single-element extensions
of corank 1 hyperplane arrangements containing incoherent
$\f{}$-monotone paths. Applying Lemma ~\ref{l:coherent-projection},
these eight corank 2 hyperplane arrangements have incoherent
$\f{}$-monotone paths.  The remaining minimal obstruction is the
subject of Section ~\ref{ss:cr2-exceptional}; we carefully find an
incoherent $\f{}$-monotone path for every $\f{}$. We conclude this
chapter with our third main theorem, which is the classification of
monotone path graphs in corank 2 into universally all-coherent, and
containing an incoherent $\f{}$-monotone path for every $\f{}$. We
assume that all hyperplane arrangements in this section are
irreducible unless otherwise noted.

For an arrangement $\sA$ of $n=d+2$ hyperplanes in $\R{d}$, the dual
$\sAd$ consists of $d+2$ vectors in $\R{2}$. To highlight our
classification and guide our results we present Figure
~\ref{fig:cr2-minimal-obstruction} showing the universally
all-coherent families and the minimal obstructions in corank 2. We
focus on green affine Gale diagrams which correspond universally
all-coherent families. The purple affine Gale diagram is the special
case in Section ~\ref{ss:cr2-exceptional}. All remaining minimal
obstructions are red and illustrated in illustrate in Table
~\ref{tab:cr2-extensions}.

%\section{Classification} 
\begin{figure}
\includegraphics[scale=.5]{figures/g2_min_obstructions}%figures/g2-min-obstructions}
\caption{Classification of monotone paths in corank 2. Universally
all-coherent pointed hyperplane arrangements are green, minimal
obstructions are red, and  the exceptional case from Section
~\ref{ss:cr2-exceptional} is purple.}

\label{fig:cr2-minimal-obstruction}
\end{figure}


\section{First Universally All-Coherent Family}
\label{ss:cr2-zonotopal-1}
In corank 2, there are two analogs to the universally all-coherent family
of Section ~\ref{ss:cr1-zonotopal}. The first, which we
call \emph{type I}, consists of $n=d+2$ vectors in $\R{d}$ with $4$
parallelism classes in $\sAd$. We remark that we have to include the
cross-ratio to prove coherence, but will not need the
cross-ratio to enumerate the galleries of $(\sA,c)$. We illustrate the
dual configuration and affine Gale diagram in Figure ~\ref{fig:cr2-allcoh-type1}.

\begin{figure}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=1.5]
\draw[->,very thick] (0,0) -- (0,1);
\draw (0,1.2) node {$\aid{1}$};
\draw[->,very thick] (0,0) -- (-.7,-.7);
\draw (-.7,-.9) node {$\aid{2}$};
\draw[->,very thick] (0,0) -- (1,0);
\draw (1.25,0) node {$\aid{3}$};
\draw[->>,very thick] (0,0) -- (.7,-.7);
\draw (.7,-.9) node {$\aid{4},\ldots,\aid{n}$};
\end{tikzpicture}
%\includegraphics[scale=.6]{figures/cr2-allcoh-type1}
\end{minipage}
\begin{minipage}{.45\linewidth}
\centering
\includegraphics[scale=.5]{data/img/g2-class-12.png}
\end{minipage}
\caption{Type I universally all-coherent hyperplane arrangement in
corank 2; dual vectors and sample affine gale dual.}
\label{fig:cr2-allcoh-type1}
\end{figure}

We describe $\sAd$, including its
cross-ratio $\mu>0$, in
coordinates as
\[
\sA^*=
\bordermatrix{
& \aid{1} & \aid{2} & \aid{3} & \aid{4} & \cdots & \aid{n} \cr
& 0 & -1 & 1 & 1  &   1  & \cdots &  1   \cr
& 1 & -1 & 0 &-\mu & -\mu & \cdots & -\mu  \cr
}.
\]
%% This example has symmetry which we will use to count the galleries of $\sA$ from $\sAd$. 

\begin{lemma} When $(\sA,c)$ is type I, the number of galleries of $(\sA,c)$ is:
\[
\abs{\Gamma(\sA)}=2(d-1)!\binom{d+1}{3}.
\]
\label{l:cr2-allcoh-1-enum}
\end{lemma}

%% \begin{example} Before we prove Lemma ~\ref{l:cr2-all-coh-1-enum} we
%% look at a few of the galleries. 
%% \end{example}

\begin{proof}
When $(\sA,c)$ is of type I,  its galleries have a combinatorial
description based on three observations:
\begin{itemize}
\item The only possible orderings of $\setof{\aid{1},\aid{2},\aid{3}}$
  are $(\aid{3},\aid{2},\aid{1})$ and $(\aid{1},\aid{2},\aid{3})$.
\item There are no galleries $\gal$ with $\gal(1) \in \setof{1,2,3}$ and
$\gal(n) \in \setof{1,2,3}$. 
%% gallery which begins and ends with
%%   vectors from $\setof{\aid{1},\aid{2},\aid{3}}$.
%%   Any gallery with $\gal(1)=3$ must have
%%   $\gal(n) \in\setof{4,\ldots,n}$; any gallery with $\gal(n)=1$ must
%%   have $\gal{1} \in \in\setof{4,\ldots,n}$. 
\item The vectors $\setof{a_4^*,\ldots,a_{d+2}^*}$ are interchangeable.
\end{itemize}

The idea of is to build galleries based on these
observations using the following enumeration.
\begin{itemize}
\item Pick an ordering of $\setof{\aid{1},\aid{2},\aid{3}}$ in one 
      of $2$ possible ways. 
\item Choose locations for $\aid{1},\aid{2},\aid{3}$ in $\gal$ by
      picking $i,j,$ and $k$ to which for $\gal(i)=\ai{1},\gal(j)=\ai{2},$ and
      $\gal(k)=\ai{3}$. A fixed ordering of $\aid{1},\aid{2},\aid{3}$
      eliminates the first or last position of $\gal$, so $i,j,$
      and $k$ are elements of a $d+1$ set and we must choose $3$ elements.
\item The vectors $\setof{a_4^*,\ldots,a_{d+2}^*}$ are interchangeable
      and ordered in $(d-1)!$ ways. 
\end{itemize}

Taken together this gives 
\[
\abs{\Gamma(\sA,c)}=2(d-1)!\binom{d+1}{3}
\]
galleries of $(\sA,c)$. 
\end{proof}

To show that $(\sA,c)$ of type I is universally all-coherent, we must
show that $(\Z{\sA},\f{})$ is all-coherent for any $\f{}$ realizing
$(\sA,c)$. We start from a generic $\f{}$ and show that
$(\Z{\sA},\f{})$ is all-coherent by finding $\g{i}$ 
which select an arbitrary $\f{}$-monotone path and using Lemma
~\ref{l:coherent-strings} to extend coherence to any cellular string
of $(\Z{\sA},\f{})$. 

\begin{lemma} When $(\sA,c)$ is of type I, for any generic functional
$\f{}$ on $\Z{\sA}$ realizing $(\sA,c)$, and an $\f{}$-monotone path
$\gal$ we have that
\begin{itemize}
\item there are $a,b>0$ and a positive real partition
$b=\sum_{i=4}^nb_i$ so that the $\f{}$-valuation of $\sA$ is:
\[
\f{}^*=
\left(a+b(1+\mu),a+b,a,b_4,\ldots,b_n\right), \text{ and }
%\left(2+\mu,2,1,\frac{1}{d-1},\ldots,\frac{1}{d-1},\right)
\]
\item the $\f{}$-monotone path $\gal$ is coherent.
\end{itemize}
\label{l:cr2-zonotopal-1}
\end{lemma}

\begin{proof}
 We take $\f{}^*=(\f{1},\ldots,\f{n})$ to be a positive linear
 dependence on $\sAd$. Since $\setof{\aid{4},\ldots,\aid{n}}$ are
 parallel in $\sAd$ we can simplify this to a linear dependence of 
\[
\bordermatrix{
& \aid{1} & \aid{2} & \aid{3} & \aid{4} \cr
& 0 & -1 & 1 & 1    \cr
& 1 & -1 & 0 & -\mu \cr
}. 
\]
This matrix has a kernel spanned by
\[
\setof{
\begin{pmatrix}
1 \\
1 \\
1 \\
0 \\
\end{pmatrix}
,
\begin{pmatrix}
1+\mu \\
1 \\
0 \\
1 \\
\end{pmatrix}
}.
\]
Define $a=\f{3}>0$ and $b=\sum_{i=4}^n\f{i}>0$. The vector
$\left(a+b(1+\mu),a+b,a,b\right)$ is a
linear combination of kernel elements and therefore a linear
dependence. Further,   
$\f{}^*=\left(a+b(1+\mu),a+b,a,b,\f{4},\ldots,\f{n}\right)$ is the
same linear dependence with $b\aid{4}$ partitioned into
the parallel vectors $\setof{\aid{4},\ldots \aid{n}}$
and satisfies our first condition. 

We now find $\g{}$ by describing $\g{i}$ explicitly and checking that
$\g{i}$ are a linear dependence $\sum \g{i}\aid{i}=0$ of
$\sAd$. Mirroring our enumeration of the galleries we
begin by picking the ordering $(\aid{1},\aid{2},\aid{3})$ of 
$\setof{\aid{1},\aid{2},\aid{3}}$. We recall that $\gal(1)\ne 1$ so we
 assume that $\gal(1)=4$ by re-indexing. We set
\[
\g{1} = 1 \quad \g{2} = 1 \quad \g{3} = 1
\]
so that 
\[
\frac{\g{1}}{\f{1}} = \frac{1}{a+b+b\mu} <
\frac{\g{2}}{\f{2}} = \frac{1}{a+b} < 
\frac{\g{3}}{\f{3}} = \frac{1}{a}.
\]
We order $\setof{\g{5},\ldots,\g{n}}$ according to
$\gal$ and pick them so that 
\[
\begin{split}
& \frac{\gga{i}}{\fg{i}}<\frac{\gga{i+1}}{\fg{i+1}} \quad \text{ for } 5<i<n, \text{and}\\
&\frac{\abs{\g{5}}}{\f{5}}<\frac{\g{5}}{\f{5}}+\cdots+\frac{\g{n}}{\f{n}}
\end{split}
\] 
We set
$\frac{\g{4}}{\f{4}}=-\left(\frac{\g{5}}{\f{5}}+\cdots+\frac{\g{n}}{\f{n}}\right)$
so that $\g{4}<0<\frac{\g{1}}{\f{1}}$ and
$\sum_{i=1}^n \g{i}\aid{i}=0$. If we instead pick the ordering 
$(\aid{3},\aid{2},\aid{1})$ of $\setof{\aid{3},\aid{2},\aid{1}}$ we
make a symmetric argument by 
multiplying $\g{i}$ by $-1$. We conclude every $\f{}$-monotone path of
$(\sA,c)$ is coherent and therefore $(\sA,c)$
is all-coherent by Lemma ~\ref{l:coherent-strings} for every $\f{}$,
and $(\sA,c)$ is universally all-coherent. 
\end{proof}

\section{Second Universally All-Coherent Family}
\label{ss:cr2-zonotopal-2}

The second class of universally all-coherent hyperplane arrangements
in corank 2, which we
call \emph{type II}, consists of $d+2$ hyperplanes in $\R{d}$. We
characterize this arrangement by its dual, which 
has 3 parallelism classes in standard
position; we do not need to include a cross-ratio in our
computations. We illustrate $\sAd$ with 
Figure ~\ref{fig:cr2-allcoh-type2} and give it coordinates
\[
\sA^*=
\bordermatrix{
 & \aid{1} & \aid{2} & \aid{3} & \aid{4} & \cdots & \aid{n} \cr
 & 0 & -1 & 1 & 1 & \cdots & 1 \cr
 & 1 & -1 & 1 & 0 & \cdots & 0 \cr
}.
\]
\begin{figure}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=1.5]
\draw[->,very thick] (0,0) -- (0,1);
\draw (0,1.2) node {$\aid{1}$};
\draw[->,very thick] (0,0) -- (.7,.7);
\draw (.7,.9) node {$\aid{3}$};
\draw[->,very thick] (0,0) -- (-.7,-.7);
\draw (-.7,-.9) node {$\aid{2}$};
\draw[->>,very thick] (0,0) -- (1,0);
\draw (1.6,0) node {$\aid{4},\ldots,\aid{n}$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{.45\linewidth}
\centering
\includegraphics[scale=.6]{data/img/g2-class-9.png}
\end{minipage}%
\caption{Type II universally all-coherent hyperplane arrangement in
corank 2; dual vectors and sample affine gale dual.}
\label{fig:cr2-allcoh-type2}
\end{figure}

We use the same logical progression as Section
~\ref{ss:cr2-zonotopal-1}, first
enumerating the galleries combinatorially, then explicitly finding a
$\g{}$ which selects every gallery and using Lemma
~\ref{l:coherent-strings} to then claim that $\sA$ is universally all-coherent.

\begin{lemma} For $(\sA,c)$ of type II, the number of galleries of $(\sA,c)$ is 
\[
\abs{\Gamma(\sA,c)}=(d-1)!\left(\sum_{k=1}^{d-2}(k+2)(k+1)\right)
\]
\label{l:cr2-allcoh-2-enum}
\end{lemma}

\begin{proof} We begin with four observations.
\begin{enumerate}
\item The non parallel vectors $\setof{\aid{1},\aid{2},\aid{3}}$ can only be
  ordered in two ways, $(\aid{1},\aid{2},\aid{3})$ or
  $(\aid{3},\aid{2},\aid{1})$,
%% \item For a fixed position of $\ai{1}$ there are $2$ orderings of
%% $\aid{3}$ and $\aid{2}$. 
\item there is no gallery with $\gal(1)=1$ and symmetrically,
  no gallery with $\gal(n)=1$,
\item there is no gallery which begins with either 
$(\aid{1},\aid{2},\aid{3})$, or $(\aid{3},\aid{2},\aid{1})$, and
symmetrically no gallery which ends with $(\aid{1},\aid{2},\aid{3})$,
or $(\aid{3},\aid{2},\aid{1})$,
\item the parallel vectors $\setof{\aid{4},\ldots,\aid{d+2}}$ can be
 ordered arbitrarily.
\end{enumerate}

We begin by choosing one of the two orderings of
$\setof{\aid{1},\aid{2},\aid{3}}$ and assume that $\aid{1}$ is in
position $k$. For the ordering $(\aid{1},\aid{2},\aid{3})$ we know
$1 < k < d$ for a total of $d-2$ choices for $k$. Symmetrically, for
the ordering $(\aid{3},\aid{2},\aid{1})$ we know $3 < k < d+1$ for a
total of $d-2$ choices for $k$. Given any choice $k$
for the position $\aid{1}$, the remaining two non-parallel vectors are
inserted prior in any of the $\binom{k+2}{2}$ ways. We then pick any
ordering of the parallel vectors, $\setof{\aid{4},\ldots,\aid{d+2}}$,
in $(d-1)!$ possible ways. Each choice of $k$ gives a distinct
gallery, so we count the total number of galleries as
\[
\abs{\Gamma(\sA,c)}=(d-1)!2\sum_{k=1}^{d-2}\binom{k+2}{2}=(d-1)!\sum_{k=1}^{d-2}\frac{(k+2)!}{k!}=(d-1)!\sum_{k=1}^{d-2}(k+2)(k+1).
\]
\end{proof}

Next, we show $(\sA,c)$ is universally all-coherent by showing that 
$(\Z{\sA},\f{})$ is all-coherent for any $\f{}$ realizing
$(\sA,c)$. We start from a generic $\f{}$ and show that
$(\Z{\sA},\f{})$ is all-coherent by finding $\g{i}$ 
which select an arbitrary $\f{}$-monotone path and using Lemma
~\ref{l:coherent-strings} to extend coherence to any cellular string
of $(\Z{\sA},\f{})$. 

\begin{lemma} When $(\sA,c)$ is of type II and for any generic functional
$\f{}$ on $\Z{\sA}$ realizing 
$(\sA,c)$ and an $\f{}$-monotone path $\gal$ we have that
\begin{itemize}
\item there are $a,b>0$ and a positive real partition
$a=\sum_{i=4}^na_i$ so that the $\f{}$-valuation of $\sA$ is:
\[
\f{}^*=(a,a+b,a,a_4,\ldots,a_n), and 
\]
\item the $\f{}$-monotone path $\gal$ is coherent.
\end{itemize}
\label{l:cr2-zonotopal-2}
\end{lemma}

\begin{proof}  We take $\f{}^*=(\f{1},\ldots,\f{n})$ to be a positive linear
 dependence on $\sAd$. Since $\setof{\aid{4},\ldots,\aid{n}}$ are
 parallel in $\sAd$ we can simplify this to a linear dependence of 
\[
\bordermatrix{
 & \aid{1} & \aid{2} & \aid{3} & \aid{4} \cr
 & 0 & -1 & 1 & 1 \cr
 & 1 & -1 & 1 & 0 \cr
}. 
\]
This matrix has a kernel spanned by
\[
\setof{
\begin{pmatrix}
1 \\
1 \\
0 \\
1 \\
\end{pmatrix},
\begin{pmatrix}
0 \\
1 \\
1 \\
0 \\
\end{pmatrix}
}.
\]
Define $a=\f{3}>0$ and $b=\sum_{i=4}^n\f{i}>0$. The vector
$(a,a+b,a,a), $ is a
linear combination of kernel elements and therefore a linear
dependence. Further,   
$\f{}^*=(a,a+b,a,a_4,\ldots,a_n)$ is the
same linear dependence with $a\aid{4}$ partitioned into
the parallel vectors $\setof{\aid{4},\ldots \aid{n}}$
and satisfies our first condition. 

We now find a functional $\g{}$ selecting an arbitrary $\f{}$-monotone
$\gal$ by finding $\setof{\g{i}}$ and checking it is a linear
dependence. We mirror the enumeration of $\Gamma(\sA,c)$ and begin by
assuming $\setof{\aid{1},\aid{2},\aid{3}}$ are ordered as
$(\aid{1},\aid{2},\aid{3})$ of in $\gal$ and set
\[
\g{1}=0,\quad
\g{2}=1,\quad
\g{3}=1.
\]
So that 
\[
\begin{split}
&\frac{\g{1}}{\f{1}} = \frac{0}{a} < \frac{\g{2}}{\f{2}}=\frac{1}{a+b}
< \frac{\g{3}}{\f{3}} = \frac{1}{b}, \qquad \text{ and } \\
& \g{1}\aid{1}+\g{2}\aid{2}+\g{3}\aid{3}=0.
\end{split}
\]
We must have
\[
\sum_{i=4}^{n} \g{i}\aid{i}=0.
\]
Lemma ~\ref{l:cr2-allcoh-2-enum} guarantees $\gal(1)\ne 1$, so we assume $\gal(1)=4$. We find values for
$\g{i}$ with $i\ge 5$ just as in Lemma ~\ref{l:cr2-zonotopal-1}
with a $\gal$ ordering then pick $\g{4}$  so that 
\[
\g{4}+\cdots+\g{d+2}=0.
\]
Since $\aid{1}$ is never first, $\g{4}$ is
negative so $\setof{\g{i}}$ exist. For  the symmetric ordering of
$\setof{\aid{1},\aid{2},\aid{3}}$ we make a symmetric argument by
multiplying $\g{i}$ by $-1$. We conclude every $\f{}$-monotone path of
$(\sA,c)$ is coherent and therefore $(\Z{\sA},\f{})$
is all-coherent by Lemma ~\ref{l:coherent-strings} and $(\sA,c)$ is
universally all-coherent. 
\end{proof}

\section{Exceptional Cases}
\label{ss:cr2-exceptional}
The final arrangements we deal with are the minimal obstruction not
dealt with by Corollary ~\ref{c:coherent-projection}, which we call
the exceptional arrangements. These arrangements are drawn in Figures
~\ref{fig:cr2-exc-1} and ~\ref{fig:cr2-exc-2} and consist of
$6$ vectors in $\R{4}$ and are single-element lifting of the universally
all-coherent arrangements with $5$ vectors. The duals of
both arrangemetns are deformations of Example 
~\ref{ex:product-incoherent} and retain enough of the shuffle
structure to contain incoherent $\f{}$-monotone paths for every
$\f{}$. The exceptional arrangement $\sA$ has $4$ parallelism classes
so we must include the cross-ratio in our calculations. We begin by
describing the galleries of the arrangement in Figure
~\ref{fig:cr2-exc-2} and computing the incoherent$\f{}$-monotone
paths for various $\f{}$. Once we understand how 
$\f{}$ dictates which paths are coherent, we prove that there is
always an incoherent path. Our argument will be general enough to
apply to the both exceptional arrangements at once.

\begin{figure}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=1.5]
\draw[->,very thick] (0,0) -- (.7,-.7);
\draw (.8,-.8) node {$\aid{1}$};
\draw[->,very thick] (0,0) -- (0,.5);
\draw (.15,.55) node {$\aid{2}$};
\draw[->,very thick] (0,0) -- (0,1.0);
\draw (.15,1.05) node {$\aid{3}$};
%\draw (0,1.15) node {$\aid{2},\aid{3}$};
\draw[->,very thick] (0,0) -- (.3,0);
\draw (.3,.2) node {$\aid{4}$};
\draw[->,very thick] (0,0) -- (1.5,0);
\draw (1.5,.2) node {$\aid{5}$};
\draw[->,very thick] (0,0) -- (-1.8,0);
\draw (-1.8,.25) node {$\aid{6}$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{.45\linewidth}
\centering
\centering
\includegraphics[scale=.6]{data/img/g2-class-16.png}
\end{minipage}
\caption{First degenration of product case.}
\label{fig:cr2-exc-1}
\end{figure}

\begin{figure}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=1.5]
\draw[->,very thick] (0,0) -- (.7,-.7);
\draw (.8,-.8) node {$\aid{1}$};
\draw[->,very thick] (0,0) -- (0,.5);
\draw (.15,.55) node {$\aid{2}$};
\draw[->,very thick] (0,0) -- (0,1.0);
\draw (.15,1.05) node {$\aid{3}$};
%\draw (0,1.15) node {$\aid{2},\aid{3}$};
\draw[->,very thick] (0,0) -- (.3,0);
\draw (.3,.2) node {$\aid{4}$};
\draw[->,very thick] (0,0) -- (1.5,0);
\draw (1.5,.2) node {$\aid{5}$};
\draw[->,very thick] (0,0) -- (-1.8,.5);
\draw (-1.8,.7) node {$\aid{6}$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{.45\linewidth}
\centering
\centering
\includegraphics[scale=.6]{data/img/g2-class-15.png}
\end{minipage}
\caption{Second degenration of product case.}
\label{fig:cr2-exc-2}
\end{figure}

We eescribe $\sA$ using its dual $\sAd$, including a cross-ratio
$\mu\ge0$. Notice that when $\mu>0$ we have the dual arrangement of Figure
~\ref{fig:cr2-exc-2} and when $\mu=0$ we have Figure
~\ref{fig:cr2-exc-1}. In this way, we argue for both arrangements at
once. We incorporate a
generic $\f{}$ on $\Z{\sA}$ into our description of $\sAd$ so that for
any $\f{}$ we have $\f{}(\ai{i})=1$ by making $\sum_{i=1}^n \aid{i}=1$.
\[
\sAd=\bordermatrix{
 & \aid{1} & \aid{2} & \aid{3} & \aid{4} & \aid{5} & \aid{6} \cr
 & \f{1} &  0  & 0  & \f{4} & \f{5} & -\f{6} \cr
 & -\f{1} & \f{2} & \f{3} & 0  &  0  & \mu \f{6} \cr
}
\]
%\label{eq:cr2-exc-presentation}

We can describe any $\setof{\f{1},\ldots,\f{6}}$ using only four
parameters and we pick $\setof{\f{2},\f{3},\f{4},\f{5}}$.
From these parameters we compute $\f{1}$ and $\f{6}$ as
\[
\begin{split}
\f{1}=\frac{-1}{\mu-1}\left(\f{2}+\f{3}+\mu\f{4}+\mu\f{5}\right),\quad\text{and}
\f{6}=\frac{-1}{\mu-1}\left(\f{2}+\f{3}+\f{4}+\f{5}\right).
\end{split}
\]
and we must check that $\f{1},\f{6}>0$ and $(1,1,1,1,1,1)$ is a positive linear
dependence on $\sAd$. We know that if $\f{2},\f{3},\f{4},\f{5}>0$
then $\f{1}>0$ and $\f{6}>0$ and check $\sum_{i=1}^6 \aid{i}$: 
\[
\begin{split}
\aid{1}+\aid{6} &=
\begin{pmatrix}
\f{1}-\f{6} \\
-\f{1}+\mu \f{6}
\end{pmatrix} \\
&=
\begin{pmatrix}
\frac{-1}{\mu-1}\left(\f{2}+\f{3}+\mu\f{4}+\mu\f{5}-\f{2}-\f{3}-\f{4}-\f{5}\right)\\
\frac{-1}{\mu-1}\left(-\f{2}-\f{3}-\mu\f{4}-\mu\f{5}+\mu\f{2}+\mu\f{3}+\mu\f{4}+\mu\f{5}\right)\\
\end{pmatrix} \\
&=
\frac{-1}{\mu-1}\begin{pmatrix}
\mu\f{4}+\mu\f{5}-\f{4}-\f{5}\\
-\f{2}-\f{3}+\mu\f{2}+\mu\f{3}\\
\end{pmatrix} \\
&=
\frac{-1}{\mu-1}\begin{pmatrix}
\left(\mu-1\right)\left(\f{4}+\f{5}\right)\\
\left(\mu-1\right)\left(\f{2}+\f{3}\right)\\
\end{pmatrix} \\
&=
-\begin{pmatrix}
\f{4}+\f{5}\\
\f{2}+\f{3}\\
\end{pmatrix} = -\left(\aid{2}+\aid{3}+\aid{4}+\aid{5}\right).\\
\end{split}
\]

\begin{example} 
We compute the monotone path for a fixed $\mu$. The monotone path
graph contains $152$ galleries and has diameter $15$. We can also
compute the fiber polytope and check that
it has at most $148$ galleries so we expect at least $4$ incoherent
galleries for every $\f{}$. Remarkably, the cross-ratio does not
change coherence/incoherence for this configuration.

We use our computer to generate a list of incoherent
nodes for fixed $\mu$ and a variety of $\f{}$, which is summarized in
Table ~\ref{tab:cr2-exceptional-summary}. 
\end{example}
\input{table-cr2-exceptional-summary.tex}

We must show that there
is an incoherent path for every choice of
$\setof{\f{2},\f{3},\f{4},\f{5}}$. Based on Table
~\ref{tab:cr2-exceptional-summary} we guess that incoherent
galleries of $\sA$ will always start with one of
$\setof{\aid{4},\aid{5}}$, followed by one of
$\setof{\aid{2},\aid{3}}$, then followed by $\aid{1}$ and $\aid{6}$ in
order, the unused element of $\setof{\aid{4},\aid{5}}$, and
the final unaccounted for vector from $\setof{\aid{2},\aid{3}}$. We
write this using a regular expression.
\[
\gal=(\setof{\aid{4},\aid{5}},\setof{\aid{2},\aid{3}},\aid{1},\aid{6},
\setof{\aid{5},\aid{4}},\setof{\aid{3},\aid{2}}).
\]
In Table ~\ref{tab:cr2-exceptional-summary}, we see that there is no
path which is incoherent for every $f$ but, when we restrict our
attention to the functionals in which $\f{2}\le\f{3}$ and
$\f{4} \le\f{5}$, we suspect that $\gamma=521643$ is incoherent.

\begin{lemma} Suppose ($\Z{\sA}$,$\f{}$) is the exceptional pointed hyperplane
arrangement above with dual $\sAd$. The gallery $\gal=421653$ is coherent if and only if
\begin{equation}
\frac{\f{3}}{\f{5}} < \frac{\f{3}+\f{5}}{\f{4}+\f{5}} < \frac{\f{2}}{\f{4}}.
\label{eq:cr2-exc-ineq}
\end{equation}
\label{l:cr2-exc}
\end{lemma}

\begin{proof} Our proof is geometric in nature and clear with
adequate illustrations. All our diagrams rely on $\sum \aid{i}=0$. When
$\gal$ is coherent, we also know that $\sum \g{i}\aid{i}=0$. Our
diagrams will focus on $6$ vectors:
\[
\begin{array}{cc}
\begin{split}
v_{24}&=\aid{2}+\aid{4}\\
v_{16}&=-\aid{1}-\aid{6}\\
v_{35}&=\aid{4}+\aid{5}\\
\end{split}
&
\begin{split}
w_{24}&=\g{2}\aid{2}+\g{4}\aid{4}\\
w_{16}&=-\g{1}\aid{1}-\g{6}\aid{6}\\
w_{35}&=\g{4}\aid{4}+\g{5}\aid{5}.\\
\end{split}
\end{array}
\]
Our convention will be to draw
$v_{ij}$ in black, $w_{ij}$ in green, and both $v_{16}$ and $w_{16}$
as with dotted lines.  We a priori know that $v_{24}$ and $v_{35}$
will lay in the first quadrant and $v_{16}$ will lay in the third
quadrant. By using Lemma ~\ref{l:gallery-change-g} we can pick $\g{i}>0$
and know that $w_{24}$ and $w_{35}$ will also lay in the first
quadrant while $w_{16}$ will lay in the third quadrant. Our
presentation of $\sAd$ allows us to visualize information about
$\g{i}$ in the following way:
\begin{itemize}
\item  When $\g{4}<\g{2}$, $w_{24}$ has a larger slope than $v_{24}$.
\item  When $\g{5}<\g{3}$, $w_{35}$ has a larger slope than $v_{35}$.
\item  When $\g{1}<\g{6}$, $w_{16}$ has a smaller slope than $v_{35}$.
\end{itemize}
%% 

First we will check is that if Equation ~\ref{eq:cr2-exc-ineq} holds
then $\gal=421653$ is coherent. Figure ~\ref{fig:cr2-exc-coh} show
$v_{24},v_{16},v_{35}$ satisfying Equation ~\ref{eq:cr2-exc-ineq} .
We pick $\g{1}$ and $\g{6}$ so that $\abs{v_{16}}=\abs{w_{16}}$ and
$\g{2},\g{3},\g{4},\g{5}$ so that $\sum \g{i}\aid{i}=0$. Since the
solid green arrows are steeper than the solid black arrows,
$\g{4}<\g{2}$ and $\g{5}<\g{6}$, and the dotted green arrow is less
steep than the dotted black arrow, so we know $\g{1}<\g{6}$. We also
see that $\g{2}+\g{4}<1<\g{3}+\g{5}$. We conclude that $\gal=421653$
is coherent.

\begin{figure}
\begin{tikzpicture}[scale=8.0]
%\draw[->,very thick] (0,0) -- (-1,-1);
\draw[->,dotted, very thick] (0,0) -- (1,1);
\draw (.8,.75) node {$v_{16}$};
\draw[->,very thick] (0,0) -- (1/5,3/5);
\draw (.1,.4) node {$v_{24}$};
%\draw[] (.17,.3) node {$v_{24}$};
\draw[->,very thick] (1/5,3/5) -- (1,1);
\draw (.6,.85) node {$v_{35}$};
\draw[->,very thick,green] (0,0) -- (.05,.25);
\draw[green] (-0.025,.125) node {$w_{24}$};
\draw[->,very thick,green, dotted] (0,0) -- (1.09,.9);
\draw[green] (.58,.44) node {$-w_{16}$};
\draw[->,very thick,green] (.05,.25) -- (1.09,.9);
\draw[green] (.55,.6) node {$w_{35}$};
\end{tikzpicture}
\caption{$\gal=421653$ is coherent when $\frac{\f{3}}{\f{5}} < \frac{\f{3}+\f{5}}{\f{4}+\f{5}} < \frac{\f{2}}{\f{4}}$.}
\label{fig:cr2-exc-coh}
\end{figure}

In reverse, we must show that if Equation
~\ref{eq:cr2-exc-ineq} is not satisfied, then $\gal=421653$ is
incoherent. We first consider the case that
\[
\frac{\f{3}}{\f{5}} = \frac{\f{3}+\f{5}}{\f{4}+\f{5}} = \frac{\f{2}}{\f{4}}.
\]
%% Notice that this occurs when either half of Equation
%% ~\ref{eq:cr2-exc-ineq} degenerates to equality and the vectors
%% $v_{24}$, $v_{16}$, and $v_{35}$ are all parallel. Any choice of
%% $\g{1}\ldots\g{6}$ shifts $w_{24}$ $w_{35}$, up/left from the corresponding
%% $v_{ij}$, so there is no hope of $w_{24} +w_{16} + w_{35}$ equaling
%% $0$, thus $\gal=421653$ is incoherent.  

We now assume
\[
\frac{\f{3}}{\f{5}} \ge \frac{\f{3}+\f{5}}{\f{4}+\f{5}} \ge \frac{\f{2}}{\f{4}}.
\]
Figure ~\ref{fig:cr2-exc-incoh} guides our proof. Suppose there exist 
$\g{4}<\g{2}<\g{1}<\g{6}<\g{5}<\g{3}$ with $\sum \g{i}\aid{i}=0$, and
chosen so that $\abs{v_{16}}=\abs{w_{16}}$. Since $w_{35}$ cannot
point down, we must have
\[
\frac{\g{2}\f{2}}{\g{4}\f{4}}
> \frac{\g{2}\f{2}+\g{3}\f{3}}{\g{4}\f{4}+\g{5}\f{5}}.
\]
which contradicts Figure ~\ref{fig:cr2-exc-incoh}. To make 
$\sum \g{i}\aid{i}=0$ we have had to shrink
$w_{35}$ while stretching $w_{24}$. This in turn means that
$\g{2}+\g{4}>\g{3}+\g{5}$, contradicting our hypothesis on
$\setof{\g{i}}$. 

\begin{figure}
\begin{tikzpicture}[scale=8.0]
%\draw[->,very thick] (0,0) -- (-1,-1);
\draw[->,dotted, very thick] (0,0) -- (1,1);
\draw (.43,.5) node {$-v_{16}$};
\draw[->, very thick] (0,0) -- (.8,.4);
\draw (.4,.17) node {$v_{24}$};
%\draw (.56,.44) node {$-v_{16}$};
\draw[->,very thick] (.8,.4) -- (1,1);
\draw (1.01,.93) node {$v_{35}$};
%\draw (.17,.3) node {$v_{24}$};
%\draw[->,very thick] (.2,.6) -- (1,1);
\draw[->,very thick,green] (0,0) -- (1.05,.7);
\draw[green] (.54,.325) node {$w_{24}$};
\draw[->,very thick,green, dotted] (0,0) -- (1.09,.9);
\draw[green] (.65,.49) node {$-w_{16}$};
\draw[->,very thick,green] (1.05,.7) -- (1.09,.9);
\draw[green] (1.11,.8) node {$w_{35}$};
\end{tikzpicture}
\caption{$\gal=421653$ is incoherent when 
$\frac{\f{3}}{\f{5}} \ge \frac{\f{3}+\f{5}}{\f{4}+\f{5}} \ge \frac{\f{2}}{\f{4}}$.}
\label{fig:cr2-exc-incoh}
\end{figure}
\end{proof}

Lemma ~\ref{l:cr2-exc} gives a method of seeing which galleries in
$\sA$ are incoherent. In $\sA$, the ordering of
$\setof{\aid{2},\aid{3}}$ and the ordering of
$\setof{\aid{4},\aid{5}}$ were arbitrary so we can relabel vectors
and see which labeling make $421653$ incoherent. To make this easier,
we state this for an arbitrary gallery $\gal$ in the following corollary.
 
\begin{corollary} Suppose $\sAd$ is the dual configuration in Lemma
~\ref{l:cr2-exc} and $\gal$ is a gallery of the form
$\setof{4,5}\setof{2,3}16\setof{5,4}\setof{3,2}$, then
\[
\frac{\fg{6}}{\fg{5}} < 
\frac{\f{3}+\f{5}}{\f{4}+\f{5}} < 
\frac{\fg{2}}{\fg{1}}.
\]
\label{cor:cr2-exceptional-gen}
\end{corollary} 
\begin{proof}
Since both the pairs $\aid{2}$, $\aid{3}$ and $\aid{4}$, $\aid{5}$ are
parallel we can label them arbitrarily and after reordering, have
$\gamma=421653$ is coherent.
\end{proof}

This corollary explains the results of Table
~\ref{tab:cr2-exceptional-summary} and why $\gal=521643$ is
incoherent for every $\f{}$. When $\f{2}\le\f{3}$ and $\f{4}\le\f{5}$ we have:
\[
\frac{\f{2}}{\f{5}}\le\frac{\f{2}+\f{3}}{\f{4}+\f{5}}\le\frac{\f{3}}{\f{4}},
\]
so $\gal=521643$ is incoherent according to Corollary ~\ref{cor:cr2-exceptional-gen}. We have shown that $\sA$ has an
incoherent $\f{}$-monotone path for every $\f{}$ and are ready to give a
complete classification of universally all-coherent hyperplane
arrangements in corank 2.

\section{Corank 2 classification}

\input{table-cr2-min-extensions.tex}
We claim that every pointed hyperplane arrangement in corank 2 is
either one of our two universally all-coherent families or has an
incoherent $\f{}$-monotone path for every $\f{}$. Section
~\ref{ss:cr2-exceptional} gave a single minimal obstruction however
Figure ~\ref{fig:cr2-minimal-obstruction} has pointed hyperplane
arrangements which we claim have incoherent galleries. We recall that
deleting a vector from $\sA$ is equal to projecting parallel to a
vector in $\sAd$ ~\cite[Prop. 6.1]{Ziegler95} so we rely on Lemma
~\ref{l:coherent-projection} to prove that $\sA$ has incoherent
$\f{}$-monotone paths. Table ~\ref{tab:cr2-extensions} shows
each of the affine Gale duals, along with a representative dual vector
configuration with a single vector highlighted in red. Projecting
parallel to each of the red vectors results in a vector configuration
which contains an incoherent $\f{}$-monotone path $\gal$ according to
Lemma ~\ref{l:cr1-non-zonotopal} so each configuration in Table
~\ref{tab:cr2-extensions} has an incoherent gallery by Lemma
~\ref{l:coherent-projection}. We now can state the main theorem of
this Chapter.

\begin{theorem} In corank 2 there are exactly two infinite families of
universally all-coherent pointed hyperplane arrangements. All other
  corank 2 families have at least one incoherent $\f{}$-monotone path for every
  $\f{}$ and Figure   ~\ref{fig:cr2-minimal-obstruction} illustrates
  the minimal obstructions to universal all-coherence.
\label{thm:crank2-main-theorem}
\end{theorem}

\begin{proof} When $\sA$ is reducible,
  Corollary ~\ref{c:incoherent-product} describes precisely when
  $\sA=\sA_1 \bigsqcup \sA_2$ has
  incoherent galleries; $\sA$ has incoherent galleries when $\sA_1$
  and $\sA_2$ are both of corank $1$. When $\sA_1$ is of corank 2 and
  $\sA_2$ is of corank $0$, the disjoint union
  $\sA=\sA_1 \bigsqcup \sA_2$ has incoherent galleries when $\sA_1$
  has incoherent galleries, i.e., when $\sA_1$ is not of type I or type
  II.

When $\sA$ is an irreducible hyperplane arrangement, the
  two universally all-coherent infinite families are the subject of
  Lemma ~\ref{l:cr2-zonotopal-1} and Lemma
  ~\ref{l:cr2-zonotopal-2}. The minimal obstruction consisting of $6$
  vectors in $\R{4}$ contains an incoherent $\f{}$-monotone path for
  every $\f{}$ by Lemma ~\ref{l:cr2-exc}. The remaining hyperplane
  arrangements are single element extensions of the corank 1
  hyperplane arrangement of Section ~\ref{l:cr1-non-zonotopal}; These
  arrangements always contain an incoherent $\f{}$-monotone path by
  Lemma ~\ref{l:coherent-projection} and are illustrated in Table
  ~\ref{tab:cr2-extensions}.
\end{proof}


%This completes the  of this document and w
