We now make our discussion more geometric by switching from the
language of vector configurations to the language of hyperplane
arrangements. Hyperplane arrangements are reinterpretations of vector
configurations in which we consider each vector normal to a
hyperplane; any statement about the oriented matroid of a hyperplane
arrangement translates into a statement about configurations of normal
vectors. We will continue to work with vector configurations, they
are essential for our understanding of duality, however we focus on
hyperplane arrangements to better understand the geometry of oriented
matroids.

\begin{defn}
A \emph{hyperplane arrangement} $\sA\subset\R{d}$ is a set of $n$
hyperplanes in $\R{d}$. Each hyperplane has a
specified normal vector $\ai{i}$ and we will write $\sA$ as $d \times n$ matrix
in which each column $A=(\ai{i})$ is a normal vector to hyperplane $\sH_i$. For
our purposes, hyperplane arrangements will always be
\begin{itemize}
\item \emph{central} so that$0 \in \sH_i$ for all $i$,
\item \emph{essential} meaning $\bigcap_{\sH_i \in \sA} \sH_i =\setof{0}$, and
\item \emph{repetition free} meaning each hyperplane appears only once in $\sA$ that is, $\sH_i \ne
  \sH_j$ for $i \ne j$. 
\end{itemize}
\end{defn}

Hyperplane arrangements, as rephrasing of vector configurations, have
a covector structure ~\cite{Björner93}. The
covectors of $\sA$ are the covectors of the
underlying configuration of normal vectors. The dual of $\sA$ is the
dual of the underlying vector configuration of $\sA$ (the normal
vectors of $\sA$). 



The non-maximal covectors of $\mat{\sA}$ are similar. An intersection
of $k>1$ hyperplanes corresponds to a covector supported in exactly
$n-k$ positions constructed as sign vector with $0$ for all
hyperplanes in the intersection, and $+/-$ for all other hyperplanes,
depending on which half-space the intersection lives in. 

%corank 1
\begin{example} We now discuss a
  hyperplane arrangements of interest in
  dimension $3$. This arrangement illustrated
  single element liftings in example ~\ref{ex:matroid-lifting}. 
  The arrangement is of corank 1 and specified by
\[
\sA=
\begin{pmatrix}
1 & 0 & 0 & 1 \\
0 & 1 & 0 & 1 \\
0 & 0 & 1 & 1 \\
\end{pmatrix}
\]

Since visualizing subdivisions of $\R{3}$ is tricky, we will look at
how the hyperplanes of $\sA$ intersect a unit sphere, and visualize
the intersection under Stereographic projection. Figure
~\ref{fig:hyperplane-stereo} shows the sterographic
projection of $\sA$. We see the
chambers of $\sA$ are
\[
\mcC{}=\setof{
\begin{aligned}
++++,\quad%+++-,\quad
&++-+,\quad++--,\quad+-++,\quad\\
+-+-,\quad&+--+,\quad+---,\quad-+++,\quad\\
-++-,\quad&-+-+,\quad-+--,\quad--++,\quad\\
&--+-,\quad%---+,
----
\end{aligned}
}
\]
This example falls into our
corank 1 classification in Example ~\ref{ex:matroid-cr1-classification} and is
$d=3,k=1$. We see that $++++$ is a covector of
$\sA$ and $-+++$ is not be a covector of
$\sA$ using duality. The important observation in this example is that the
hyperplanes of $\sA$ determine the chambers, while
exactly which covector labels any given chamber depends on the choice
of the normal vector.
\label{ex:hyperplane-cd1}
\end{example}

\begin{figure}
\begin{tikzpicture}[scale=3.0]
\draw (-2,0) -- (2,0);
\draw (0,-2) -- (0,2);
\draw (0,0) circle (1);
\draw (-.7,-.7) circle (1.45 and 1.25);
%\draw [->] (1.5,0) -- (1.5,.25);
\draw (1.5,0.01) node {$\sH_{1}$};
\draw (0,1.5) node {$\sH_{2}$};
\draw (.72,.72) node {$\sH_{3}$};
\draw (-1.65,-1.65) node {$\sH_{4}$};
\draw (1.25,1.25) node[label] {$++++$};
\draw (-1.25,1.25) node[label] {$+-++$};
\draw (1.25,-1.15) node[label] {$-+++$};
\draw (.4,.5) node[label] {$++-+$};
\draw (-2,-2) node[label] {$--++$};
\draw (-1,-1) node[label] {$--+-$};
\draw (-.45,-.3) node[label] {$----$};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\draw (-1.3,.1) node[label] {$+-+-$};
\draw (-.35,.7) node[label] {$+--+$};
\draw (-.45,.1) node[label] {$+---$};
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\draw (.27,-1.10) node[label] {$-+-+$};
\draw (.27,-.5) node[label] {$-++-$};
\draw (.8,-.1) node[label] {$-+++$};
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\draw (.2,.1) node[label] {$++--$};
\end{tikzpicture}
\caption{Arrangement of $4$ hyperplanes in $\mathbb{R}^3$ with sign vectors labeling chambers}
%      \caption{Oriented matroid $\mat{\sA}$.}
\label{fig:hyperplane-stereo}
\end{figure}


Example ~\ref{ex:hyperplane-cd1} illustrates that we a choice in
picking normal vectors for $\sA$. So long as $\sA$ has an acyclic
orientation,  we can always choose $\ai{i}$ so that $(+)^n$ labels a
specified chamber $c$. The length of $\ai{i}$ is
irrelevant in specifying $\mat{\sA}$, so we will typically choose 
$\ai{i}$ so that
function $\f{}$ inducing an acyclic orientation on $\sA$ satisfies 
$\f{}(\ai{i})=\f{i}=1$.


Drawing hyperplanes via Stereographic projection as we did in Example
~\ref{ex:hyperplane-cd1} is useful for seeing the geometry of
hyperplane arrangements in $\R{3}$ but becomes cumbersome in
dimension $4$. Stereographic projection is dissatisfying because
it depends on the choice of the north pole. A different
choice of the north pole will produce a different looking
picture of the same hyperplane arrangement. We would prefer to
understand hyperplane arrangements using duality.

Affine Gale diagrams will, of course, tell us when two hyperplane
configurations have the same oriented matroid structure, the
generalized Baues problem provides another insight into understanding
and classifying $\sA$. The matrix $A$
specifies a linear surjection $A:\R{n} \to \R{d}$ and the image
$A(C_n)$ of the $n$-cube $[-1,+1]^n$ is the ~\emph{Zonotope} \cite{McMullen71}.

\begin{defn} Given a hyperplane arrangement $\sA$, written
  as a $d \times n$ matrix $A$, the \emph{zonotope} is the image of
  the $n$ cube under the projection specified by $A$. Equivalently,
  the zonotope is the Minkowski sum of vectors $\ai{i} \in \sA$.
\[
Z(\sA)=\sum_{i=1}^n \left[-\ai{i},\ai{i} \right].
\]
\label{def:hyperplane-zonotope}
An important special cases of the zonotope is the $1$-skeleton of
$\Z{\sA}$ which we refer to as $\gone{\sA}$.
\end{defn}

In geometric terms, vertices of $Z(\sA)$ are the chambers of
$\mat{\sA}$ and two vertices $c,c^\prime$ are adjacent when they are
separated by exactly $1$ hyperplane and the edges of $Z(\sA)$ are
covectors supported in exactly $n-1$ positions. The zonotope $Z(\sA)$
is a polytope and its oriented matroid structure is
equal to the oriented matroid structure of $\mat{\sA}$. The zonotope
$Z(\sA)$ depends on our choice of the vectors $\ai{i} \in \sA$,
however the zonotopes will still have isomorphic face lattices
~\cite{Ziegler95}.

\begin{lemma} Two hyperplane arrangements $\sA$ and $\sA^\prime$ have the same
  oriented matroid if and only if their zonotopes $Z(\sA)$ and $Z(\sA^\prime)$
  have the same face lattice.
\end{lemma}

To illustrate the geometric connection between zonotopes and oriented
matroids, we illustrate a classic result on the diameter of the
$1$-skeleton of the zonotope ~\cite{Reiner12}. 

\begin{example} The $1$-skeleton of $Z(\sA)$ is a graph $G_1(\sA)$
  whose vertices are chambers of $\sA$ and in which two chambers
  $c,c^\prime$ are adjacent if $\abs{\sep{c,c^\prime}}=1$. For any two
  chambers $c,c^\prime$ we know
  $d(c,c^\prime)=\abs{\sep{c,c^\prime}}$. Given any chamber $c$ we
  know from oriented matroid symmetry that $-c$ is
  also a chamber of $\sA$ and that $\sep{c,-c}=\setof{1,\ldots,n}$ so
  the diameter of $G_1(\sA)=n$.
\label{ex:hyperplane-diameter}
\end{example}

The paths on $Z(\sA)$ between
a distinguished chamber $c$ and its antipodal chamber $-c$ is
(finally!) the monotone paths which we  study in this
dissertation. Such paths are galleries (long paths that
visit chambers) or monotone paths and we are now ready to
discuss them in-depth.
 
