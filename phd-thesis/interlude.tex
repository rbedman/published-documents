This chapter serves as a brief interlude with three functions before
diving into our theoretical tools . First, it is a chance to
summarize the background material we have just completed. Second, it
serves as an entry point for readers with a background in oriented
matroids. Third, and most importantly, it is an opportunity focus on
the several specific problems and give a few new definitions which did
not fit into the background material.

Our presentation so far has moved from oriented matroids, to
vector configurations and hyperplane arrangements, and 
finally to zonotopes with a generic linear functional
$\f{}$. At each step along we have defined a gallery-like object, a
flip between gallery-like objects, and a graph whose vertices are gallery-like objects and edges are flips.

We now discuss the monotone path graph and $\gtwo{Z,v}$ in greater
detail. We first review the key definitions of galleries and
$\f{}$-monotone paths. We then focus on the 
monotone path graph and its distinguished subgraph of \emph{coherent}
$\f{}$-monotone paths and \emph{coherent} flips drawing attention to
the special cases of monotone path graphs in which every
$\f{}$-monotone path is coherent.  We reverse our
presentation and work inductively from the specific setting of generic
functionals on zonotopes to the general setting of acyclically oriented
matroids. We make several key definitions
including \emph{all-coherent}, \emph{universally all-coherent}, and
\emph{$L_2$-accessible} which will guide the rest of our work. 
All of our examples thus far have been rank $2$ to illustrate key
ideas and we now allow ourselves to discuss more examples in rank $3$
and above to illustrate more interesting behavior.

The three levels of generality we have worked with so far are
\begin{enumerate}
\item (Most Specific) The pair $(\Z{},\f{})$ is a \emph{zonotope with
a generic linear functional $\f{}$}. Equivalently, $\f{}$ is generic
 on $\Z{V}$ when $\f{}(v_i)>0$ for all $v_i \in V$ (or generic on
 $\sA$ when $\f{\ai{i}}>0$ for all $\ai{i} \in \sA$.

 An \emph{$\f{}$-monotone path} of $(\Z{},\f{})$ is a path from the
 $\f{}$-minimal vertex $-v$ to the $\f{}$-maximal vertex $v$ on the
 edges of $\Z{}$, which is $\f{}$-monotone in the sense of Definition
 ~\ref{def:gallery-1}. $\f{}$-monotone paths are coherent when
 selected by a functional $\g{} \in \Rd{d}$ (see Definition
 ~\ref{def:gallery-coherent}). The set of all $\f{}$-monotone paths
 are the vertices of the monotone path graph, whose edges are flips
 across faces of $\Z{}$.

\item (Intermediate) When $v=\sum v_i$ is the $\f{}$-maximal
 vertex of $\Z{}=\Z{V}$ the pair $(\Z{},v)$ is a \emph{pointed zonotope} (or,
 equivalently, for the chamber $c$ corresponding to $v$, a pointed
 hyperplane arrangement $(\sA,c)$) and a \emph{gallery} of $(\Z{},v)$ is a path from
 $-v$ to $v$ of minimal length (see Definition
 ~\ref{def:gallery-2}). Galleries of $(\Z{},v)$ correspond to
 $\f{}$-monotone paths of $(\Z{},f)$; when $(\Z{},\f{})$ realizes
 $(\Z{},v)$ a gallery of $(\Z{},v)$ is an $\f{}$-monotone path
 of $(\Z{},\f{})$. The galleries of $(\Z{},v)$ are
 the vertices of the graph $\gtwo{\Z{},v}$ and $\gtwo{\sA,c}$.

 At this level of generality we talk about the intersection lattice
 $L(\sA)=\bigsqcup L_k(\sA)$ of $\sA$. The elements $X$ of $\in L_k(\sA)$ are
 $k$-dimensional subspaces which are intersections of hyperplanes in
 $\sA$. Every corank $k$ covector of $\mat{\sA}$ gives rise to an
 element $X$ of $L_k(\sA)$, but an element $X$ of $L_k(\sA)$ does not uniquely
 determine a covector of $\mat{\sA}$. Flips between galleries are
 cellular strings $\cell{}$ with a distinguished non-trivial cell
 $\cell{k}$ of corank $2$, and all other cells of corank $1$. For ease
 of notation we will often refer to $X$, the element of $L_2(\sA)$
 rather than $\cell{}$.

\item (Most General) The face lattice of $\Z{V}$ defines the oriented
 matroid structure of $\mat{}=\mat{V}$ and the choice of $v$ gives
 $\mat{V}$ an acyclic orientation. We say that the pair
 $(\mat{},(+)^n)$ is an \emph{acyclically oriented matroid} and
 its \emph{galleries} and \emph{cellular strings} are defined in
 Definitions ~\ref{def:cellular-3} and ~\ref{def:gallery-3}. The
 galleries of $(\mat{V},(+)^n)$ are galleries of $(\Z{},v)$ when
 $(\Z{},v)$ realizes $(\mat{V},(+)^n)$. The galleries of
 $\gtwo{\mat{},(+)^n}$ are the vertices of the graph
 $\gtwo{\mat{},(+)^n}$ and two galleries are adjacent when they differ
 by cellular string with a single distinguished cell of corank $2$.  
\end{enumerate}

All of Chapter ~\ref{s:background} was devoted to defining the basic
objects of generic functions on zonotopes, pointed zonotopes and
pointed hyperplane arrangements, and acyclically oriented matroids. 
From these objects we defined galleries, monotone path graphs,
coherent $\f{}$-monotone paths, flips, cellular strings, etc... We add to these
definitions with two new definitions which apply to the pairs themselves.

\begin{defn} A zonotope with a generic linear
functional is \emph{all-coherent} if every cellular string of
$(\Z{},\f{})$ is \emph{coherent}.
\label{def:all-coherent} 
\end{defn}

We saw an all-coherent pair $(\Z{},\f{})$ in Example
~\ref{ex:zonotope-simple-1}. We will later prove Lemma
~\ref{l:coherent-strings} which says that every cellular
string will be coherent when every $\f{}$-monotone path is
coherent. If we prove that every $(\Z{V},\f{})$ which
realizes the pair $(\Z{},v)$ is all-coherent we will call $(\Z{},v)$
universally all coherent. 

\begin{defn} The pair $(Z,v)$, a pointed zonotope 
is \emph{universally all-coherent} if every gallery of $(\Z{},v)$ is a
coherent $\f{}$-monotone path for every $(\Z{},\f{})$ realizing
$(\Z{},v)$.
\label{def:universally-all-coherent}
\end{defn}

We have not discussed it in detail, however Example
~\ref{ex:zonotope-simple-1} is universally all-coherent because it is
of corank $2$. Lest we give the impression that all
pointed vector configurations are universally all-coherent we now give
our first example with \emph{incoherent} $\f{}$-monotone paths, which also serves
to illustrate the interplay of our $3$ levels of abstraction. 

%example
\begin{example} We consider $\sA$ to the corank
  $1$ hyperplane arrangement consisting of $4$ hyperplanes in $\R{3}$,
  in which $\sAd$ has exactly $2$ negative vectors. We specify $\sA$
  and $\sAd$ as
\[
\sA=
\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} \cr
& 1 & 0 & 0 &  1 \cr
& 0 & 1 & 0 &  1 \cr
& 0 & 0 & 1 & -1 \cr
},
\qquad
\sAd=
\bordermatrix{
& \aid{1} & \aid{2} & \aid{3} & \aid{4} \cr
& -1 & -1 & 1 & 1 \cr
}.
\]
The functional $f(x,y,z)=x+y+z$ is maximized at the point $(2,2,0)$
corresponding to the covector $++++$. We can check that $\gal=1423$ is
a gallery of $\sA$ using each definition independently.

To check that $\gal=1423$ is a gallery of the acyclically oriented
matroid $(\mat{\sA},(+)^n$ we use Definition ~\ref{def:gallery-3} and
find covectors $(0---,+--0,+0-+,++0+)$ of $\mat{\sA}$. While the set
$\mcC{} \subset \mcV{}$ of covectors is longer than we are
willing to list here, we can easily check that each element in the
sequence for $\gal^{(i)}$ is a covector using $\sAd$

To check that $\gal=1423$ is a gallery of $(\sA,c)$, we use Definition
~\ref{def:gallery-2} and simply notice that each chamber listed for
$\gal$ is a vertex of $Z(\sA)$, and we can check that they are
adjacent in $\Z{\sA}$ by using duality, checking that
$0---,+--0,+0-+,++0+$ are still covectors of $\sA$.
 
Finally, to check that $\gal=1423$ is a $\f{}$-monotone path of
$(\Z{},\f{})$ we use Definition ~\ref{def:gallery-1}. We give
coordinates to the vertices of $Z(\sA)$:
\[
\begin{split}
v_0 = ---- &= (-2,-2,0), \\
v_1 = +--- &= (0,-2,0), \\
v_2 = +--+ &= (2,0,-2), \\
v_3 = ++-+ &= (2,2,-2), \\
v_4 = ++++ &= (2,2,0).
\end{split}
\]
We evaluate $f$ on each of these vertices and find
$(f(v_i))=(-4,-2,0,2,4)$ so this is an $\f{}$-monotone path.

For $f(x,y,z)=x+y+z$ the $\f{}$-monotone path $1423$ is incoherent. Suppose that $g(x,y,z)=Ax+By+Cz$ selects $\gal$, then we would have
\[
A<A+B-C<B<C 
\]
but $B-C<0$ so $A+B-C<A$ which contradicts $A<A+B-C$. 
We will explore this example further in Section
~\ref{ss:cr1-non-zonotopal}. 

The function $f(x,y,z)=3x+2y+z$ also makes $(\Z{},\f{})$ realize
$(\Z{},v)$ since $f(\ai{i})>0$. The $\f{}$-monotone path $1423$ is coherent because the functional $\g{}(x,y,z)=-33x+34y+31z$ selects it in the sense that
\[
\frac{\g{1}}{\f{1}}=\frac{-33}{3} 
< \frac{\g{4}}{\f{4}}=\frac{-32}{4}
< \frac{\g{2}}{\f{2}}= \frac{34}{2} 
< \frac{\g{3}}{\f{3}} = \frac{31}{1}
\]
We can check computationally that $\setof{1324,2314, 4132, 4231}$
is a complete list of incoherent galleries for $f(x,y,z)=3x+2y+z$.  
The monotone path graph of $(\Z{},\f{})$ is shown in Figure
~\ref{fig:gallery-g2} with incoherent galleries drawn in red.

Curiously, $\gp{}(x,y,z)=-30x+36y+32z$ also make $\gal$ coherent since 
\[
\frac{\g{1}}{\f{1}}=\frac{-30}{3}
< \frac{\g{4}}{\f{4}}=\frac{-38}{4}
< \frac{\g{2}}{\f{2}}= \frac{36}{2}
< \frac{\g{3}}{\f{3}} = \frac{32}{1}. 
\]
\label{ex:gallery-coherent}
\end{example}

\begin{figure}
\centering
\input{figures/mpg-n4-k2-interlude}
%\includegraphics[width=.5\linewidth]{figures/mpg-n4-k2}
\caption{The monotone path graph for $f(x,y,z)=3x+2y+z$ with
incoherent $\f{}$-monotone paths in red.}        
\label{fig:gallery-g2}
\end{figure}

The choice of $\g{}$ in Example ~\ref{ex:gallery-coherent} is 
important. The $\g{}$ which selects $\gal$ is not unique and how
we pick $\g{i}$ will be fundamental to most of our arguments, so we
pause briefly to understand the flexibility we have in the choice of
$\setof{\g{i}}$.

\begin{lemma} Given $(\Z{\sA},\f{})$, a zonotope with a generic
functional $\f{}$ and an $\f{}$-monotone path $\gal$, if $\gp{1},\ldots,\gp{n}$ satisfy
\[
\begin{split}
  \frac{\gpga{1}}{\fg{1}} < 
  &\cdots < 
  \frac{\gpga{n}}{\fg{n}} 
   \mbox{ and } \\
  \sum \gp{i} & \aid{i} = 0\\
\end{split}
\]
then for any $B$ and any $0<A$,  $\g{i}=A\gp{i}+B\f{i}$
satisfies
\[
\begin{split}
  \frac{\gga{1}}{\fg{1}} < 
  &\cdots < 
  \frac{\gga{n}}{\fg{n}} 
   \mbox{ and } \\
  \sum \g{i} & \aid{i} = 0.\\
\end{split}
\]
\label{l:gallery-change-g}
\end{lemma}

\begin{proof} Suppose $\gp{i}$ satisfies
\[
\begin{split}
  \frac{\gpga{1}}{\fg{1}} < 
  &\cdots < 
  \frac{\gpga{n}}{\fg{n}} 
   \mbox{ and } \\
  \sum \gp{i} & \aid{i} = 0\\
\end{split}
\]
then we must check that
$\displaystyle \sum_{i=1}^n \left(A\gp{i}+B\f{i}\right)\aid{i}=0$. We compute this directly
\[
\begin{split}
\sum_{i=1}^n (A\gp{i}+B\f{i}) \aid{i} &= \sum_{i=1}^n A\gp{i}\aid{i}+\sum B\f{i}\aid{i}  \\
&= A \sum_{i=1}^n \gp{i}\aid{i}+B\sum_{i=1}^n \f{i}\aid{i}  \\
&= A\cdot 0+B\cdot 0 = 0.
\end{split}
\]
We must also check that $(A\gp{i}+ B\f{i})$ is $\gal$
monotone. We see that for any 
$\frac{\gp{i}}{\f{i}} < \frac{\gp{k}}{\f{k}}$ we have
\[
\begin{split}
\frac{\g{i}}{\f{i}} &=
\frac{A\gp{i}+B\f{i}}{\f{i}} =  \frac{A\gp{i}}{\f{i}} + B  \\
&<  \frac{A\gp{k}}{\f{k}} + B
= \frac{A\gp{k}+B\f{k}}{\f{k}}=\frac{\g{k}}{\f{k}}.
\end{split}
\]
\end{proof}

\begin{corollary} For any coherent $\f{}$-monotone path $\gal$ of
$(\Z{},\f{})$, we may pick $\setof{\g{i}}$ which realize the coherent of $\gal$ so that:
\begin{itemize}
\item for any bound $C$, $\g{i}<C$ for all $i$ (or, symmetrically
$C<\g{i}$), and
\item we may scale $\setof{\g{i}}$ so that $\abs{\g{i}-\g{j}}=\delta$ for specific
$i,j$ and any $\delta>0$.
\end{itemize}
\label{cor:pick-gs}
\end{corollary}

\begin{proof} Since $\gal$ is a coherent $\f{}$-monotone path we know there
must exist $\setof{\gp{i}}$ which satisfy
\[
\begin{split}
  \frac{\gpga{1}}{\fg{1}} < 
  &\cdots < 
  \frac{\gpga{n}}{\fg{n}} 
   \mbox{ and } \\
  \sum \gp{i} & \aid{i} = 0\\
\end{split}
\]
Given any $C$, we will use Lemma ~\ref{l:gallery-change-g} to pick $A$ and
$B$ making $\g{i}<C$ for all $i$. Pick $A=1$ and 
$B<\frac{C-\gp{i}}{\f{i}}$ for all $i$, define $\g{i}=\gp{i}+B\f{i}$
so that
\[
\g{i}=\gp{i}+B\f{i}<\gp{i}+\left(\frac{C-\gp{i}}{\f{i}}\right)\f{i} 
=\gp{i}+C-\gp{i}=C
\]

To scale we use Lemma ~\ref{l:gallery-change-g} with
$A=\delta/\abs{\g{i}-\g{j}}$ to make $\abs{\g{i}-\g{j}}=\delta$ for
any $\delta>0$.
\end{proof}

We now describe the subgraph of coherent $\f{}$-monotone paths in the
monotone path graph. These subgraphs are induced by all the coherent
galleries and equal to the $1$ skeleton of the \emph{fiber zonotope}
of the projection $f:Z(\sA) \to \R{}$. The fiber zonotope is a
particular fiber polytope ~\cite{Billera92} which generalizes the
secondary polytopes of ~\cite{Gelfand94}. Although we will not give
the full details of fiber zonotopes here, we will refer the reader to
Chapter 9 of ~\cite{Ziegler95} and ~\cite{Billera92}. We give an
explicit construction of the fiber zonotope restated in a manner which
will be useful to us ~\cite{Billera92}, ~\cite{Reiner12}.

\begin{defn} The \emph{fiber zonotope} of the projection
$\f{}:\Z{\sA} \to \R{}$ is the $n-d$ dimensional zonotope $\Z{\sAp}$
generated by the vectors
\[
\sAp=\setof{v_{ij}\,|\, \text{ where }\,v_{ij}=f(v_i)v_j-f(v_j)v_i}\text{ and } 1\le i < j \le n.
\]
\label{def:gallery-fiber-zonotope}
\end{defn}

The vertices of fiber zonotope correspond to coherent $\f{}$-monotone
paths and any cellular string on $\Z{\sAp}$ is a coherent cellular
string of $(\Z{},\f{})$. The $1$-skeleton of $\Z{\sAp}$ will be the subgraph of
the monotone path graph consisting of all coherent $\f{}$-monotone
paths.

\begin{example} We will continue Example ~\ref{ex:gallery-coherent}
in the context of the fiber zonotope. Recall that
$\sAd=\begin{pmatrix} -1 & -1 & 1 & 1 \end{pmatrix}$. For
$f(x,y,z)=3x+2y+z$ we find $\setof{v_{ij}}$. 
\[
\begin{array}{ccc}
v_{12}=\begin{pmatrix} -2 \\  3 \\ 0  \end{pmatrix} &
v_{13}=\begin{pmatrix} -1 \\  0 \\ 3  \end{pmatrix} &
v_{14}=\begin{pmatrix} -1 \\  3 \\ -3  \end{pmatrix} \\
v_{23}=\begin{pmatrix} 0  \\ -1 \\ 2  \end{pmatrix} &
v_{24}=\begin{pmatrix} 2  \\ -1 \\ -2  \end{pmatrix} &
v_{34}=\begin{pmatrix} 1  \\  1 \\ -5  \end{pmatrix} \\
\end{array}.
\]
When we construct the Zonotope of $\setof{v_{ij}}$ we obtain a
polytope whose $1$-skeleton we can embed in the monotone path graph of
$\sA$. We have already seen this monotone path graph in Example
~\ref{ex:gallery-coherent} and illustrated it Figure
~\ref{fig:gallery-g2} where the vertices corresponding to 
$Z{\sAp}$ are the black, coherent $\f{}$-monotone paths.
\label{ex:gallery-fiber-zonotope}
\end{example}

The major results of this dissertation are that, in coranks $1$ and
$2$,
\begin{itemize}
\item  $(Z,v)$ being universally all-coherent depends only on
$(\mat{},(+)^n$, and
\item when $(Z,v)$ is not universally all-coherent every $(Z,f)$
realizing $(Z,v)$ contains at least one incoherent gallery.
\end{itemize}

The final definitions of this Chapter are taken from ~\cite{Reiner12}
which we will use for our diameter computation in corank 1. 

\begin{defn} For a pointed hyperplane arrangement $(\sA,c)$,
 the $L_2$ separation set of galleries $\gal$ and $\galp$ is the set of
 elements $X \in L_2(\sA)$ in which $\gal$ and $\galp$ differ. That
 is, given a path $(\gal=\gal_0,\gal_1,\ldots,\gal_k=\galp)$ the
 separation set
\[
L_2(\gal,\galp)=\setof{X_i \big| \quad \gal_{i-1} \text{ and
} \gal_{i} \text{ are adjacent by the flip } X_i}
\]
Remarkably
$L_2(\gal, \galp)$ is well-defined and does not depend on the path in
$\gtwo{\sA,c}$ ~\cite[Example 3.3]{Reiner12}. It is important to note
that $X \in L_2(\sA)$ is the codimension 2 subspace of $\R{d}$
corresponding to the flip between $\gal_{i-1}$ and $\gal_{i}$
and \emph{not} a cellular string. The separation $L_2(\gal, \galp)$ is
only well-defined on $L(\sA)$, not
on $L(\mat{})$. 
\end{defn}

Its not hard to see that the graph distance
$d_{\gtwo{\sA,c}}(\gal,\galp)\ge L_2(\gal,\galp)$ for all
$\gal,\galp \in \Gamma(\sA,c)$. For a fixed gallery $\gal$, when the
standard graph distance $d(\gal,\galp)$ equals the $L_2$-separation
for every $\galp$, we say that $\gal$ is $L_2$-accessible.

\begin{defn} A gallery $\gal$ of $\gtwo{\sA,c}$
is \emph{$L_2$-accessible} when $d_G(\gal,\galp)=\abs{L_2(\gal,\galp)}$ for every $\galp \in \gtwo{\sA,c}$.
\end{defn}

The graph $\gtwo{\sA,c}$ is nicely symmetric with an involution from
$\gal$ to its reversal, so $L_2$-accessibility is the key for the
following proposition from ~\cite[Proposition 3.12]{Reiner12}. 

\begin{proposition} If $\gtwo{\sA,c}$ contains an
$L_2$-accessible vertex $\gal$, then the diameter of $\gtwo{\sA,c}$ is exactly $\abs{L_2(\sA)}$.
\end{proposition}

In the case of corank 1, Theorem ~\ref{l:cr1-existsL2} shows that a particular
gallery of $\gtwo{\sA,c}$ is $L_2$-accessible and proves that the
diameter of $\gtwo{\sA,c}$ equals $\abs{L_2}$. 

