%% This appendix is a roughly 4-5 page presentation of
%% the problems I've tackled in my thesis, for readers with a curious
%% mind but little formal mathematical background.
%%
%% Plan is to set up a few fun problems for people, which are clever
%% rewordings of problems from the thesis. The problems will be
%% concrete and solveable.
%% 
%% The general outline is this:
%% * Introduction
%% * The one-dimensional tiger problem
%%   - organize the fences
%%   - bribe the zookeeper
%% * A more mathematical approach
%%   - path in the zonotope
%%   - coherent, actually
%%   - dual statement
%% * Some related problems
%%   - linear optimization
%%   - 4 color theorem
%% * Actual results
%%   - k=2
%% * Solutions provided

%% My approach will be to reword a few of the problems we have solved in
%% this thesis, making them more accessible by making games out of them
%% and making them concrete by looking at specific instances. The
%% problems might feel a little funny, but they will hopefully be clear.  


\label{a:summary-everyone}
\epigraph{Die Mathematiker sind eine Art Franzosen; redet man zu ihnen, so übersetzen sie es in ihre Sprache, und dann ist es alsobald ganz etwas anders.\footnote{Mathematicians are [like] a sort of Frenchmen; if you
talk to them, they translate it into their own language, and then it
is immediately something quite different.}}{Johann Wolfgang von Goethe}

Welcome to the part of my thesis written for non-mathematicians. This section
is an attempt to answer the question ``what exactly is it you
study?''. A typical answer to this question drops some buzz words, includes a variety of tangentially related applications, but never
says much of anything. To me this misses the point in the same way
that ``I work in that building'' misses the point of the question
``what do you do''. A better answer allows non-mathematicians to
interact with the same problems I think about day-to-day, in a less
technical way, which is what this appendix attempts to do.

At the highest level I study mathematics (Duh!). The particular flavor
of math is combinatorics, which is the mathematical study of objects
which you can count. Within combinatorics I enjoy counting objects of
geometric or algebraic interest (e.g., counting solutions to an
equation). The topic of this thesis is coherent monotone paths,
specifically trying to answer the question ``which monotone paths are
coherent''. 

Already, we are dealing with technical objects without a formal
definition. Chapters~\ref{s:background} and ~\ref{s:interlude} of this
dissertation give meaning to monotone paths and coherent
galleries. Instead of asking you to read those 30 pages, I am going
pose some problems involving a tiger. The problems about tigers are
actual examples from this thesis; they will not look technical at
first, but that is because I have replaced the frightening technical
words like ``acyclic'', ``monotone'', and ``bijection'' with more
friendly words like ``tiger'' and ``fence''. These problems aren't
designed to trick anyone and if you make an honest attempt to solve the
problem, you'll have a good idea of the methods I used in this
thesis. 

\textbf{Acyclic tiger cages:}
Suppose you are a zoo keeper and your job is to keep a tiger from
escaping its cage. The Tigers live on a line and the cage is four
fences, two on either side of the tiger as illustrated in Figure
~\ref{fig:tiger}.

Occasionally, you must rotate the fences, which means you to take
every fence and move it to the opposite side of the tiger. The catch
is that you cannot let the tiger escape while you are rotating
fences. You can move any fence at any time, you can move each fence
only once, and you must move all of them.

\begin{figure}
\centering
\includegraphics[scale=.6]{figures/tiger-graphics}
\caption{The tiger problem}
\label{fig:tiger}
\end{figure}

This is not possible with only one fence on each side of the
tiger, but with our tiger cage has fences. Using the fence labeling in figure
~\ref{fig:tiger} you can move the fences in the
order $1423$ or $2431$, but you cannot move the fences in the
order $1234$; after moving the first two fences, your tiger
will escape out the left side of the cage, as illustrated
in \ref{fig:tiger}. We can now ask our first problem:

{\bf Counting Problem 1}: In how many ways can you move all the fences
exactly once without ever letting the tiger escape.

You should pause here, get out a pencil, and try this problem
for yourself. It is a fun problem, just try some rotations and see if
you notice any patterns. You can't count all the rotations  with just your
fingers, but if you use both fingers and toes you can get there.

A standard answer to this question is to create the decision
tree illustrated in Figure 
~\ref{fig:tiger-decision}. You can see $4$
choices for the first fence you move, $2$ choices for the second, $2$
choices for the 3rd fence, and only $1$ choice for the last
fence. This gives a total of $4*2*2=16$ possible ways to rotate the
fences. The tree is reasonably large so I
have drawn it using a shorthand. In our shorthand we
write $+$ or $-$ for each fence: We write $-$ if we still must move the
fence and $+$ if we have already moved the fence. This shorthand calls
the starting fence arrangement $----$ and the final fence arrangement
$++++$ and a fence rotation is a path between these, such as $1423$,
which we write in shorthand:
\[
---- \to +---- \to +--+ \to ++-+ \to ++++. 
\]
The tiger will escape if our shorthand is ever $--++$ or $++--$. 

Counting fence rotations is a problem
that I solved in this thesis. I called a fence rotations
a \emph{gallery} in Definition ~\ref{def:gallery-3}. The
counting problem you 
just solved was important enough that it was Lemma
~\ref{l:cr1-gallery-count}. 

Lemma ~\ref{l:cr1-gallery-count} says that if a tiger is surrounded by
$n$ fences with $k<n$ fences to his right, then the number of fence
rotations is
\[
n!-2(n-k)!k!.
\]
I encourage you to use $n=4$ and $k=2$ to check your answer to
Counting Problem 1. My solution is different from the decision tree; to count
the number of fence rotations, I count ways to let the tiger 
free and remove that from the all possible ways to rotate the fences.
Once you have done that you can use Lemma
~\ref{l:cr1-gallery-count} for 20 fences or 100
fences with ease. Which way do you think is easier?

\begin{figure}
\centering
%\includegraphics[width=.75\linewidth]{figures/decision-tree-n4-k2}
\input{figures/decision-tree-n4-k2}
\caption{The tiger decision tree}
\label{fig:tiger-decision}
\end{figure}

When we look at the decision tree more closely we see some
repetition. Some fence arrangements come up multiple times, for
instance the cage 
$+-+-$ is seen when we move fence $1$ followed by fence $3$ and seen
again when we move fence $3$ followed by fence $1$. The decision tree
was great for counting fence rotations but is does not
distinguish between fence arrangements when we get to the same
arrangement multiple ways. This is confusing! we want to understand
all fence arrangements which keep the tiger from escaping. By
redrawing the decision tree with each fence  
draw configuration drawn once, we have a more clear picture of ways to
keep the tiger from escaping. See Figure ~\ref{fig:tiger-tope}.

\begin{figure}
\centering
\input{figures/zonotope-n4-k2.tex}
%\includegraphics[width=.25\linewidth]{figures/zonotope-n4-k2}
\caption{The tiger-tope}
\label{fig:tiger-tope}
\end{figure}

This picture looks like a 3d shape and remarkably it \emph{is} a 3d
shape that you can think of as being a weird 12-sided die or a
stretched cube. Suddenly we are doing geometry while
talking about tigers in a zoo. This is so cool that we need to give it
a name, so we call it the \emph{Tiger tope}. In this setting, fence
rotations are paths from $----$ to $++++$ on the tiger tope.

We can now say how similar two fence rotations are
because they can partly overlap as paths on the tiger tope. We
can say that $1342$ ``is close to'' $1324$ because they are the same for the
first two fence moves and differ only by moving the last two fences in opposite
orders. We call this flipping two fences because we can move both
fences at once without letting the tiger escape.

If you look at all the possible fence rotations you found, you can arrange
them together and connect the fence arrangement which are flips
(like $1324$ and $1342$). This gives you a picture which looks like
Figure ~\ref{fig:tiger-graph} and which we call the fence
diagram. A fancier word for it is the monotone path graph (Definition
~\ref{def:gallery-1}) which are the geometric
objects that this dissertation is about. 

\begin{figure}
\centering
\input{figures/mpg-n4-k2.tex}
%\includegraphics[scale=.5]{figures/mpg-n4-k2}
\caption{The tiger fence diagram}
\label{fig:tiger-graph}
\end{figure}

Congratulations, you now understand the geometric half of this
thesis. The remaining half of the thesis is the question of
which monotone paths are ``coherent''. To explain coherence, we will
go back to our tiger and ask a second counting problem. After
understanding the solution you will be ready to understand the main
results of this thesis. 

\textbf{Coherent tigers:}
Lets start with the same tiger in the same cage and we still want to
rotate the fences without letting the tiger escape. Further
suppose that you cannot move the fences yourself (perhaps there was an
unfortunate tiger accident), but you can still ask people to move
the fences for you. You have interns who are all hanging out by
the fences to the left of the tiger and you have tiger lovers who are
all hanging out to the right of the tiger. To get the
interns and the tiger lovers to move fences, you must follow these
rules:

\begin{enumerate}
\item You must pay interns to move the fences from left to right.
\item Tiger lovers pay you for the opportunity to move fences from right to
left. 
\item As the fences move, the tiger gets more hungry and active, which
excites the tiger lovers and scares the interns. The result is that
for each fence moved, more money is exchanged (you must either pay
more to the interns or the tiger lovers will pay you more to move
fences for you). 
\end{enumerate}

{\bf Counting Problem 2} As a non-profit zoo, you can't make money
moving the fences, but you shouldn't lose money either. Which of
the $16$ fence rotations can convince the tiger lovers and interns to move
without losing or making money?

An example is in order here. We can convince tiger lovers and interns
to rotate the fences in the order $1342$ by paying an
intern $\$1$ to move fence $1$, then charge a tiger lover $\$2$ to
move fence $3$, then get another tiger lover to pay $\$3$ to
move fence $4$, and finally pay $\$4$ to have an intern move fence
$2$. We paid $\$5$ to interns and we got $\$5$ from tiger lovers. In
contrast there is no way to pay people to move the fences in the order
$1324$ since you will make money from moving $1$ followed by
$4$ and you will also make money from $2$ followed by $4$, so you are
doomed to make money for the zoo.

You should again pause here, get out your pencil and try this problem
yourself. Checking all $16$ fence rotations might get dull, but try a
few and see if you notice any patterns.

The fence rotations that balance tiger lovers and interns are
called \emph{coherent}. That is the same notion of coherence as in the
title of this dissertation and was Definition
~\ref{def:gallery-coherent}. The fence rotations which cannot be done
for free are \emph{incoherent}. Understanding which galleries are
coherent and which galleries are incoherent is the major question of
this thesis, and you now understand it! This dissertation
is a careful analysis of two generalizations of the tiger problem.

The first is to ask about more
fences or different arrangements of the fences. Both of these possibilities are
addressed in Chapter ~\ref{s:crank1} which presents 2 major results,
which we will paraphrase in the language of tigers and fences:
\begin{enumerate}
\item Theorem ~\ref{l:cr1-zonotopal}: If you have a tiger enclosed
by $n$ fences but only $1$ of them is to the left of the tiger, you
can always pay your tiger lovers and interns to rotate the fences for
you. That is, there are no incoherent fence rotations when $k=1$. 
\item Theorem ~\ref{l:cr1-non-zonotopal} When $k>1$ there will
always be a fence rotation which you cannot pay
tiger lovers and interns to do for you, without making or losing
money. The proof of this is particularly interesting, because it
uses a technical Lemma ~\ref{l:incoherent-lifting} to say
``If there is an incoherent fence rotation for $n$ fences I cannot
make every ordering coherent by adding more fences''. This
Lemma is hugely important because it lets us carefully solve bite
sized problems (like you just did!) then draw conclusions
about all other tiger cages.
~\ref{fig:cr1-classification}
\end{enumerate}

A second  way to generalize the tiger problems is to consider a
tiger who lives on a plane. The definitions of fence rotations, coherent
and incoherent become more involved here but we have analogous results
\begin{enumerate}
\item Lemma ~\ref{l:cr2-zonotopal-1} and Theorem
~\ref{l:cr2-zonotopal-2} give conditions making every
fence rotation coherent.
\item Lemma ~\ref{l:cr2-exc} shows that all other
configurations have at least 1 incoherent fence
rotation. Lemma ~\ref{l:incoherent-lifting} is again the
primary tool we use, so this again reduces down to
checking $9$ bite sized problems carefully. 
\end{enumerate}

If you are still reading, thank you for your patience. The question
which I should still answer 
is ``who cares''? I doubt that I will receive any calls from zoo
keepers asking how to rotate fences around their one-dimensional
tigers. Mathematicians are interested in this problem because when a
tiger cage has only coherent fence rotations the fence rotation graph
will look like a sphere (in $d$ dimensions). This may seem
abstract and impractical but I agree with 
De Loera, Santos, and Rambau who wrote: ``We firmly believe that
understanding the fundamentals of geometry and combinatorics pays up for
algorithms and applications.'' ~\cite{DeLoera10}. The algorithms and
applications they are referring to include:
\begin{itemize}
\item The simplex algorithm for optimization. Galleries are monotone
paths so we are describing the same objects used in linear
programming. Unfortunately our description does not help solve linear
programming problems but this illustrates that these are practical
objects. Interestingly, the Hirsch conjecture for linear programming
was solved by Santos ~\cite{Santos12} whom we consulted for Lemma
~\ref{l:coherent-strings}.
\item Triangulations, which are a natural ways to
decompose convex regions into smaller, bite-sized convex regions. Triangulations
like galleries, can be coherent or incoherent and are used in
everything from meshing for numerical methods and algebra problems to
computer graphics, or volume computations. 
\item Gr\"obner Bases, which are methods of working with multivariate
polynomials on computers. They are cutting edge math which uses
triangulations to replace polynomials with monomials and solve
nonlinear systems of equations. 
%\item Generalized Baues Problem and the Hirsch Conjecture. 
\end{itemize}
Finally, I think this is a beautiful topic full of cool math and that
makes it worth studying for its own sake. I have avoided talking about
applications because I wanted to give readers the chance to actually
do some math and appreciate its beauty for themselves.


%% The reason I like these problems is the classification into
%% always coherent and sometimes incoherent provides geometric insight
%% into the structure of monotone path graphs. When every fence rotation
%% is coherent, the monotone path graph becomes a polytope, just like the
%% tiger tope was. There are powerful optimization tools for computers
%% which rely on understanding which paths along polytopes which are the
%% maximize functions and our notion of \emph{coherent} is equal to
%% functionals maximized along a path. Last I hope that you now care, at
%% least slightly, if not because they will solve some practical problem
%% for you, but because they are fun problems that gave you a taste of
%% what research mathematics actually looks like instead of vague
%% analogies.

%% Finally, my answer to the question ``what do you study'' is ``I study
%% coherent rotations of tiger cages'' and now not only do you understand
%% what that means, but you've studied it too.

