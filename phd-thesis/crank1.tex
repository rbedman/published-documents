This chapter uses the results of Chapter ~\ref{s:methods} to give a
complete classification of all monotone path graphs in corank 1. We
begin by recalling the classification in Section ~\ref{ss:level3},
restricting our attention to pointed hyperplane arrangements $(\sA,c)$
and count the galleries of $(\sA,c)$ explicitly. From our
classification we identify a unique family of universally all-coherent
pointed irreducible hyperplane arrangements and find functionals to
select any $\f{}$-monotone path or cellular string of
$(\Z{\sA},f)$. For all other corank 1 hyperplane arrangements, we
describe the incoherent galleries explicitly for a minimal
obstruction. We then lift these incoherent galleries to all hyperplane
arrangements outside of our universally all-coherent family, using
Lemma ~\ref{l:incoherent-lifting}. After classifying pointed
irreducible hyperplane arrangements in corank 1, we prove Corollary
~\ref{c:incoherent-product} to say precisely when reducible hyperplane
arrangements have incoherent galleries. Galleries and flips of pointed
hyperplane arrangements are simple enough to understand in corank 1
that we show a specific gallery of $\gtwo{\sA,c}$ is $L_2$-accessible and prove the diameter of
$\gtwo{\sA,c}=\abs{L_2}$ for every corank 1 pointed hyperplane
arrangement. We assume that all hyperplane arrangements in this
section are irreducible unless otherwise noted.

\section{Combinatorial Model}
\label{ss:crank1-model}
Pointed hyperplane arrangements in corank $1$ are classified in
$\sAd\subset \R{1}$
by counting the number $k$ of negative vectors and irreducible when
they do not contain the zero vector. When $k=0$ $\mat{\sA}$
has no acyclic orientation so we discard it and focus only on $k>1$.
Our first step is to describe the number $\abs{\Gamma(\sA,c)}$ of
galleries. Table ~\ref{tab:crank1-gallery-counts} presents the number
of galleries for small $n$ and $k\le n/2$ which we use to check our
analytic 
results. We count galleries for any $k$ by looking at all permutations
of $n$ and removing those which do not correspond to galleries. When
$k=1$ we have a constructive description which we use to describe
functionals $\g{}$ selecting arbitrary $\f{}$-monotone paths. 

\begin{lemma} For a hyperplane arrangement $\sA$ of $n=d+1$ vectors in
$\R{d}$ with $k$ negative vectors in $\sAd$, the number of galleries $\abs{\Gamma(\sA,c)}$ is
\[
\abs{\Gamma(\sA,c)}=n!-2k!(n-k)!.
\]
\label{l:cr1-gallery-count}
\end{lemma}
\input{table-crank1-galleries.tex}

\begin{proof}
We begin by presenting $\sAd$ explicitly
\[
\sAd=
\bordermatrix{
& \aid{1} & \ldots & \aid{k} & \aid{k+1} & \cdots & \aid{n} \cr
& -1 & \ldots & -1 & 1 & \cdots & 1 \cr
},
\]
and count the number of galleries in $\sA$ by counting permutations 
of the $n$ dual vectors and removing the permutations  which are not paths
on $\Z{\sA}$. A permutation $\gal$ of $[n]$ is not a gallery
when the $\gal^{(i)}$ fails to capture the origin in $\sAd$. This happens when $\gal$ crosses all the negative
dual vectors $\setof{\ai{1}, \ldots, \ai{k}}$ followed by
all the positive dual vectors $\setof{\ai{k+1}, \ldots, \ai{n}}$, or
all the positive dual vectors followed by all the negative dual
vectors.

We count the permutations which are not galleries by ordering the
$k$ negative hyperplanes, ordering the $n-k$ positive hyperplanes and
doubling the result for reversals. Subtracting from all $n!$
permutations gives the total:
\[
\abs{\Gamma(\sA,c)}=n!-2k!(n-k)!.
\]
\end{proof}

This enumeration of the galleries matches the computational results of Table ~\ref{tab:crank1-gallery-counts}.

\begin{example} To illustrate permutations which are not galleries we use
\[
\sAd=\bordermatrix{
& \aid{1} & \aid{2} & \aid{3} & \aid{4} \cr
& -1 & -1 & 1 & 1 \cr
}.
\]
We previously worked with $\sAd$ in Example ~\ref{ex:gallery-coherent} where we
enumerated all $16$ galleries of the monotone path graph by listing all paths on
$(\Z{\sA},v)$. We can now revisit this result and count $8$
non-galleries of $\sA$, which have the form
(positive,positive,negative,negative) or
(negative,negative,positive,positive). 
\[
\setof{
\begin{array}{cccc}
1234,&1243,&2134,&2143 \\
4321,&3421,&4312,&4512 \\
\end{array}
}
\]
We then know that there are $16=24-8$ galleries of $\sA$, which we can
check by referring back to Example ~\ref{ex:gallery-coherent}.
\end{example}

\section{A Universally All-Coherent Family.}
\label{ss:cr1-zonotopal}
We now focus on the family of pointed hyperplane arrangements in which
$\sAd$ has exactly $1$ negative vector and claim this pointed
hyperplane arrangement is universally all-coherent. Definition ~\ref{def:universally-all-coherent} describes
universally all-coherent, pointed hyperplane arrangements, as those
for which any cellular string of $(\Z{\sA},\f{})$ is coherent for any
$\f{}$. We prove this by enumerating the galleries constructively,
then finding a functional $\g{}$ corresponding to each $\f{}$-monotone
path. We sketch a method to find $\g{}$ for an arbitrary cell but rely
on Lemma ~\ref{l:coherent-strings} to prove all-coherence for a particular
$\f{}$ and since our argument is for any $\f{}$ we conclude
that $(\sA,c)$ is universally all-coherent.

\begin{lemma} For the corank 1 hyperplane arrangement with
\[
\sAd=
\bordermatrix{
 & \aid{1} & \aid{2} & \cdots & \aid{n} & \cr
 & -1      &  1      & \cdots & 1 \cr
}
\]
and the chamber $c$ corresponding to the covector $(+)^n$, the number
of galleries is:
\[
\abs{\Gamma(\sA,c)}=d!(d-1).
\]
\label{l:cr1-coh-galleries}
\end{lemma}

\begin{remark} The number of galleries $\abs{\Gamma(\sA,c)}$ equals
the number of ways to
organize $n$ books on two bookshelves so that each shelf receives at
least one book~\cite{OEIS_bookshelf}. Such arrangements are well
documented and the subject of the occasional research project
~\cite{Rivera14}.
\end{remark}

\begin{proof}
The bijection is straightforward. Since $\aid{1}$ is the only negative
vector of $\sAd$, we know $\gal(1)\ne 1$ and $\gal(n)\ne 1$ and $\aid{1}$ divides the $d$
remaining vectors into two pieces. There are $d-1$ possible
positions for the negative vector and $d!$ orderings of the remaining
vectors.

We check that this equals the expression for $\abs{\Gamma(\sA,c)}$
found in when $k=1$ and $n=d+1$ in Lemma ~\ref{l:cr1-gallery-count}:
\[
n!-2k!(n-k)!\bigg|_{k=1}=(d+1)!-2(d+1-1)!=d!(d+1-2)=d!(d-1).
\]
\end{proof}

Our goal is, for any generic
$\f{}\in \Rd{d}$, to find a functional $\g{}\in \Rd{d}$ selecting an arbitrary $\f{}$-monotone, and to extend that argument to an arbitrary cellular
string using Lemma ~\ref{l:coherent-strings}. 
When an $\f{}$-monotone path is coherent we can use Lemma 
~\ref{l:cr1-coh-galleries} to pick $\setof{\g{i}}$ so that
$\g{1}=0$. We know that there will be a positive vector to the left
and right of in $\gal$ so our idea is simply to make these positive
and negative vectors as large as we want to satisfy
\[
\sum_{i=2}^n \g{i}=0.
\]
We make a similar argument for a
cellular string. We locate the block $\cell{k}$ containing
$\ai{1}$ and make $\g{i}=0$ for all $\ai{i} \in \cell{i}$. When every
cellular string is coherent, the monotone path graph is equal to the
fiber polytope. 
%Table ~\ref{tab:cr1-f-vector} displays $f$-vectors
%of the fiber polytope of $(\sA,c)$ for small $d$ and for generic $\f{}$. 
%\input{table-k1fvector.tex}

\begin{lemma} For the corank 1 hyperplane arrangement with
\[
\sAd=
\bordermatrix{
 & \aid{1} & \aid{2} & \cdots & \aid{n} & \cr
 & -1      &  1      & \cdots & 1 \cr
}
\]
with $\fd{}=(\f{1},\ldots,\f{n})$ generic on $\Z{\sA}$, any
 $\f{}$-monotone path
 $\gal$ is coherent. 
\label{l:cr1-zonotopal}
\end{lemma}

\begin{proof} 
Our proof has been sketched above. Using Lemma
~\ref{l:cr1-coh-galleries} we assume that $\sA$ has been
ordered to that $\gal=(2,3,\ldots,k,1,\ldots,n)$. Pick arbitrary
$\setof{\g{i}}$ so that  
\begin{itemize}
\item $\displaystyle \g{1}=0$,
\item $\displaystyle
\frac{\g{2}}{\f{2}}<\cdots<\frac{\g{k}}{\f{k}}
<0=\frac{\g{1}}{\f{1}}<
\frac{\g{k+1}}{\f{k+1}}<
\cdots <
\frac{\g{n}}{\f{n}},
$
\item $\displaystyle \sum_{i=2}^n \g{i}=0$
\end{itemize}
This is easily satisfied
by making $\g{n}$ adequately large and $\g{2}$ adequately negative. We then know that
\[
\sum_{i=1}^n \g{i}\aid{i}=0
\]
so $\gal$ is a coherent $\f{}$-monotone path of $\Z{\sA}$. 
\end{proof}

Thus, when $\sA$ is of corank $1$ with exactly $\sAd$
containing exactly $1$ negative vector, every $\f{}$-monotone path is coherent. Lemma ~\ref{l:coherent-strings} then tells us that $(\sA,c)$ is
universally all-coherent. As a corollary we know the diameter of $\gtwo{\sA,c}$ is $\abs{L_2}$.


\section{Minimal Obstructions and Uniqueness}
\label{ss:cr1-non-zonotopal}
Now we find a minimal obstruction set in corank $1$ and prove that our
universally all-coherent family is unique by showing all other corank
1 pointed hyperplane arrangements are liftings of the minimal
obstruction set and contain at least one incoherent $\f{}$-monotone
path by Lemma ~\ref{l:incoherent-lifting}. The
minimal obstruction set is simple, consisting of a single hyperplane
arrangement $\sA$, with $4$ vectors in $\R{3}$ and $2$ negative
vectors in $\sAd$. In Example
~\ref{ex:gallery-coherent} we found incoherent $\f{}$-monotone paths
for a specific $\f{}$. Our task now is to find an incoherent
$\f{}$-monotone path in $\sA$ for every $\f{}$. We realize $\sA$ as
\[
\sA=
\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} \cr
& 1 & 0 & 0 & 1 \cr
& 0 & 1 & 0 & 1 \cr
& 0 & 0 & 1 & -1 \cr
}
\qquad \sAd=
\bordermatrix{
& \aid{1} & \aid{2} & \aid{3} & \aid{4} \cr
&-1 & -1 & 1 & 1 \cr
}
\]

The graph, $\gtwo{\sA,c}$, depends on $\sA$ and $c$. As in Example
~\ref{ex:gallery-coherent} coherence of $\gal$ depends on the function
$\f{}$ inducing an acyclic orientation on $\sA$. We gain some
intuition for which $\f{}$-monotone paths are incoherent by looking at
the fiber zonotope for any arbitrary $\f{}$, whose vertices
parametrize the coherent $\f{}$-monotone paths ~\cite{Billera92}.

\begin{figure}
\begin{minipage}{.8\linewidth}
\centering
\input{figures/mpg-n4-k2-generic.tex}
%% \includegraphics[width=\linewidth]{figures/mpg-n4-k2-f1111}
%% \end{minipage}
%% \begin{minipage}{.22}
%% \includegraphics[width=\linewidth]{figures/mpg-n4-k2-f1212}
%% \end{minipage}
%% \begin{minipage}{.22}
%% \includegraphics[width=\linewidth]{figures/mpg-n4-k2-f2112}
%% \end{minipage}
%% \begin{minipage}{.22}
%% \includegraphics[width=\linewidth]{figures/mpg-n4-k2-f3214}
\end{minipage}
\\
\begin{minipage}{.45\linewidth}
\input{figures/mpg-n4-k2-A.tex}
\end{minipage}
\begin{minipage}{.45\linewidth}
\input{figures/mpg-n4-k2-B.tex}
\end{minipage}
\begin{minipage}{.45\linewidth}
\input{figures/mpg-n4-k2-C.tex}
\end{minipage}
\begin{minipage}{.45\linewidth}
\input{figures/mpg-n4-k2-D.tex}
\end{minipage}
\caption{Monotone path graph of $\sA$ and possible fiber polytopes.}
\label{fig:cr1-min-incoherent-cases}
\end{figure}

\begin{example} Using Lemma
~\ref{def:gallery-fiber-zonotope} we can write down the vectors which
generate the fiber polytope. Using
$\f{}(x,y,z)=\f{1}x+\f{2}y+\f{3}z$ we can compute
$\f{4}=\f{1}+\f{2}-\f{3}$ and 
\[
\sAp=\setof{\begin{array}{ccc}
v_{12}=\begin{pmatrix} -\f{2} \\  \f{1} \\ 0  \end{pmatrix} &
v_{13}=\begin{pmatrix} -\f{3} \\  0 \\ \f{1}  \end{pmatrix} &
v_{14}=\begin{pmatrix} \f{3}-\f{2} \\  \f{1} \\ -\f{1}  \end{pmatrix} \\
v_{23}=\begin{pmatrix} 0  \\ -\f{3} \\ \f{2}  \end{pmatrix} &
v_{24}=\begin{pmatrix} \f{2}  \\ \f{3}-\f{1} \\ -\f{2}  \end{pmatrix} &
v_{34}=\begin{pmatrix} \f{3}  \\  \f{3} \\ -(\f{1}+\f{2})  \end{pmatrix} \\
\end{array}}.
\]
\end{example}
It is clear from these vectors that
the fiber zonotope has at most $12$ vertices and $12$ edges. Lemma
~\ref{l:cr1-gallery-count} gives $16$ galleries of
$\gtwo{\sA,c}$ so we argue that the monotone path graph must contain at
least $4$ incoherent $\f{}$-monotone paths. We would like to
predict, given $\f{}$, which $\f{}$-monotone paths are incoherent. 
Looking at Figure ~\ref{fig:cr1-min-incoherent-cases} we guess that the incoherent
$\f{}$-monotone paths are from the set:
\[
\setof{
\begin{split}
1423,2314,1324,2413 \\
3241,4123,4231,3142 \\
\end{split}
}.
\]
%\input{table-cr1-exceptional-summary.tex}

Each vector $v_{ij}$ corresponds to a flip of $\gtwo{\sA,c}$. We
notice that the vectors $v_{ij}$ will be parallel
for specific $\f{}$. Looking at the
vectors $v_{13}$ and $v_{24}$ in $\sAp$ we see that the vectors
\[
v_{13}=\begin{pmatrix} -\f{3} \\  0 \\ \f{1}  \end{pmatrix} \quad
v_{24}=\begin{pmatrix} \f{2}  \\ \f{3}-\f{1} \\ -\f{2}  \end{pmatrix}.
\]
are parallel when $\f{3}=\f{1}$ and speculate that a degeneracy of
the $\sAp$ causes the failure of coherence. In contrast, we
see that $2314$ is incoherent whenever $\f{3}\le\f{4}$ regardless of
$\sAp$. This motivates a geometric proof that the monotone path
graph contains incoherent galleries.

\begin{lemma} In the pointed hyperplane arrangement $(\sA,c)$ of $4$
vectors in $\R{3}$ in which $\sAd$ has exactly $2$ negative vectors,
and $(\sA,c)$ is realized  by $\f{}^*=(\f{1},\f{2},\f{3},\f{4})$ on $\Z{\sA}$, the $\f{}$-monotone path $\gal=2314$ is incoherent.
\label{l:cr1-non-zonotopal}
\end{lemma} 

\begin{proof} The proof is elementary and we include it as a model for harder
minimal obstructions in Lemma ~\ref{l:cr2-exc}. We avoid fractions
by choosing $\ai{i}$ so that $\f{i}=1$ giving us:
\[
\sA=
\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} \cr
& \frac{1}{\f{1}} & 0 & 0 & \frac{1}{\f{1}} \cr
& 0 & \frac{1}{\f{2}} & 0 & \frac{1}{\f{2}} \cr
& 0 & 0 & \frac{1}{\f{3}} & \frac{-1}{\f{3}} \cr
},
\qquad \text{and} \quad 
\sAd=
\bordermatrix{
& \aid{1} & \aid{2} & \aid{3} & \aid{4} \cr
&-\f{1} & -\f{2} & \f{3} & \f{4} \cr
}.
\]
With $\f{1}+\f{2}=\f{3}+\f{4}$. Without loss
of generality we assume that $\f{1} \le \f{2}$ and
$\f{3} \le \f{4}$. Further we know that $\f{1}\le\f{4}$ and
$\f{3} \le \f{2}$ so the vector $\aid{2}+\aid{3}$ points in
the negative direction while $\aid{1}+\aid{4}$ points in the
positive direction and $\aid{1}+\aid{4}+\aid{2}+\aid{3}=0$. 

We know $2314$ is a 
gallery of $\sA$ and if $\gal=2314$ is coherent we can find 
$\setof{\g{1},\g{2},\g{3},\g{4}}$ so that 
\[
\begin{split}
\g{1}\aid{1}+\g{2}\aid{2}+\g{3}\aid{3}+\g{4}\aid{4} \text{ and } 
0<\g{2}<\g{3}<\g{1}<\g{4}.
\end{split}
\]
The vector $\g{2}\aid{2}+\g{3}\aid{3}$ is less negative than
$\aid{2}+\aid{3}$ while the vector $\g{1}\aid{1}+\g{4}\aid{4}$ is more positive
than $\aid{1}+\aid{4}$. Since $\aid{1}+\aid{4}+\aid{2}+\aid{3}=0$ we
then know that
$\g{1}\aid{1}+\g{4}\aid{4}+\g{2}\aid{2}+\g{3}\aid{3}>0$, contradicting
our assumption that $\gal$ is coherent. 
\end{proof}

\begin{figure}
\begin{tikzpicture}
\draw (0,.1) -- (0,-.1);
\draw (0,-.25) node {$0$};
\draw[->,thick] (0,0) -- (1.5,0);
\draw[->,thick] (0,0) -- (-1.5,0);
\draw (-1.5,-.25) node {$\aid{2}+\aid{3}$};
\draw  (1.5,-.25) node {$\aid{1}+\aid{4}$};
%% \draw[color=blue] (-.75,-.1) -- (-.75,.25);
%% \draw[color=blue] (-.75,.5) node {$\g{2}\aid{2}+\g{3}\aid{3}$};
%% \draw[color=green] (1.75,-.1) -- (1.75,.25);
%% \draw[color=green] (1.75,.5) node {$\g{1}\aid{1}+\g{4}\aid{4}$};
\draw[->, thick, color=blue]  (0,.1) -- (-.75,.1);
\draw[color=blue] (-.75,.5) node {$\g{2}\aid{2}+\g{3}\aid{3}$};
\draw[->, thick, color=green] (0,.1) -- (1.75,.1) ;
\draw[color=green] (1.75,.5) node {$\g{1}\aid{1}+\g{4}\aid{4}$};
\end{tikzpicture}
\caption{$\gal=2314$ is incoherent for every $\f{}$}
\label{fig:cr1-non-zonotopal}
\end{figure}

\begin{corollary} Let $(\sA,c)$ and $(\sAp,c')$ be pointed hyperplane
arrangements, $\sA \subset \R{d}$ and $\sAp \subset \R{d^\prime}$.  
\begin{enumerate}
\item Suppose $\sAp=\setof{\ai{n+1}}$ is an isthmus,
$\fl{} \in \Rd{d+1}$ is generic on $\Z{\sA \bigsqcup \sAp}$, and
$\gall$ is an $\fl{}$-monotone path on $\Z{\sA \bigsqcup \sAp}$. Then
defining $\f{}\in \Rd{d}$ via
\[
\R{d} \xhookrightarrow{\quad\iota\quad} \R{d+1} \xrightarrow{\quad\fl{}\quad} \R{}
\]
and $\gal=\gall\contract \setof{\ai{n+1}}$, one has that:
\begin{itemize}
\item $\f{}$ is generic on $\Z{\sA}$,
\item $\gal$ is $\f{}$-monotone, and
\item $\gall$ is coherent if and only if $\gal$ is coherent.
\end{itemize}

\item If $\corank{\sA}\ge 1$ and $\corank{\sAp} \ge 1$ and
$\fl{}\in \Rd{d+d^\prime}$ is a generic functional on
$\Z{\sA \bigsqcup \sAp}$, then there is an incoherent $\fl{}$-monotone
path of $\Z{\sA \bigsqcup \sAp}$
\end{enumerate}

\label{c:incoherent-product}
\end{corollary}

\begin{proof}
To prove item (1), we assume that $\setof{\ai{i}}$ have been chosen so
that $\fl{i}=1$ for all $i$ and 
$\gal=\gall \contract \setof{\ai{n+1}}=(1,2,\ldots,n)$. We define
$\f{}=\fl{} \circ \iota$ by the 
embedding $\iota:\R{d}\xhookrightarrow{\quad\iota\quad}\R{d+d^\prime}$
as the first $d$ coordinates. We check $\f{}$ is generic on by
computing $\f{i}=\f{}(\ai{i})=\fl{}(\ai{i})=1>0$.

To show that $\gall\contract \setof{\ai{n+1}}=(1,2,\ldots,n)$ is an
$\f{}$-monotone path of $\Z{\sA}$, we view $\gall$ as a path on
$\Z{\sA \bigsqcup \sAp}$. We understand $\Z{\sA \bigsqcup \sAp}$ as a
prism over $\Z{\sA}$ using Lemma ~\ref{l:product-zonotope}. Removing
$\ai{n+1}$ projects $\Z{\sA \bigsqcup \sAp}$ to $\Z{\sA}$ along the
isthmus $\ai{n+1}$ and the path
$\gall$ projects to $\gal$, following edges of $\Z{\sA}$. Therefore,
$\gal$ is a $\f{}$-monotone path

We suppose $\gal$ is a coherent $\f{}$-monotone path of $(\Z{\sA},f)$,
and recall that $\f{}(\ai{i})=\f{i}=1$ so there are 
$\setof{\g{1},\ldots,\g{n}}$ such that
\[
0 < \g{1} < \ldots \g{n} .
\]
Let $k$ be the position of $\ai{n+1}$ in $\gall$ and define $\gl{i}=\g{i}$ for $1 \le i \le n$ and
\[
\gl{n+1} =
\begin{cases}
0 & \text{ if } k=1,\\
\g{n} +1 & \text{ if } k=n+1, \\
\frac{\g{k}+\g{k+1}}{2} & \text{otherwise.}\\
\end{cases}
\]
For $1 \le i < k$ we know $\gl{i}=\g{i}<\gl{n+1}$ and for 
$k < i \le n+1$ we also know $\gl{n+1} < \g{i}=\gl{i}$, so $\gall$ is a
coherent $\fl{}$-monotone path of $\Z{\sA \bigsqcup \sAp}$. 

Now suppose that 
$\gall$ is a coherent $\fl{}$-monotone path so there are
$\setof{\g{i}}$ selecting $\gall$ and by ignoring $\g{n+1}$ we have
\[
\frac{\g{1}}{\f{1}} < \frac{\g{2}}{\f{2}} < \cdots
< \frac{\g{n}}{\f{n}}. 
\]
We know that $\setof{\g{1},\ldots,\g{n},\g{n+1}}$ are a linear dependence of
$\left(\sA \bigsqcup \sAp\right)^*=\sAd \cup \setof{0}$, so
\[
\sum_{i=1}^{n+1}\g{i}\aid{i}=\sum_{i=1}^{n}\g{i}\aid{i}+\g{n+1}\begin{pmatrix}0 \\ \vdots\\0\end{pmatrix}=\sum_{i=1}^{n}\g{i}\aid{i}=0.
\]
Therefore, $\setof{\g{1},\ldots,\g{n}}$ is a linear dependence of $\sAd$ and
$\gal$ is a coherent $\f{}$-monotone path.

To prove item (2), we must show that $\Z{\sA \sqcup \sAp}$ has
incoherent $\fl{}$-monotone paths for any $\fl{} \in \Rd{d+d^\prime}$.
When $\sA$ or $\sAp$ are not universally all coherent,
$\Z{\sA \sqcup \sAp}$ has incoherent $\fl{}$-monotone paths by Lemma
~\ref{l:incoherent-lifting}. When $\sA$ and $\sAp$ are universally
all-coherent, both $\sA$ and $\sAp$ are extensions of universally
all-coherent corank 1 hyperplane arrangements. We show that
$\Z{\sA \sqcup \sAp}$ has incoherent $\fl{}$-monotone paths when $\sA$
and $\sAp$ are both of universally all-coherent and of corank 1, and
any higher corank will follow from Lemma
~\ref{l:coherent-projection}. We assume that $\abs{\sA}=d+1\le
d'+1=\abs{\sAp}$, and assume that $\sA$ and $\sAp$ are ordered so
that:
\[
\begin{split}
 \sA&=\setof{\ai{1},\ldots,\ai{d},\ai{d+1}=\sum_{i=1}^d \ai{i}}\text{, and} \\
\sAp&=\setof{\ai{d-d'+1}^\prime,\ldots,\ai{0}^\prime,\ai{1}^\prime,\ldots,\ai{d}^\prime,\ai{d+1}^\prime=\sum_{i=1}^d \ai{i}^\prime}.
\end{split}
\]
We further assume $\sA$ and $\sAp$ are chosen so that
$f(\ai{i})=1$ for $1 \le i \le d$ and $f(\ai{i}^\prime)=1$ for $d-d'+1 \le
i \le d'$. Then know $f(\ai{d+1})=d$ and
$f(\ai{d+1}^\prime)=d'$. Using the combinatorial description in Lemma
~\ref{l:cr1-coh-galleries} we claim that
\[
\begin{split}
\gal&=(\ai{1},\ldots,\ai{d-1},\ai{d+1},\ai{d})\text{ is a gallery of }
(\sA,c),  \text{ and } \\
\galp&=(\ai{d-d'+1}^\prime,\ldots,\ai{0}^\prime,\ai{1}^\prime,\ldots,\ai{d-1}^\prime,\ai{d+1}^\prime,\ai{d}^\prime) \text{
is a gallery of } (\sAp,c')  \text{. }\\
\text{Define }\gall&=
(\ai{d-d'+1}^\prime,\ldots,\ai{0}^\prime,
\ai{1}^\prime,
\ai{1},
\ldots,
\ai{d-1}^\prime,
\ai{d-1},
\ai{d+1},
\ai{d+1}^\prime,
\ai{d}^\prime,
\ai{d}
)
\end{split}
\]
as a shuffle of $\gal$ and $\galp$ and an $\fl{}$-monotone path of $\Z{\sA \sqcup \sAp}$ by Lemma ~\ref{l:product-graph}. We claim
that $\gall$ is an incoherent $\fl{}$-monotone path of
$\Z{\sA \sqcup \sAp}$. Suppose $\gall$ were coherent, then there would
be $\setof{\g{i},\gp{i}}$, chosen so that $\gp{0}<0<\gp{1}$ and  satisfying
\[
\underbrace{\gp{d-d'+1}<\ldots<\gp{0}}_{\gp{i}<0}<
\underbrace{\gp{1}<
\g{1}<
\cdots<
\gp{d-1}<
\g{d-1}}_{\gp{i}<\g{i}}<
\frac{\g{d+1}}{d}<
\frac{\gp{d+1}}{d'}
<
\gp{d}<
\g{d}.
\]
In particular we
have $\g{d+1}/d< \gp{d+1}/d'$. 
We compute:
\[
\frac{\gp{d+1}}{d'}=
\sum_{i=d-d'+1}^d \frac{\gp{i}}{d'}<
\frac{1}{d'}\sum_{i=0}^d \gp{i}<
\frac{1}{d'}\sum_{i=0}^d \g{i}\le
\frac{1}{d}\sum_{i=0}^d \g{i}=\frac{\g{d+1}}{d},
\]
a contradiction so $\gall$ is an incoherent $\fl{}$-monotone
path. Thus $\Z{\sA}$ contains incoherent $\fl{}$-monotone paths for
every $\fl{}$. 
\end{proof}


\begin{figure}
\input{figures/mpg-shuffle-segment.tex}
\caption{All shuffles of $132$ and $465$}
\label{fig:shuffle-segment}
\end{figure}

Corollary ~\ref{c:incoherent-product} will be particularly useful for
our classification, allowing us to focus on irreducible hyperplane
arrangements. This completes our classification of universally
all-coherent pointed hyperplane arrangements in corank 1. To put the
result in a tidy package we state the classification as our first main
theorem.
\begin{theorem} For a pointed hyperplane arrangement $\sA$ in corank $1$, 
   \begin{itemize}
   \item if $\sAd$ has exactly 1 negative vector, then $(\sA,c)$ is
   universally all-coherent, and
   \item if $\sAd$ has $2$ or more negative vectors then the monotone
   path graph has at least one incoherent $\f{}$-monotone path for any
   $\f{}$. 
   \end{itemize}

The classification, with minimal obstruction class highlighted in red,
   the universally all-coherent family in green, is illustrated in
   Figure ~\ref{fig:cr1-classification}.
\label{thm:cr1-classification}
\end{theorem}

\begin{proof} 
Our previous lemmas contain all the arguments required for this
proof. The classification of irreducible hyperplane arrangements in
corank $1$ can be see in Figure ~\ref{fig:cr1-classification}. Every
hyperplane arrangement for which $\sAd$ has $2$ or more negative
vectors is a lifting of the minimal obstruction class in Lemma
~\ref{l:cr1-non-zonotopal} and has incoherent galleries by Lemma
~\ref{l:incoherent-lifting}. When $\sAd$ has exactly $1$ negative
vector, every gallery is coherent by Lemma
~\ref{l:cr1-zonotopal}. Moreover, every cellular string is coherent
according to Lemma ~\ref{l:coherent-strings}. Reducible hyperplane
arrangements are addressed by Corollary ~\ref{c:incoherent-product}
and universally all-coherent when $\sA$ is a disjoint union of an
all-coherent irreducible hyperplane arrangement and an isthmus, or
series of isthmuses.
\end{proof}


\begin{figure}
\centering
\includegraphics[scale=.5]{figures/cr1_classification}
\caption{Classification of monotone path  graphs in corank 1 ordered
by single element lifting. Zonotopal arrangements distinguished by circular nodes}
\label{fig:cr1-classification}
\end{figure}

\section{Diameter bound}

Having classified monotone path graphs into universally all-coherent
and containing an incoherent path for every $\f{}$ we now return
to the main question of ~\cite{Reiner12} ``For real hyperplane
arrangements $\sA$ and a choice of base chamber $c$, does the graph
$\gtwo{\sA,c}$ of minimal galleries from $-c$ to $c$ have diameter
exactly $\abs{L_2}$?''. Our strategy is to explicitly compute the
diameter of $\gtwo{\sA,c}$ when $\sAd$ has exactly 1 negative vector.
By identifying $\gtwo{\sA,c}$ with the one skeleton of $\Z{\sAp}$, we
know the diameter of $\gtwo{\sA,c}$ is $\abs{L_2}$. For all other pointed
hyperplane arrangements, we find a gallery which is provably
$L_2$-accessible using our understanding of the covectors of
$\mat{\sA,(+)^n}$.

The first part of this strategy is simple. We have already shown that
$\gtwo{\sA,c}$ equals the one-skeleton of $\Z{\sAp}$ when $\sAd$ has
exactly $1$ negative vector. To compute its diameter, we simply recall
the well-known result of ~\cite{Billera92}, and we know
$\diam \gtwo{\sA}=\diam \gone{\sAp}=\abs{L_2}$

The second part of this strategy requires more work. It will be
helpful to give $\sAd$ coordinates. To make $L_2$ separation easier
to work with, we will use slightly nonstandard coordinates for $\sAd$:
\[
\sAd=
\bordermatrix{
& \aid{1} & \ldots & \aid{k-1} & \aid{k} & \cdots & \aid{n} \cr
& -1 & \ldots & -1 & 1 & \cdots & -1 \cr
}.
\]
In this ordering, $(1,2,\ldots,n)$ a gallery of $(\sA,c)$.
We find $L_2$ separation using modified bubble sort operations ~\cite{Cormen09},
~\cite{Knuth98} and claim that $(1,2,\ldots,n)$ is $L_2$-accessible.

%Theorem: corank 1, k>1 has an L_2 accessible gallery
\begin{lemma} For an irreducible hyperplane arrangement $\sA$ of
$n=d+1$ hyperplane in $\R{d}$ in which $\sAd$ has at least $2$
negative vectors, $\aid{1}=\aid{n}=-1$, the gallery $(1,2,\ldots,n)$
is $L_2$-accessible in $\gtwo{\sA,c}$.
\label{l:cr1-existsL2}
\end{lemma}

\begin{proof} For irreducible $\sA$ of corank $1$,
$L_2(\sA)=\setof{X_{i,j} \big| 1\le i < j \le n}$. For an arbitrary 
$\gal$ separation set of $\gal$ and
$(1,2,\ldots,n)$ is
\[
L_2((1,2,\ldots,n),\gal)=\setof{X_{i,j} \big| i<j \quad \text{and} \quad \gal(i)
> \gal(j)}.
\]
Any flip $X_{i,j}$ of $\gal$ with $i<j$ will reduce
$L_2((1,2,\ldots,n),\gal)$ by one. We must exhibit such a flip for
every $\gal$. We consider two cases:
\begin{itemize}
\item If $\gal(1) \ne 1$ and $\gal(n) \ne n$, and $\gal(i)=1$,
$\gal(j)=n$ then either $X_{1,\gal(i-1)}$ or $X_{\gal(j+1),n}$ is a
flip of $\gal$. 

Suppose $\gal(1) \ne 1$, $\gal(n) \ne n$, and
$X_{1,\gal(i-1)}$ is not a flip of $\gal$. Our combinatorial
description of Lemma ~\ref{l:cr1-gallery-count} says $\gal$ has the form 
\[
\gal=\left(\underbrace{(i-1) \text{ negative vectors of } \sAd}\right)
\gal(i+1)1
\left(\underbrace{(n-i-1) \text{ positive vectors of } \sAd}\right)
\]
and $X_{\gal(j+1),n}$ is a flip of $\gal$ that
decreases the $L_2$ separation of $\gal$ and $(1,2,\ldots,n)$. A symmetric argument tells us that
when $X_{\gal(j+1),n}$ is not a flip of $\gal$ then $X_{1,\gal(i-1)}$
is. 

\item If $\gal(1)=1$ and $\gal(n)=n$ we take
$k=\min\setof{l \big| \gal(l) < \gal(l+1)}$ and claim that 
$X_{k,\galp(k+1)}$ is a flip of $\gal$. We know that $1<k<n-1$ so any  
sign vector $c$ with $c_{\gal(k)}=c_{\gal(k+1)}=0$ corresponding to
$X_{k,\gal(k+1)}$ has $c_1=-$ and $c_n=+$ so $c$ will be a covector
of $\sA$. 

\end{itemize}
\end{proof} 

%Illustration of algorithm
\begin{example} We will illustrate Theorem ~\ref{l:cr1-existsL2} using
a hyperplane arrangement of $10$ hyperplanes in $\R{9}$, in which
  $\sAd$ has exactly $2$ negative vectors. Our idea is to begin by
  first moving $1$ to the left then 10 to the right. The result is the
  following path in $\gtwo{\sA,c}$:
\[
\begin{array}{c|c|c}
\text{step} & \text{gallery} & \text{next flip} \\
\hline
0 & (10 , 3 , 2 , 4 , 9 , 8 , 7 , 6 , 5, 1) & X_{1,5} \\
1 & (10 , 3 , 2 , 4 , 9 , 8 , 7 , 6 , 1, 5) & X_{3,10} \\
2 & (3 , 10 , 2 , 4 , 9 , 8 , 7 , 6 , 1, 5) & X_{2,10} \\
\vdots& \vdots &\vdots \\
9  & (3 , 2  , 4 , 9 , 8 , 7 , 6 , 10, 1, 5) & X_{1,10} \\
10 & (3 , 2  , 4 , 9 , 8 , 7 , 6 , 1, 10, 5) & X_{5,10} \\
11 & (3 , 2  , 4 , 9 , 8 , 7 , 6 , 1, 5, 10) & X_{2,3} \\
\vdots& \vdots &\vdots \\
 & (1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10) & \text{complete}\\
\end{array}
\]
At each step we avoid the permutations
$(-,\ldots,-,1,10)$ and $(-,\ldots,-,10,1)$. After 
moving $10$ to the right, we know the origin
is captured by $\aid{10}$ and $\aid{\gal(9)}$.
\end{example}

We now prove our second main theorem which answers the main question of
~\cite{Reiner12} in corank 1.

\begin{theorem} For a hyperplane arrangement $\sA$ of $n=d+1$ hyperplane in
$\R{d}$ with an acyclic orientation, the monotone path graph 
$\gtwo{\sA,c}$ has diameter $\abs{L_2}$.
\label{thm:cr1-diameter}
\end{theorem}

\begin{proof} We restrict our attention to irreducible hyperplane
arrangements, relying on Lemma ~\ref{c:l2-product} to deal with
disjoint unions. Let $\sAd$ be the dual of $\sA$ and $k$ be number of
negative vectors in $\sA$. When $k=1$, Theorem
~\ref{l:cr1-zonotopal} tell us that $\gtwo{\sA,c}$ is universally all-coherent,
written as $\Z{\sAp}$, and thus has diameter
$\abs{\sAp}=\abs{L_2(\sA)}$. When $k>1$ Theorem ~\ref{l:cr1-existsL2}
tells us that there is an $L_2$-accessible node and ~\cite{Reiner12}
guarantees that the diameter of $\gtwo{\sA,c}=\abs{L_2}$ . Thus
$\gtwo{\sA,c}$ has diameter $|L_2|$ for all $k$.
\end{proof}

%% We have given a positive answer to the main question of
%% ~\cite{Reiner12} for arrangements of $n=d+1$ hyperplanes in
%% $\R{d}$. In addition we found a family of all universally all-coherent
%% pointed hyperplane arrangements, providing both combinatorial
%% descriptions of the galleries and an explicit description of
%% functionals selecting any $\f{}$-monotone path. Outside of our universally
%% all-coherent family, we gave a minimal obstruction set to
%% all-coherence proving that all other monotone path graphs 
%% contain an incoherent $\f{}$-monotone path for every $\f{}$. 


