We now make oriented matroids concrete by introducing the closely
related objects of vector configurations and hyperplane
arrangements. We explain the close relationship between these two
objects by describing their cocircuits and the zonotope. We then
discuss galleries of pointed hyperplane arrangement and pointed
zonotopes, and we make galleries and flips geometrically clear by
viewing them as paths on zonotopes. Finally, we revisit oriented
matroid duality, explicitly constructing the Gale dual and explaining
galleries using duality. We close by recalling some highlights of a
classification of pointed hyperplane arrangements, including
affine Gale duals, single-element liftings, and single-element
extensions, although we leave most details to the supplementary
material in Appendix ~\ref{a:agd-classification}.

\begin{defn}
A \emph{hyperplane arrangement} $\sA\subset\R{d}$ is a set of $n$
hyperplanes in $\R{d}$. Each hyperplane has a specified normal vector
$\ai{i}$ and we will write $A=(\ai{i})$ as $d \times n$ matrix in
which each column vector $\ai{i}$ is normal to hyperplane $\sH_i$. For
our purposes, hyperplane arrangements will always be:
\begin{itemize}
\item \emph{central}, so that $0 \in \sH_i$ for all $i$, and
\item \emph{essential}, meaning 
      $\bigcap_{\sH_i \in \sA} \sH_i=\setof{0}$, or equivalently, the
      matrix $\sA$ has rank $d$.
%% \item \emph{repetition free} meaning each hyperplane appears only once in $\sA$ that is, $\sH_i \ne
%%   \sH_j$ for $i \ne j$. 
\end{itemize}
\end{defn}

Any hyperplane divides $\R{d}$ into positive and negative
half-spaces. The set of all hyperplanes divides $\R{d}$ into open
polyhedral cones which we call \emph{chambers}. Each of those chambers
is associated to a sign vector $c$ in which $c_i$ is $+$ if the
chamber is in the positive half space of hyperplane $\sH_i$, and $-$
otherwise. The chambers are the maximal covectors of $\mat{\sA}$. We
refer to maximal covectors as chambers when we want to stress the
geometric nature of hyperplane arrangements and maximal covectors when
emphasizing the oriented matroid structure.

\begin{example} We return to the oriented matroid of
  Example ~\ref{ex:matroid-simple} which we realize as a hyperplane
  arrangement by specifying the vector $\ai{i} \in \sA$ as normal to
  hyperplane $\sH_i$.
\[
\sA=
\bordermatrix{%\begin{bmatrix}
&\ai{1} & \ai{2} & \ai{3} \cr
&1 & 0 & 1 \cr
&0 & 1 & 1 \cr
}%\end{bmatrix}.
\]

Figure ~\ref{fig:hyperplane-ex} makes clear the relationship between
the maximal covectors and chambers $\sA$. We also now understand
the geometry of the elimination axiom of Definition
~\ref{def:matroid-covector}; the covector $+0+$ eliminates $2$
between $+++$ and $+-+$ and corresponds to the part of hyperplane $2$
which divides the chambers $+++$ and $+-+$.
\label{ex:hyperplane-ex}
\end{example}

\begin{figure}
\begin{tikzpicture}[scale=1.5]
\draw[-,green] (-2.0,0) -- (2.0,0);
\draw[->] (1.5,0) -- (1.5,.5);
\draw (1.5,.65) node {$\ai{2}$};
\draw[-,red] (0,-2.0) -- (0,2.0);
\draw[->] (0,1.5) -- (.5,1.5);
\draw (.65,1.5) node {$\ai{1}$};
\draw[-,blue] (-2.0,2.0) -- (2.0,-2.0);
\draw[->] (0,0) -- (.5,.5);
\draw (.65,.65) node {$\ai{3}$};
\draw (1.5,1.5) node {$+++$};
\draw (-1,-1) node {$---$};
\draw (-0.9,1.5) node {$-++$};
\draw (-1.5,.7) node {$-+-$};
\draw (0.5,-1.5) node {$+--$};
\draw (1.5,-0.8) node {$+-+$};
\draw (.85,.01) node {$+0+$};
\draw (-.85,.01) node {$-0-$};
\end{tikzpicture}
%\includegraphics[scale=.5]{figures/cr1-hyperplane-ex}
\caption{A simple hyperplane arrangement (Example ~\ref{ex:hyperplane-ex})}
\label{fig:hyperplane-ex}
\end{figure}

We have not specified the oriented matroid structure of
$\sA$, but the maximal covectors of $\sA$ depend only on the normal vectors
$\setof{\ai{i}}$. The oriented matroid 
structure of $\sA$ is defined using the normal vectors. Ignoring
that $\setof{\ai{i}}$ are normal to hyperplanes in $\sA$, we
call any finite set of vectors a vector configuration. 

\begin{defn} A \emph{vector configuration} $V$ is a finite set of vectors
  $V=\setof{v_1\ldots,v_n}$ spanning $\R{d}$ and which we 
  write as a $d \times n$ matrix $V$. We $d$ is the rank of $V$
  and $n-d$ as the corank of $V$. 
\label{def:matroid-vector-config}
\end{defn}

Vector configurations and hyperplane arrangements are the same
combinatorial objects and we use them interchangeably. Any set finite
set of vectors defines a hyperplanes arrangement simply by viewing
each vector as the normal vector of a hyperplane arrangement. Any set
of hyperplanes are described by a configuration of normal vectors. We
define $\mat{}$ by specifying the covectors of $\mat{V}$. The
covectors of $V$ are built from the valuations of linear functions
$\f{}\in \Rd{d}$ on $V$. We favor the hyperplane terminology but often
think using vector configurations. We use $\mat{\sA}$ and $\mat{V}$
interchangeably.


\begin{defn} Give a vector configuration $V$ and $\f{} \in \Rd{d}$,
the vector $\fd{} \in \R{n}$ whose i${}^{th}$ component
 $\f{i}=\f{}(v_i)$ for every $v_i \in V$ is \emph{the valuation} of
 $\f{}$ on $V$. The set of all valuations of $V$ is
\[
\val{V}=\setof{\fd{} \in \R{n}\,:\,\exists \f{}\in \Rd{d} \text{ such
that } \f{}(v_i)=\f{i}} \subset \R{n}.
\]
\label{def:matroid-valuation}
\end{defn}

To any vector $\fd{} \in \R{n}$ we can associate a sign vector $c$ by
mapping the $\sgn$ function over the components of $\f{}$. 
\[
c_i\sgn(w_i)=
\begin{cases}
- & \text{ if } w_i<0, \\
+  & \text{ if } w_i>0, \\
0  & \text{ else}.
\end{cases}
\]
Mapping the sign function over all valuations of $V$ turns the set of
valuations into a set of sign vectors, which we suggestively denote $\mcV{V}=\setof{\sgn{\fd{}}: \,\fd{} \in \val{V}}$. 

\begin{lemma} Given a vector configuration $V$, the set of sign vectors of
  valuations of $V$ satisfies the covector axioms of Definition
  ~\ref{def:matroid-covector} and we say
  $\mcV{V}=\setof{\sgn{\fd{}}: \,\fd{} \in \val{V}}$ are the covectors of an
  oriented matroid $\mat{V}$. When $\mat{}=\mat{V}$ we say that
  $\mat{}$ is a \emph{realized oriented matroid} or that $\mat{V}$
  realizes $\mat{}$. When $V$ is the set of normal to a hyperplane
  arrangement $\sA$, we $\mat{V}=\mat{\sA}$.
\end{lemma}

\begin{proof}
This is a standard and well-known result ~\cite{Ziegler95},
~\cite{Björner93} which we do not reproduce here. We point out
that the matroid structure of $\mat{V}$ depends only on the direction
of $v_i$ and not its magnitude $\abs{v_i}$.
\end{proof}

\begin{example} We have already realized the oriented matroid in
Example ~\ref{ex:matroid-simple} with a hyperplane arrangement in
Example ~\ref{ex:hyperplane-ex}.The
oriented matroid $\mat{}$ has rank $2$, and consists of sign vectors
of length $3$ so we expect $V$ to be a configuration of $3$ vectors in
$\R{2}$. The vectors of $V$ are normal vectors of hyperplanes in $\sA$
so $\mat{}$ is
realized by the vectors
\[
V=
\bordermatrix{
& v_1 & v_2 & v_3 \cr
&1 & 0 & 1 \cr
&0 & 1 & 1 \cr
}
%\end{pmatrix}
\]
To verify that $\mat{}=\mat{V}$ we must find a functional $\f{}(x,y)$ so that
$\sgn{\f{}(v_i)}=c_i$ for every $c\in \mcV{}$. We do this explicitly and
write the functionals $\f{}(x,y)$ directly above its sign vector $\f{}(v_i)$:
\[
\mcV{}=\setof{
\begin{array}{ccc}
x+y, & y,   & -x+2y, \\
+++, & 0++, & -++,   \\
\rule{0pt}{4ex} -x+y,&-2x+y,& -x,    \\
-+0, & -+-, & -0-,   \\
\rule{0pt}{4ex}-x-y,& -y   & x-2y,  \\
---, & 0--  & +--,   \\
\rule{0pt}{4ex}x-y, & 2x-y,& x, \\
+-0, & +-+, & +0+,   \\
\rule{0pt}{4ex}     & 0    & \\
     & 000, & \\
\end{array}
}.
\]
\label{ex:matroid-realized}
\end{example}

We describe both hyperplane arrangements and vector
configurations with a $d \times n$ matrix with vectors as columns. The
matrix $V$ specifies a linear surjection $V:\R{n} \to \R{d}$ and the
image of the $n$-cube $[-1,+1]^n$ is the zonotope
$\Z{}=\Z{V}=\Z{\sA}$ ~\cite{McMullen71}. The zonotope carries all the
oriented matroid information of $\mat{\sA}$ in an easy to understand
geometric package.

\begin{defn} Given a vector configuration $V$, written
  as a $d \times n$ matrix, the \emph{zonotope} is the image of
  the $n$-cube under the linear surjection specified by $V$
  (rep. $V$), written explicitly as the Minkowski sum
\[
Z(V)=\sum_{i=1}^n \left[-\ai{i},\ai{i} \right].
\]
As always when $V$ is the set of normal vectors to a hyperplane
arrangement $\sA$ we define $\Z{\sA}=\Z{V}$. The graph
$\gone{V}=\gone{\sA}$ is the $1$-skeleton of the zonotope and an
important special case.
\label{def:hyperplane-zonotope}
\end{defn}

In geometric terms, the zonotope is a centrally symmetric polytope in
$\R{d}$ whose vertices correspond to maximal covectors of $V$, and
whose $k$ faces correspond to corank $k$ covectors of $\mat{V}$. In
geometric terms, vertices of $Z(\sA)$ are the chambers of $\sA$
and two chambers $c,c^\prime$ are adjacent when they are separated by exactly $1$
hyperplane. 

The zonotope $Z(V)$ is a polytope and its face lattice equals the poset of
covectors of the oriented
matroid of $\mat{V}$. The geometry of the zonotope $Z(V)$ depends on
the choice of the vectors $v_i \in V$, however two vector
configurations have the same oriented matroid if and only if their
zonotopes have equal face lattices ~\cite{Ziegler95}.

To define galleries for vector configurations we want a geometric
understanding of acyclic orientations of $\mat{V}$. An acyclic orientation of
$\mat{}$ is the choice of a maximal covector, and maximal covectors
of $\mat{V}$ are vertices of $\Z{V}$. A realized acyclically oriented
matroid is a zonotope with a distinguished vertex $v$. 

\begin{defn} A \emph{pointed zonotope} is a pair $(Z,v)$ with $Z$ a zonotope in
$\R{d}$ and $v$ a vertex of $Z$. When $V$ is the set of normal
vectors to a hyperplane arrangement $\sA$, and $c$ is the chamber
corresponding to $v$ we say $(\sA,c)$ is a pointed hyperplane
arrangement; we use $(\sA,c)$ and $(Z,v)$ interchangeably.

We say that $(Z,v)$ or $(\sA,c)$ realizes the acyclically oriented matroid
$(\mat{},(+)^n)$ when $\Z{}=\Z{V}=\Z{\sA}$, $\mat{}=\mat{V}$ and $v=\sum v_i$
is the vertex corresponding to $(+)^n$. 
\label{def:vector-pointed}
\end{defn}

Galleries of an acyclically oriented matroid are sequences of corank
$1$ covectors of $\mat{}$. The corank $1$ vectors of $\mat{V}$ are the
edges of $\Z{V}$ so the galleries of $(Z,v)$ of a pointed zonotope are
paths on the edges of $\Z{V}$ between $-v$ and $v$. 

%% Definition ~\ref{def:matroid-acyclic}
%% Pointed zonotopes give a geometric interperation of galleries. When we pick a
%% chamber of $\sA$ we can always pick the normal vectors
%% $\setof{\ai{i}}$ to point towards that chamber, guarantting that
%% $(+)^n$ is a maximal covector of $\mat{\sA}$. 

\begin{defn} Given a pointed zonotope $(Z,v)$, 
a \emph{gallery} $\gal$ is a path of minimal length on the edges of $Z$ from
$-v$ to $v$. 

A gallery of a pointed hyperplane arrangement $(\sA,c)$
is a gallery of the pointed zonotope $(\Z{\sA},v)$ in which the
chamber $c$ corresponds to the vertex $v$ of $\Z{\sA}$ is 
a path in $\Rd{d}$ from $-c$ to $c$.
\label{def:gallery-2}
\end{defn}

It is clear that this is equivalent to Definition
~\ref{def:gallery-3} when $(Z,v)$ realizes the acyclically oriented
matroid $(\mat{},(+)^n)$.

\begin{defn} A polygonal face $X$ of $\Z{\sA}$ is a S\emph{flip} between
two galleries $\gal$ and $\galp$ of a pointed hyperplane
arrangement $(\Z{\sA},v)$ when $\gal$ and $\galp$ differ on $X$ and
agree elsewhere on $\Z{\sA}$ as illustrated in Figure
~\ref{fig:gallery-flip} ~\cite{Athanasiadis01}.
\label{def:gallery-flip}
\end{defn}

This zonotope definition of a flip is consistent with Definition
~\ref{def:flip-3} but not nearly as precise. The polygonal face $X$
corresponds to a codimension $2$ intersection of hyperplanes in $\sA$
which gives rise to a corank $2$ covector of $\mat{\sA}$. From this
information we can build a cellular string $\cell{}$ in which $X$ gives
rise to the distinguished cell $\cell{k}$ and $\gal$ and $\galp$
define corank 1 covectors. Although we defined both
galleries and flips of acyclically oriented matroids using cellular
strings, we omit cellular strings in current level of
generality. Such definitions are possible but are not be necessary
for our understanding. We return to cellular strings in Section
~\ref{ss:level1} and we refer interested readers to ~\cite{Billera94}.

\begin{figure}
\centering
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=2.5]
%\draw (-1,0) -- (1,0);
\draw (-0.707,-0.707) -- (0.707,0.707);
%\draw (0,-.9) node {$----$};
\draw (-0.707,-0.707) node {$H_1$};
\draw (-0.707,0.707)  -- (0.707,-0.707);
\draw (-0.707,0.707) node {$H_2$};
\draw (-0.707,0.707)  -- (0.707,-0.707);
\draw (-1.0,0) -- (1.0,0);
\draw (-1.0,0.0) node {$H_3$};
\draw (-1.0,.4) .. controls (-0.1,0.9) and (0.1,0.9) .. (1.0,.4);
\draw (1.0,.4) node {$H_4$};
\draw (-1.0,.-.4) .. controls (-0.1,-0.9) and (0.1,-0.9) .. (1.0,-.4);
\draw (1.0,.-.4) node {$H_5$};
\draw[style=dashed,color=red] (-.1,-1) -- (-.1,-.25) .. controls
(-.35,-.25) and (-.35,.25) .. (-.1,.25) -- (-.1,1);
\draw[color=red] (-.1,-1.1) node {$\gal$};
\draw[style=dashed,color=blue] (.1,-1) -- (.1,-.25) .. controls
(.35,-.25) and (.35,.25) .. (.1,.25) -- (.1,1);
\draw[color=blue] (.1,-1.08) node {$\galp$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=2.25]
%\draw ()
\fill[green!20!white] (0,-1) -- (1,-.5) -- (1,.5) -- (0,1) -- (-1,.5)
-- (-1,-.5) -- cycle;
\draw  (0,-1.5) -- (0,-1) -- (1,-.5) -- (1,.5) -- (0,1) -- (0, 1.5);
\draw  (0,-1) -- (-1,-.5) -- (-1,.5) -- (0,1);
\draw (-1,0) node {$\ai{3}$};
\draw (1,0) node {$\ai{3}$};
\draw (-.5,-.75) node {$\ai{1}$};
\draw (.5,.75) node {$\ai{1}$};
\draw (.5,-.75) node {$\ai{2}$};
\draw (-.5,.75) node {$\ai{2}$};
\draw (0,1.25) node {$\ai{4}$};
\draw (0,-1.25) node {$\ai{5}$};
%% \draw (-.5,.5) node {$\ai{4}$};
%% \draw (.5,-.5) node {$\ai{1}$};
%% \draw (.5,.5) node {$\ai{4}$};
%% \draw (0,-1) node {$----$};
\draw (0,0) node {$X_{132}$};
\draw[style=dashed,color=red] (-.1,-1.5) -- (-.1,-1) -- (-1.1,-.5) --
(-1.1,.5) -- (-.1,1) -- (-.1,1.5);
\draw[style=dashed,color=blue] (.1,-1.5) -- (.1,-1) -- (1.1,-.5) -- (1.1,.5) -- (.1,1) -- (.1,1.5);
\draw[color=red] (-.2,-1.5) node {$\gal$};
\draw[color=blue] (.2,-1.5) node {$\galp$};
\end{tikzpicture}
\end{minipage}
\caption{Flips as hyperplane intersections and faces of the zonotope}
\label{fig:gallery-flip}
\end{figure}

\begin{defn} Given a pointed zonotope $(Z,v)$ the \emph{graph
$\gtwo{Z,v}$} is the graph whose vertices are galleries and in which
  two galleries are adjacent when they differ by a flip.  The graph
  $\gtwo{\sA,c}=\gtwo{Z,v}$ when $\Z{}=\Z{V}$ is a set of normal
  vectors of a hyperplane arrangement $\sA$ and $v$ corresponds to the
  chamber $c$ of $\sA$.
\end{defn}

\begin{example} Revisiting Example ~\ref{ex:matroid-realized} we
  recall the vector configuration
\[
V=
\bordermatrix{
& v_1 & v_2 & v_3 \cr
&1 & 0 & 1 \cr
&0 & 1 & 1 \cr
}.
\]
Figure ~\ref{fig:zonotope-simple} shows that the zonotope is a hexagon and 
we pick the vertex $+++$ to make $(Z,v)$ a pointed zonotope. We see
the galleries from Example ~\ref{ex:gallery-simple} as the two paths
between $---$ and $+++$ and the flip between them is the lone $\Z{V}$ since $V$
has rank $2$. 
\label{ex:zonotope-simple}
\end{example}

\begin{figure}
\input{figures/zonotope-simple.tex}
\caption{$(\Z{V},+++)$ and $\gtwo{\Z{V},+++}$ for example
~\ref{ex:zonotope-simple}}
\label{fig:zonotope-simple}
\end{figure}

We can easily see galleries in flips in dimension $2$ and $3$ however,
as we increase dimension we will need more powerful tools to
understand galleries and flips. To make
galleries easier to work with, we define and illustrate oriented
matroid duality. The idea of duality is to reverse the roles of
vectors and covectors of $\mat{}$ allowing us to understand the
covectors of $V$ by understanding the vectors of $V^*$. We must first
describe the vectors of $\mat{V}$ and will then build $V^*$ whose
vectors are covectors of $V$. The
vectors of $\mat{V}$ are the sign vectors of linear dependencies of $V$.

\begin{defn} A \emph{linear dependence} of $V$ is a vector $w\in \R{n}$ such
that $V\cdot w=0$, which we denote by:
  \[
  \dep{V}=\setof{w \in \R{n}\,:\,V\cdot w=0}\subset \R{n}.
  \]
\label{def:matroid-dependence}
\end{defn}
The vectors of $\mat{V}$ are the sign vectors of linear dependencies of
$V$, that is
\[
\mV{V}=\setof{\sgn{v}: \,v \in \dep{V}}.
\] 
The dual
vector configuration $V^*$ is a configuration of $n$ vectors whose
linear dependencies correspond to valuations of $V$. To find $V^*$ of
$V$ we must find a basis for the kernel of 
$V$. We will illustrate this with an example.

\begin{example} Building from Example ~\ref{ex:matroid-realized} we
understand the covectors of $V$ in terms of oriented matroid duality. Recall that
\[
V=
\bordermatrix{
& v_1 & v_2 & v_3 \cr
& 1 & 0 & 1 \cr
& 0 & 1 & 1 \cr
}.%\end{pmatrix}.
\]
We find a basis for $\ker{V}$ which we treat as a row vector.
\[
V^*=
\bordermatrix{
&v_1^* & v_2^* & v_3^* \cr
&1 & 1 & -1 \cr
}
\]
We saw the covector $+++$ of $V$ as a valuation of $V$ but we can now
also understand it as a linear dependence. The sum
$v_1^*+v_2^*+2v_3^*=0$ is a linear dependence of $V^*$ so $+++$ is a
vector of $V^*$ and a covector of $V$. Similarly we can understand all
covectors of $V$. As in Example ~\ref{ex:matroid-realized}, we write
each sign vector under the corresponding linear dependence:
\[
\mcV{}=\setof{
\begin{array}{ccc}
v_1^*+v_2^*+2v_3^*=0, &
v_2^*+v_3^*=0, &
-v_1^*+2v_2^*+v_3^*=0 \\
+++, &
0++, & 
-++, \\
\rule{0pt}{4ex}-v_1^*+v_2^*=0, & 
-2v_1^*+v_2^*-v_3^*=0, &
-v_1^*+v_3^*=0, \\
-+0, &
-+-, & 
-0-,\\
\rule{0pt}{4ex}-v_1^*-v_2^*-2v_3^*=0, &
-v_2^*-v_3^*=0, &
v_1^*-2v_2^*-v_3^*=0 \\
---, & 
0--, & 
+--\\
\rule{0pt}{4ex}v_1^*-v_2^*=0, &
2v_1^*-v_2^*+v_3^*=0, &
-v_1^*+v_3^*=0 \\
+-0, & 
+-+, & 
+0+\\
\rule{0pt}{4ex}& 0v_1^*+0v_2^*+0v_3^*=0,& \\  
&000  &
\end{array}
}.
\]
\label{ex:matroid-dual-vector-config}
\end{example}

The crucial idea is that valuations of $V$ correspond to linear
dependencies on $V^*$. Duality will be essential in many of our
definitions and proofs,
particularly: Definition ~\ref{def:gallery-coherent} and Lemmas and
Chapter ~\ref{s:methods}.
%% ~\ref{l:gallery-change-g}, ~\ref{l:orientation-lifting}, 
%% ~\ref{l:coherent-strings},
%% ~\ref{l:coherent-projection}, 
%%  and ~\ref{l:incoherent-lifting}.

Given $V$, a configuration of $n$ vectors in $\R{d}$, the dual $V^*$
is a configuration of $n$ vectors in $\R{n-d}$. When $n-d$ is small we
can replace vectors in $\R{n}$ with the dual vectors in $\R{n-d}$,
which we can visualize. Duality is the primary tool we use to
classify and understand low corank vector configurations and we
include a classification of pointed vector configuration in coranks 1
and 2 as Appendix ~\ref{a:agd-classification}. Our classification
includes the details of affine gale diagrams, however we will recall
the definition of single-element liftings extensions and recall a few
key highlights here.

\begin{defn} A vector configuration $V^+$ is a
  \emph{single-element extension} of $V$ when
  \[
  V^+=V \cup \setof{v_{n+1}}.
  \]
Single-element extensions are inverse to oriented matroid deletion and we recall $\sA=\sAe \delete \setof{\ai{n+1}}$.
\label{def:matroid-extension}
\end{defn}

We illustrate single-element extensions with a simple example.
\begin{example} The vector configuration $V^+$ is a single-element
extension of the vector configuration $V$ from Example ~\ref{ex:matroid-realized}
\[
V=
\bordermatrix{
& v_1 & v_2 & v_3 \cr
&1 & 0 & 1 \cr
&0 & 1 & 1 \cr
}
\qquad
V^+=
\bordermatrix{
& v_1 & v_2 & v_3 & v_4 \cr
&1 & 0 & 1 & 1\cr
&0 & 1 & 1 & 2\cr
}
\]
\end{example}


\begin{defn} We say that a vector configuration 
  $\widehat{V} = \setof{\widehat{v}_i\in \R{d+1} \,\big|\, 1\le i \le
  n+1}$ is a \emph{single-element lifting} of
  $V=\setof{v_i \in \R{d} \,\big|\, 1 \le i \le n}$ if there is a
  linear surjection $\R{d+1} \xmapsto{\pi} \R{d}$ so
  that 
  \[ 
  \begin{split} 
  \pi(\hat{v}_{n+1})&=0 \\ 
  \pi(\hat{v}_i)&=c_i \cdot  v_i \, 1 \le i \le n 
  \end{split} 
  \] Without loss of generality, we
  typically assume that $c_i=1$. Single-element liftings are inverse
  to oriented matroid contractions ~\cite[Prop. 6.11]{Ziegler95} and
  we recall that $\sA=\sAl\contract \setof{\ai{n+1}}$. 
\label{def:matroid-lifting}
\end{defn}

When $\gale \in \Gamma(\sAe,c)$ we write $\gale\delete(\ai{n+1})$ for
the gallery of $(\sA,c\delete(\ai{n+1})$ obtained by deleting the
hyperplane corresponding to $\ai{n+1}$. Likewise when
$\gall \in \Gamma(\sAl,c)$ we write $\gale\contract(\ai{n+1})$ for 
the gallery of $(\sA,c\contract(\ai{n+1})$ obtained the contraction of
$\ai{n+1}$. We will later show liftings $\gall$ and extensions $\gale$ of a
gallery $\gal$ exist using deletion and contraction ideas and write
$\gal=\gall\contract \ai{n+1}$ and
$\gal=\gale \delete \ai{n+1}$. Confusingly $\gall\contract \ai{n+1}$
and $\gale \delete \ai{n+1}$ are equal as words in
$\setof{\ai{i}}$ but we retain the deletion and contraction notation
as a reminder of how we obtain $\sAl$ or $\sAe$ from $\sA$. 

In comparison to single-element extensions, single-element liftings
have two desirable properties.
\begin{itemize}
\item Single element-liftings preserve corank making them useful for
  our classification and Lemma ~\ref{l:incoherent-lifting} in particular.
\item Acyclic orientations of $\sA$ can always be lifted to acyclic
  orientations of $\sAl$, using Lemma ~\ref{l:orientation-lifting}. 
\end{itemize}

We illustrate with an example of single-element liftings.

\begin{example}
The configurations
\[
\widehat{V}=
\begin{pmatrix}
1 & 0 & 0 & 1 \\
0 & 1 & 0 & 1 \\
0 & 0 & 1 & 1 \\
\end{pmatrix}
\quad
\text{ and }
\quad
V=
\begin{pmatrix}
1 & 0 & 1 \\
0 & 1 & 1 \\
\end{pmatrix}
\]
are related by the linear surjection
\[
\pi(x)=
\begin{pmatrix}
1 & 0 \\
0 & 1 \\
1 & 1 \\
0 & 0 \\
\end{pmatrix}
x.
\]
We know $\widehat{V}$ is a single-element lifting of $V$ because
$\pi(\widehat{V})=V$. 
 %% In the corank 1 classification, $V$ is $n=3,k=1$
%% while $V^+$ is $n=4,k=1$ because liftings preserve corank.
\label{ex:matroid-lifting}
\end{example}

The relationship between single-element liftings and single-element
extensions is duality. 
\begin{itemize}
\item If $\widehat{V}$ is a single-element lifting of $V$ then 
$\widehat{V}^*$ is a single-element extension of $V^*$
  \[
  \left(\widehat{V}\right)^*=\left(V^*\right)^+.
  \]
\item If $V^+$ is a single-element extension of $V$ then 
$\left(V^+\right)^*$ is a single-element lifting of $V^*$
  \[
  \left(V^+\right)^*=\widehat{\left(V^*\right)}.
  \]
\end{itemize}

\begin{proposition} The following facts allow us to produce a
  classification of pointed vector configurations in coranks $1$ and
  $2$. Explanation of these facts along with a detailed classification
  can be found in Appendix ~\ref{a:agd-classification}
\begin{itemize}
\item In corank $1$, pointed vector configurations are uniquely
determined by the number $n$ of vectors in $V$ and the number $k$ of
  negative vectors in $V^*$.
\item Affine Gale diagrams describe dual vector configuration in
  coranks $1$ and $2$. Our
  convention when drawing affine Gale diagrams will be to
  draw positive vectors as black dots and negative vectors as white
  dots. We will draw affine Gale diagrams so that classes of parallel
  vectors will ``line up'' on the $x$ axis, while non parallel vectors
  have distinct $y$ values form a ``stack''.
\item We organize our classification by single-element liftings of
  $V$ but draw pictures of $V^*$ and liftings of $V$ are extensions of
  $V^*$. 
\end{itemize}
\end{proposition}

\begin{example} As an illustration of affine Gale diagram we introduce
a vector configuration that will be the focus of Section ~\ref{ss:cr2-exceptional}. It consists of $6$ vectors in $\R{4}$,
\[
V=\bordermatrix{
& v_1 & v_2 & v_3 & v_4 & v_5 & v_6 \cr
& 1 & 0 & 0 & 0 &  1 & 2 \cr
& 0 & 1 & 0 & 0 & -1 & -1 \cr
& 0 & 0 & 1 & 0 & -1 & -1 \cr
& 0 & 0 & 0 & 1 & -1 & 0 \cr
}
\quad
\text{ with dual, }
V^*=\bordermatrix{
& v_1^* & v_2^* & v_3^* & v_4^* & v_5^* & v_6^* \cr
& 0  & 1 & 1 & 1 & 1 &  1 \cr
&-1  & 1 & 1 & 0 & 0 & -1 \cr
}.
\]
There are 4
parallelism classes of $V^*$ and the first 5 vectors are in standard
position; $v_6^*$ has a cross-ratio of $\mu=1$. Depending
on the choice of affine hyperplane we can have multiple equivalent
affine Gale diagrams, as in Figure ~\ref{fig:matroid-gale-vectors}.
\label{ex:matroid-agd}
\end{example}


\begin{figure}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=1.5]
\draw[->,dual] (0,0) -- (0,-1);
\draw (0,-1.2) node {$\aid{1}$};
\draw[->>,dual] (0,0) -- (.7,.7);
\draw (1.0,.85) node {$\aid{2},\aid{3}$};
\draw[->>,dual] (0,0) -- (1,0);
\draw (1.35,0) node {$\aid{4},\aid{5}$};
\draw[->,dual] (0,0) -- (-.7,.7);
\draw (-.8,.85) node {$\aid{6}$};
\draw[dashed] (0,1) -- (1, -1);
\draw  (1.1, -1) node {$\mathcal{L}$};
\draw[dashed] (-1,.7) -- (1, -.4);
\draw  (1.15, -.4) node {$\mathcal{L}^\prime$};
\end{tikzpicture}
%\includegraphics[scale=.25]{figures/matroid-Gale-vectors} \\
\end{minipage}
\begin{minipage}{.45\linewidth}
\centering
\begin{tabular}{m{2cm}m{3cm}}
\includegraphics[scale=.5]{figures/matroid-gale-diagram-1} 
&
using $\mathcal{L}$
\\%\vfill \\
\includegraphics[scale=.5]{figures/matroid-gale-diagram-2} 
&
%\null\vfill
using 
$\mathcal{L}^\prime$
%\vfill
\end{tabular}
\end{minipage}
\caption{$V^*$ as a vector configuration and two of its affine Gale diagram}
\label{fig:matroid-gale-vectors}
\end{figure}

\begin{example} To illustrate the classification of Appendix
~\ref{a:agd-classification} we highlight the details of $5$ vectors in $\R{3}$
  when $V^*$ has no parallelism. The dual vector configuration will be
  $5$ vectors in $\R{2}$ and the affine Gale diagram will consist of
  $5$ distinct signed vectors, each colored black or white. Initially
  there are $2^5=32$ ways to color these $5$ vectors which we break up
  into equivalence classes. Affine Gale
  diagrams are equivalent when one can be obtained from the other by
  \begin{itemize} 
    \item Pulling a row of dots
  from the bottom of the diagram, changing the color of every dot,
  inserting it on top of the pile
    \item Turning the pile upside down, reversing the order of the
      rows.
   \end{itemize}
Repeatedly applying these two rules we break the $32$ affine Gale
  duals into the $4$ equivalence classes listed in Table
  ~\ref{tab:cr2-classification}.
\label{ex:cr2-classification}
\end{example}

\begin{table}
%\newcolumntype{M}[1]{>{\raggedright}m{#1}}
\begin{tabular}{|c|c|}
\hline
diagram & \text{size of eq. class} \\
\hline
 \raisebox{-.45\totalheight}{
\includegraphics[scale=.25]{figures/cr2-classification-ex-1}} %\vspace{-.25cm}}
& 10 \\
\hline
 \raisebox{-.45\totalheight}{
\includegraphics[scale=.25]{figures/cr2-classification-ex-2}}%\vspace{-.25cm}
& 10 \\
\hline
\raisebox{-.45\totalheight}{
\includegraphics[scale=.25]{figures/cr2-classification-ex-3}}% \vspace{-.25cm}
& 10 \\
\hline
\raisebox{-.45\totalheight}{
\includegraphics[scale=.25]{figures/cr2-classification-ex-4}}% \vspace{-.25cm}
& 2 \\
\hline
\end{tabular}
\vskip .1cm
\caption{Vector configurations of $5$ vectors in $\R{3}$ with no
parallelism}
\label{tab:cr2-classification}
\end{table}

Example ~\ref{ex:cr2-classification} illustrates classifications in
low corank are possible and useful, but tedious and best left to a
computer, which is just how we produced the Tables in
~\ref{a:agd-classification}. 
