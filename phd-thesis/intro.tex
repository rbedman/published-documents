In this thesis we study monotone path graphs of zonotopes and
hyperplane arrangements in low corank. For a $d$-dimensional,
centrally symmetric, zonotope in $\Z{} \subset \R{d}$, a linear
functional $\f{} \in \Rd{d}$, which is non constant along every edge
of $\Z{}$ determines unique $\f{}$-minimal and $\f{}$-maximum
vertices of $\Z{}$, $-z$ and $z$ respectively.  Paths along the edges
of $\Z{}$ from $-z$ to $z$ for which $\f{}$ is strictly increasing are
$\f{}$-monotone paths. A monotone path is coherent if it lies on the
boundary of a polygon obtained by projecting $\Z{}$ to
2-dimensions. The set of all $\f{}$-monotone paths form a geometrically
distinguished subset of the vertices of the monotone path graph.

More generally a cellular string $\cell{}$ of $\Z{}$ is a sequence of
faces of $\Z{}$, called cells, in which consecutive faces $\cell{i}$
and $\cell{i+1}$ share a single vertex $z_i$, and the sequence of
common vertices are $(z_i)$ is $\f{}$-monotone. An $\f{}$-monotone
path is a cellular string of maximal length, in which each cell is an
edge of $\Z{}$. A flip is a cellular string in which a distinguished cell is
a $2$-face of $\Z{}$ and all other cells are edges. Cellular strings,
like $\f{}$-monotone paths, are coherent when they lay on the boundary
of the polygon obtained by projecting $\Z{}$ to 2-dimensions.

Given a hyperplane arrangement, $\sA$ consisting of $n$ hyperplanes in
$\Rd{d}$ we build the $d$-dimensional zonotope
$\Z{\sA} \subset \R{d}$, whose $d-k$-faces correspond to
intersections of $k$ hyperplanes. In this polytope, vertices correspond
to chambers of $\sA$, and edges are slices of hyperplanes which divide
two chambers. A function $\f{} \in \Rd{d}$ which is non-constant along
every edge of $\Z{\sA}$ determines a unique vertex $z$ of $\Z{\sA}$
or, equivalently, a unique chamber $c$ of $\sA$. A path $\gal$ from $-c$ to
$c$ in $\Rd{d}$ is a gallery of $\sA$ and is a minimal gallery when
$\gal$ has minimal length.

The set of all minimal galleries of $\sA$ is the vertex set of a graph
$\gtwo{\sA,c}$ in which two minimal galleries are adjacent when
they differ only in the path they take around an intersection of two
hyperplanes of $\sA$.  Minimal galleries correspond to $\f{}$-monotone
paths and intersections of hyperplanes correspond to faces of
$\Z{\sA}$, so the graph $\gtwo{\sA,c}$ is equal to the graph of
$\f{}$-monotone paths of $\sA$. 

%% The galleries of $\sA$ are
%% coherent when the corresponding $\f{}$-monotone path of $\Z{\sA}$ is
%% coherent for $\f{}$. 


%% We realize a hyperplane arrangement as a $d \times n$ matrix, $A$, whose
%% columns are normal to the hyperplanes of $\sA$. Let
%% $\f{}\in \Rd{d}$ be a functional which is strictly positive on 
%% every column of $\sA$ and
%% which takes its maximum and minimum on vertices $z$ and $-z$ of
%% $\Z{\sA}$. Minimal galleries of $\sA$ correspond to $\f{}$-monotone
%% paths on $\Z{\sA}$ and 

%Figure
\begin{figure}
\begin{minipage}{.45\linewidth}
\input{figures/zonotope-3cube.tex}
\end{minipage}
\begin{minipage}{.45\linewidth}
\input{figures/mpg-3permutohedron.tex}
\end{minipage}
\caption{Zonotope (3-cube) and Monotone path graph (Permutohedron)}
\label{fig:intro-cube}
\end{figure}

We realize a hyperplane arrangement as a $d \times n$ matrix, $A$,
whose columns are normal to the hyperplanes of $\sA$. The corank of
$\sA$ is $n-d$ so corank $0$ consists of $d$ hyperplanes in
$\R{d}$. The zonotope of corank $0$ hyperplane arrangements $\sA$ is a
$d$-cube and for a generic $\f{}$ distinguishing a chamber $c$, the
graph $\gtwo{\sA,c}$ is the $d$-permutohedron, as illustrated in
Figure ~\ref{fig:intro-cube}. Every gallery in $\sA$ is coherent for
$\f{}$ and the Permutohedron is a polytope ~\cite[Section
4]{Billera94}. In contrast, for higher corank hyperplane arrangements,
the monotone path graph may contain incoherent galleries and will not
be a polytope. Our major results characterize the monotone path graphs
which contain incoherent $\f{}$-monotone paths in corank 1 and 2.

Behind both $\f{}$-monotone paths of a zonotope and minimal galleries
of a hyperplane arrangement there is a rank $d$ acyclically oriented
matroid $\mat{}$. Section ~\ref{ss:level3} reviews the relevant
details of oriented matroids. Importantly 
$\mat{} = \bigsqcup_{i=0}^d \mat{}_i$ is the lattice of flats in which
$\mat{}_i$ consists of all corank $i$ covectors of $\mat{}$. Each
corank $k$ covector of $\mat{}$ corresponds to a $k$-face of $Z$
which in turn gives rise to a $(d-k)$ dimensional intersection of
hyperplanes $L_k$. 
Since $\mat{}$ is acyclically oriented,
$(+)^n \in \mat{}_0$ is a maximal covector of $\mat{}$ and  maximal
covectors correspond to vertices of $\Z{}$ (or equivalently chambers of
$\sA$). Each edge of $Z$ corresponds to a corank $1$ covector of
$\mat{}$ and a gallery is a sequence of corank 1 covectors satisfying
a monotone property which we will make explicit in Definition
~\ref{def:cellular-3} The vertices of the graph
$\gtwo{\mat{},(+)^n}$ are the galleries of $\mat{}$; two galleries
are adjacent when they differ by an element of $\mat{}_2$.

%A cellular string $\sigma$ of $\mat{}$ is a sequence of covectors 
The diameter of $\gtwo{\sA,c}$ can be understood geometrically. Let
$L=\bigsqcup_{i=0}^d L_i$ be the graded poset of intersection subspaces
of $\sA$, ordered by reverse inclusion. Every element of $c \in \mat{}_2$
gives rise to an $X \in L_2$ however $X$ does not uniquely determine
$c$. We obtain a lower bound on the diameter of $\gtwo{\sA,c}$ by
noticing that a gallery $\gal$ and its reversal are $L_2$ flips
apart, and that these flips are well defined as a subset of $L_2$ but
not as a subset of $\mat{}_2$. In
~\cite{Reiner12} Reiner and Roichman asked ``For real hyperplane
arrangements $\sA$ and a choice of base chamber $c$, does the graph
$G_2$ of minimal galleries from $-c$ to $c$ have diameter exactly
$\abs{L_2}$?''  There are partial answers to this question, with
~\cite{Cordovil00} showing equality when $\sA$ is a hyperplane
arrangement in $\R{3}$. The diameter equals $\abs{L_2}$ when $\sA$ is a
 supersolveable hyperplane arrangement in ~\cite{Reiner12}. We add
to these results by proving that the diameter equals $\abs{L_2}$ when
$\sA$ has corank $1$.

It is harder to find examples in which the diameter strictly exceeds
the lower bound of $\abs{L_2}$. An
example of Richter-Gerbert provides one example as a 
rank $4$ oriented matroid ~\cite{Richter_Gebert93}. We improve on this
result by providing a realized hyperplane arrangement consisting of
$8$ hyperplanes in $\R{4}$.

\begin{example} 
To illustrate the structure of monotone path graphs we look at the 3
irreducible reflection arrangements in $\R{3}$, presented in Figure
~\ref{fig:intro-irreducible}. Although the graphs vary in
complexity, they seem to be ``bubbly'' versions of a regular
$2\abs{L_2}$-gon in $\R{2}$. Incoherent galleries of $\sA$ look like
nodes inside the $2\abs{L_2}$-gon, forming bubbles. We notice adding
hyperplanes increases corank and adds bubbles.
\label{ex:intro-reflections}
\end{example} 

\begin{figure}
\begin{tabular}{cccc}
&
%% \input{figures/mpg-n6-A3.tex} &
%% \input{figures/mpg-n9-B3.tex} &
%% \input{figures/mpg-n15-H3.tex} \\
\includegraphics[width=.25\linewidth]{figures/A3_reducedwords_graph} & 
\includegraphics[width=.25\linewidth]{figures/B3_reducedwords_graph} & 
\includegraphics[width=.25\linewidth]{figures/H3_reducedwords_graph} \\
 & $A_3$ & $B_3$ & $H_3$ \\
corank=$n-3$ & 3 & 6 & 12\\
\# of min. galleries & 16 & 42 & 152 \\
$\abs{L_2}$  & 7 & 13 & 31 \\
\end{tabular}
\caption{Monotone path graphs of irreducible reflection arrangements
in $\R{3}$. }
\label{fig:intro-irreducible}
\end{figure}

We have organized this document as follows: In Chapter
~\ref{s:background}, we provide a self-contained description of the
monotone path graph using the language of oriented matroids and
pointed hyperplane arrangements. We begin with the relevant oriented matroid definitions in
Section ~\ref{ss:level3}. Section ~\ref{ss:level2} focuses on oriented
matroids realized by vector configurations and
hyperplane arrangements and develops our understanding of galleries
using Gale duality. We include highlights of the
classification of affine Gale diagrams, but leave details for the
supporting material in Appendix ~\ref{a:agd-classification}. The 
classification matches that of ~\cite{McMullen71} and
~\cite{Grünbaum67} but is specifically tailored for pointed hyperplane
configurations and illustrates families
of monotone path graphs. Section ~\ref{ss:level1} concludes the
background material by giving the details of $\f{}$-monotone
paths and coherence. 

Section ~\ref{s:interlude} is an interlude summarizing the
ideas of Chapter ~\ref{s:background}. Readers with some background in
oriented matroids and monotone path graphs may choose to begin reading
in this section as it contains the first new material. We say that $(Z,\f{})$
is \emph{all-coherent} for the special situation that every cellular string 
is coherent and $(Z,v)$ universally all-coherent
when $(Z,f)$ is all-coherent for every $(Z,f)$ realizing
$(Z,v)$. We conclude by reviewing the definitions of $L_2$-accessibility and a
key proposition of ~\cite{Reiner12} which uses $L_2$-accessibility to
compute the diameter of the monotone path graph.

The theoretical tools of this dissertation are in Chapter
~\ref{s:methods}, which uses the definitions of
Chapter ~\ref{s:interlude} to prove five key lemmas. These technical
lemmas allow us prove the existence of 
incoherent galleries for hyperplane arrangements which are either
single-element extensions or single-element liftings of previously
understood hyperplane arrangements. Of these 5 lemmas, two of them,
Lemmas ~\ref{l:coherent-strings} and ~\ref{l:incoherent-lifting} are
particularly subtle.

The rewards of Chapters ~\ref{s:interlude} and ~\ref{s:methods} come
in Chapters ~\ref{s:crank1} and ~\ref{s:crank2}, which contain the
main results of this dissertation. Specifically, Theorem
~\ref{thm:cr1-classification} gives a complete classification of
monotone path graphs in corank 1. Our classification identifies a
unique family of corank 1, universally all-coherent pointed hyperplane
arrangements. For all other monotone path graphs, we find an
incoherent $\f{}$-monotone path by identifying the smallest monotone
path graph which contains an incoherent path for any $\f{}$, then use
the results of Section ~\ref{ss:incoherent-lifting} to lift this
incoherent path to all monotone path graphs outside of our previously
identified all-coherent family. Finally, in Theorem
~\ref{thm:cr1-diameter}, we prove the diameter is $\abs{L_2}$ for
all monotone path graphs of corank 1, arguing that the family of
all-coherent gallery is itself a zonotope and thus its diameter is
$\abs{L_2}$ and finding a particular $L_2$-accessible gallery in all
corank 1 hyperplane arrangements.

Chapter ~\ref{s:crank2} provides a complete classification of monotone
path graphs in corank $2$.  In Theorem ~\ref{thm:crank2-main-theorem}
we identify two families of universally all-coherent monotone path
graphs and give a set of minimal obstructions to show that all other
monotone path graphs contain at least one incoherent path for every
$\f{}$. In contrast to Chapter ~\ref{s:crank1} we are unable to use
methods of $L_2$-accessibility to prove that the diameter equal
$\abs{L_2}$ in corank 2, which we discuss further in Section
~\ref{s:unsolved}.

Chapter ~\ref{s:unsolved} presents a list of open questions, each
motivated with an example. These open questions serve as a repository of
hyperplane arrangements results which
have motivated our thinking but found no other home in this
dissertation. Chapter ~\ref{s:unsolved} also presents some minor
theoretical and computational results we collected in the course of
our research. Proposition ~\ref{prop:unsolved-cyclic} points out that
our work also classifies cyclic hyperplane arrangements, and that the
classification vaguely resembles Theorem 1.1 of
~\cite{Athanasiadis00}. 
Some of our computational are 
previously unpublished, while others are confirmations of previously
known results. The two new examples are:
\begin{itemize}
\item Example ~\ref{ex:unsolved-noL2} is a hyperplane
arrangement in corank $2$, with a pleasantly symmetric monotone path
graph, yet with no $L_2$-accessible galleries. 
\item Example~\ref{ex:unsolved-dbound} is a realized cyclic hyperplane
arrangement with $d=4$ and $n=8$ with diameter $30$ but $\abs{L_2}$
strictly less at $28$.
\end{itemize}

Finally, we hope that readers will find the appendices of this
dissertation useful and amusing. Appendix ~\ref{a:agd-classification}
contains our classification of zonotopes of corank $1$ and
$2$, a resource we wish was available to us at the outset of our
research. Appendix ~\ref{a:summary-everyone} contains a rephrasing
of monotone path problems as two recreational math
problems for non-technical audiences.

