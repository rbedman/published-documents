%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mnthesis.cls: mnthesis document class (Latex)
% Les Clowney ----- modified by K. Thorne for UofMinn
%
%  5/12/89   make version for University of Minnesota
%  11/23/89  modifications for 11-pt version for UofM
%  2/14/90   correct entries to table of contents
%  3/25/94   add in signature and abstract signature pages. -Jeff Nelson
%  5/4/2005  Modify to latex2e class format from old style format - BPS
%  5/24/2007 Fix the title and abstract page formatting. - BPS
% 11/17/2009 Fixed ordering of acknowledgments and abstract as required - PP
% 11/17/2009 'References' added to Table of Contents as required - PP
% 11/17/2009 Note: Table of content and list of Figures are ok in 'draft' format, not in 'final' (which is default) - PP
% 5/21/2010 Added Dedication - EB
% 5/24/2010 Added section to correctly give M.S. thesis or Ph.D. dissertation - EB
% 11/23/2010 Fixed the issue with restarting the numbering of pages if both Acknowledgement and Dedication pages are present
%            If Dedication page alone is present please uncomment line 406   - Sriram Doraiswamy
% 11/23/2010 Fixed the problem where additional pages of Acknowledgement were not correctly numbered - Sriram Doraiswamy
% 11/25/2010 Name of the advisor and Master of Arts or Science appears properly - Sriram Doraiswamy
% 2/20/2015 Modified not conflict with tikz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary of New Commands (many are not used outside of the style file)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The following are primarily for the titlepage et al....
%\draft			(this invokes the macro \draftrue)
%\copyrightpage		(make a copyright page)
%\figurespage		(make a page for list of figures)
%\tablespage		(make a page for list of tables)
%\title{thesis title}	(this is defined by the report style)
%\author{author's name}	(this is defined by the report style)
%\phd			(if invoked then \phdtrue is set.)
%\degree{degree type}	(Default "Master of Science")
%
%\words{words in abstract}
%\abstract{the body of the abstract}
%\acknowledgements{the body of the acknowledgments}
%\dedication{the body of the dedication}
%\director{the principal advisor's name}
%\submissionyear{year of submission to the Graduate School}
%\submissionmonth{month of submission to the Graduate School}
%(The default dates used will be those at which the document was prepared)
%\begin-end{vita}  (begin single spacing after this point for the vita)
%
%       ******* Booleans *******
%\ifpagestyletopright	(invoke \pagestyle{topright})
%\ifdraft		(Will do some things differently if draft. Set by \draft)
%\ifcopyright		(Add a copyright notice? (Set by \copyrightpage)
%\ifabstract		(set if command \abstract invoked)
%\ifpreface		(set if command \preface invoked)
%\ifextra		(set if command \extra invoked)
%\ifacknowledgements	(set by \acknowledgements)
%\ifabstract		(set by \abstract)
%\iffigures
%\iftables
%\ifafterpreface (afterpreface sections pagenumber must be at topright
%               corner. If user has chosen a header then it must be overridden.)
%               (Produce a List of figures? The default is to do so.)
%\tablestrue
%               (Produce a List of tables?)
%\begin/end{descriptionlist} (Basically a modified  \description.)
%\fullpagefigure Creates a figure where the page is a vbox whose
%                height is \textheight.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redefine the way that LaTeX starts up so that its simpler to use.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{report.cls}\relax
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






  




