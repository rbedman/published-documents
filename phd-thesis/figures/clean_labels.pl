#!/usr/bin/perl

use strict;
use warnings;
use File::Temp qw/ tempfile /;
use File::Copy qw/ move /;
my $nodestyle = "node[shape=none, margin=0];";
my $arg;
foreach $arg (@ARGV) {
    open(my $rfh, $arg) or warn "Can't open $arg!";
    my ($tfh, $tmpfile) = tempfile() or warn "Can't open temp file $arg.tmp";
    while ( my $line = <$rfh> ) {
	if($line =~ /graph \{/) {
	    print $tfh "graph g {\n\t$nodestyle\n";
	} else {
	    #regular expressions to do the following
	    # remove labels from nodes
	    # remove \{, , \} from names
	    #$line =~ /s/"[label=\"\.*\"\];"/";"ig ;
	    #$line =~ s/\"({|,|\s|})*//ig;
	    $line =~ s/\{\$.*\$\};/\{\};/ig;
	    $line =~ s/draw=none/cnode/ig;
	    #$line =~ s/$\{(\,\s*)*\}\$;/\{\};/ig ;
	    #\node ({5+ 2+ 4+ 1+ 3+ 6}) at (255.6bp,18bp) [draw,draw=none] {${5, 2, 4, 1, 3, 6}$};
	    print $line;
	    print $tfh $line;
	}
    }
    close($rfh);
    close($tfh);
    move( $tmpfile, $arg); #  autodie dies on error
}
