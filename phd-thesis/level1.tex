We now focus on coherent $\f{}$-monotone paths. This is a level of
detail which we have not previously used and requires working directly
with functions $\f{}$ rather than the sign vectors of valuations of
$\f{}$. In Definition ~\ref{def:gallery-2} we noted that pointed
zonotopes $(\Z{},v)$ and hyperplane arrangements $(\sA,c)$ realize
acyclically oriented matroids $(\mat{},(+)^n)$. Specification of a
chamber of $c$, or vertex of $v$, was equivalent to specifying maximal
covector of $\mat{V}$, which we could assume was $(+)^n$ without loss
of generality. Chambers of $\sA$, vertices of $\Z{\sA}$, and maximal
covector of $\mat{\sA}$ are all sign vectors of valuations of $\f{}$
on $V$. When the  vertex $v$ of $\Z{V}$ is the sign vector of a valuation of
the functional $\f{}$ on $V$, an $\f{}$-monotone path is a gallery $(Z,v)$.

\begin{defn} Given $\Z{}$, a $d$-dimensional, centrally symmetric
zonotope and $\f{}\in \Rd{d}$, function on $\Z{}$, we say the pair
$(\Z{},\f{})$ is a \emph{generic function on $\Z{}$} if $\f{}$ is
non-constant on every edge of $\Z{}$. Equivalently, when $\Z{}=\Z{V}$,
$\f{}$ is generic when $\f{}(v_i)>0$ for all $v_i \in V$, so $\f{}$
realizes the acyclic orientation on $\mat{V}$.
\end{defn}

A generic functional on a zonotope is directly analogous to an
acyclically oriented matroid, a pointed zonotope, or a pointed
hyperplane arrangement and allows us to define a cellular string of
$(\Z{},\f{})$.

\begin{defn} For $(\Z{V},\f{})$ a generic function on $\Z{}$ with
$-v$ and $v$ being the $\f{}$-minimal and $\f{}$-maximal
vertices of $\Z{}$ a \emph{cellular string}
$\cell{}=\cell{1}\big|\cell{2}\big|\ldots\big|\cell{m}$ is disjoint
union of $V$ with each block $\cell{i}$ being a zonotopal face
$\Z{\cell{i}}$ of $\Z{V}$ in which adjacent faces intersect in a
single vertex $\Z{\cell{i}} \cap \Z{\cell{i+1}}=v_i$ for 
$1 \le i <m$ and with
\begin{itemize}
\item $-v \in \Z{\cell{1}}$,
\item $v \in \Z{\cell{m}}$,
\item $\displaystyle f(-v) < f(v_1) < \cdots < f(v_m)<f(v)$.
\end{itemize}
%\label{l:coherent-cellular-strings}
\label{def:gallery-cellular-string} 
\end{defn}

\begin{figure}
\input{figures/zonotope-cellular-string.tex}
\caption{A cellular string of $(Z,\f{})$e.}
\label{fig:zonotope-cellular-string}
\end{figure}

It is clear that any cellular string of Definition
~\ref{def:cellular-3} is a cellular string of Definition
~\ref{def:gallery-cellular-string} and we say $(Z,f)$ realizes $(Z,v)$
or $(\sA,c)$. In this realization, $k$-faces of $\Z{V}$ correspond to
corank $k$ covectors of $\mat{V}$. The realization $\f{}$ of $(+)^n$
allows us to reinterpret the condition
$\cell{i}\circ(-)^n=\cell{i+1}\circ(+)^n$ as a series inequalities. We
now define $\f{}$-monotone paths as and $\f{}$-monotone flips in an
analogous way.

\begin{defn} Given $(\Z{},\f{})$ a generic function on $\Z{}$ a
\emph{$f$-monotone path} $\gal$ is a cellular string in which each cell is a
single vector of $V$. This nice case of a cellular string can be
expressed as an ordering of vertices $(v_0,\cdots,v_n)$ of $\Z{}$ so
that $v_i$ and $v_{i+1}$ share an edge of $\Z{}$ and which are $f$-monotone in the sense that  
\[
f(v_0) < f(v_1) < \cdots < f(v_n).
\]
Note that $v_0$ must be $\f{}$-minimal
vertex of $\Z{}$ and $v_n$ is the $\f{}$-maximal vertex of $\Z{}$.
\label{def:gallery-1}
\end{defn}

\begin{defn} Given $(\Z{},\f{})$ a generic function on $\Z{}$ a
\emph{flip} $X_F$is a cellular string $\cell{1}\big|\cdots\big|\cell{n-1}$ in
which a distinguished cell $F=\cell{i}$ is a face of $\Z{V}$ of
dimension $2$ and all other cells are contain a single vector of
$V$. We say that galleries $\gal$ and $\galp$ are adjacent by a flip
$X$ when both $\gal$ and $\galp$ are both refinements of $X_F$ and
sometimes refer to $X_F$ simply as $F$. 
\label{def:flip-1}
\end{defn}

\begin{defn} For $(\Z{},\f{})$ a generic function on $\Z{}$, an
$\f{}$-monotone path $\gal$ is \emph{coherent} if there exists a
$\g{}\in \Rd{d}$ which selects $\gal$ in the sense
\[
\frac{\gga{}(\aig{1})}{\f{}(\aig{1})} < \cdots
< \frac{\gga{}(\aig{n})}{\f{}(\aig{n})}.
\] 
\label{def:gallery-coherent}
\end{defn}

It is cumbersome to write $\g{}(v_i)$ repeatedly, so often use
shorthand for valuations of functions on $V$. We will write
$\g{i}=\g{}(\ai{i})$ and use $\gd{}=(\g{1},\ldots,\g{n}) \in \R{n}$ to
refer to the valuation of $\g{}$ on $V$. When a $\g{}$ exists making
an $\f{}$-monotone path $\gal$ coherent, we say that $\setof{\g{i}}$
are $\gal$ ordered. When there are no $\setof{\g{i}}$ we say that
$\gal$ is incoherent in $\sA$ for $f$.

We visualize a coherent $\f{}$-monotone path $\gal$ on $Z$ by
projecting $\R{} \to \R{2}$ via the map $x \to (\f{}(x),\g{}(x))$; the $\f{}$-monotone paths which are
coherent are those which appear on boundary of $\Z{}$ for some
projection. The incoherent $\f{}$-monotone paths are the paths which
wrap around $\Z{}$ to such an extent that no projection places them on
the boundary.

\begin{example} Continuing with the vector configuration last seen in
Example ~\ref{ex:zonotope-simple} we recall that
\[
V=
\bordermatrix{
& v_1 & v_2 & v_3 \cr
& 1 & 0 & 1 \cr
& 0 & 1 & 1 \cr
}.
\]
We realize the acyclic orientation $(+)^n$ of $\mat{V}$ using
$\f{}(x,y)=x+y$ and draw in Figure ~\ref{fig:zonotope-simple-1}. There are two $\f{}$-monotone paths of $(Z,f)$,
$\gal=132$ and $\galp=231$, which are the upper and lower faces of
$Z$. We make $\gal$ and $\galp$ explicitly coherent by
finding $\g{}(x,y)=-x+y$ and $\gp{}(x,y)=x-y$ and checking
\[
\begin{split}
\frac{\g{1}}{\f{1}}=
\frac{-1}{1}< 
\frac{\g{3}}{\f{3}}&=
\frac{0}{2}< 
\frac{\g{3}}{\f{3}}=
\frac{1}{1}, \text{ and } \\
\frac{\gp{2}}{\f{2}}=
\frac{-1}{1}< 
\frac{\gp{3}}{\f{3}}&=
\frac{0}{2}< 
\frac{\gp{1}}{\f{1}}=
\frac{1}{1}.\\
\end{split}
\]
In addition to $\gal$ and $\galp$ being coherent the unique flip
$X_{132}$ is also coherent and picked out by $\g{}(x,y)=0$. Every cellular string of $(Z,\f{})$ is coherent, so this is
our first example of what we will later define as \emph{all-coherent}
(see Definition ~\ref{def:all-coherent}. 
\label{ex:zonotope-simple-1}
\end{example}

\begin{figure}
\input{figures/zonotope-simple-1.tex}
\caption{Pointed zonotope from Example ~\ref{ex:zonotope-simple-1} with
$\g{}$ and $\gp{}$ shown.}
\label{fig:zonotope-simple-1}
\end{figure}

We can extend the definition of coherent to cellular strings of
$(\Z{},\f{})$. Cellular strings are coherent when there is a linear
functional $\g{}$ selecting all the zonotopal faces of $\cell{}$. 

\begin{defn} For $(\Z{V},\f{})$ a generic function on $\Z{}$,
a cellular string
$\cell{1}\big|\cell{2}\big|\ldots\big|\cell{m}$ of $(\Z{V},\f{})$, with
$V$ ordered so that
\[
\begin{split}
\cell{}&=
\underbrace{\setof{v_{1},\ldots,v_{\abs{\cell{1}}}}}_{\cell{1}}
\bigg|
\ldots
\bigg|
\underbrace{\setof{v_{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)},\ldots,v_{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}}_{\cell{k}}
\bigg|
\ldots
\bigg|
\underbrace{\setof{v_{1+n-\abs{\cell{n-m}}},\ldots,v_{n}}}_{\cell{n-m}}
\end{split}
\]
$\sigma$ is \emph{coherent} if there exists a $\g{}\in \Rd{d}$ which selects $\cell{}$ in the sense that
\[
\underbrace{
\frac{\g{1}}{\f{1}}=\cdots=
\frac{\g{\abs{\cell{1}}}}{\f{\abs{\cell{1}}}}
}_{\cell{1}} 
<
\cdots
<
\underbrace{
\frac{\g{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)}}
{\f{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)}}
=
\ldots
=
\frac{\g{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}
{\f{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}
}_{\cell{k}}
<
\cdots
<
\underbrace{
\frac{\g{n-\abs{\cell{{n-m}}}+1}}{\f{n-\abs{\cell{{n-m}}}+1}}=\cdots=
\frac{\g{n}}{\f{n}}
}_{\cell{n-m}} 
\]
\label{def:cellular-string-coherent}
\end{defn}

To show that an $\f{}$-monotone path $\gal$ or cellular string
$\sigma{}$ is coherent we must find a function $g$ which selects
$\gal$; Using Section ~\ref{ss:level2} we understand galleries of
$(\Z{V},v)$ with the dual $V^*$. We extend this understanding to
coherence of $\f{}$-monotone paths. Functionals on $V$ are linear
dependencies on $V^*$, so we coherence of an $\f{}$-monotone path is a
linear dependence which must satisfy inequalities.

\begin{lemma} An $\f{}$-monotone path $\gal$ is coherent for $\f{}$ if
and only if there exist a linear dependence
$\sum_{i=1}^n \g{i}\aid{i}=0$ with 
 $(g_1,\ldots,g_n) \in \R{n}$ such that
\[
\begin{split}
  \frac{\gga{1}}{\fg{1}} < 
  &\cdots < 
  \frac{\gga{n}}{\fg{n}} 
%   \mbox{ and } \\
%  \sum \g{i} & \aid{i} = 0\\
\end{split}
\]
%We will refer to equations ~\ref{eq:coh} as the coherence conditions. 
\end{lemma}

This lemma has an extension to coherent cellular strings which
we omit because of the complex notation involved in coherent cellular
string. The statement of the Lemma for cellular strings and the proof
are unsurprising. We also note that after Lemma
~\ref{l:coherent-strings} we will be able to prove coherence of a
cellular string by proving coherence of a cellular string $\cell{}$ by
proving coherence of any gallery $\gal$ which refines $\cell{}$.

\begin{proof} This proof is an exercise in duality. 
  To begin we will
  show that if $\gal$ is coherent and 
  let $g$ be a functional which selects $\gal$; define
  $\g{i}=\g{}(a_i)$ satisfy Definition
  ~\ref{def:gallery-coherent}. We then know that
\[
 \frac{\gga{1}}{\fg{1}} < 
 \cdots < 
 \frac{\gga{n}}{\fg{n}}  
\]
while duality tells us that 
\[
\sum_{i=1}^n \g{i}\aid{i}=0.
\]

We conclude that if $\gal$ is coherent, there exist the desired
$\setof{\g{i}}$. For the reverse, suppose there are $\setof{\g{i}}$ which satisfy Equation
~\ref{def:gallery-coherent}. By assumption we have
\[
 \frac{\gga{1}}{\fg{1}} < 
 \cdots < 
 \frac{\gga{n}}{\fg{n}}.
\]
Since $\displaystyle \sum_{i=1}^n \g{i}\aid{i}=0$ we know there
exists $\g{}\in \Rd{d}$.
\label{l:coh-change-g}
\end{proof}

We close with the reminder that \emph{galleries} depend only the pointed
zonotope $(Z,v)$, while \emph{coherent galleries} depend on which $f$
realizes $v$.  Coherent $\f{}$-monotone paths of $(Z,f)$ with coherent
flips form a subgraph of the monotone path graph, which is equal to
the graph $\gtwo{Z,v}$ when $\Z{}=\Z{V}$ and $\f{}$ realizes the
covector corresponding to $v$. What is not yet clear is how the choice
of $f$ dictates which galleries of $(Z,v)$ correspond to coherent
$\f{}$-monotone paths.

\begin{defn} Given $(\Z{},\f{})$ a generic function on $\Z{}=\Z{V}$ a with
$-v$ and $v$ being the $\f{}$-minimal and $\f{}$-maximal vertices of
$\Z{}$, the \emph{monotone path graph} is the graph whose vertices are
all $\f{}$-monotone paths of $(\Z{},\f{})$ in which two
$\f{}$-monotone paths are adjacent when they differ by a flip $X_F$.
\end{defn}

The monotone path graph of $(\Z{},\f{})$ is equal to the graph
$\gtwo{Z,v}$ when $(Z,\f{})$ realizes $(\Z{},v)$. We draw the
distinction between $\f{}$-monotone paths and galleries thus:
$\f{}$-monotone paths may be coherent (for a particular $\f{}$) however
galleries cannot be coherent, lacking any reference to $\f{}$.

