Our first lemma is about the $\f{}$-monotone paths of $\sA$, when
$\sA=\sA_1 \sqcup \sA_2$ is a disjoint union of hyperplanes. Disjoint
unions aren't enough to classify hyperplane arrangements but the
intuition provided by disjoint unions serves us well. We describe the
structure of 
$\gtwo{\sA_1 \sqcup \sA_2, (c,c')}$ explicitly in terms of
$\gtwo{\sA_1, c}$ and $\gtwo{\sA_2,c'}$ in Lemma
~\ref{l:product-graph}. As an immediate corollary, we will be able to
find $L_2$-accessible galleries when both $\sA_1$ and $\sA_2$ have $L_2$
accessible galleries. We will later return to disjoint unions in
Corollary ~\ref{c:incoherent-product} and describe when $\sA$ has incoherent
$\f{}$-monotone paths in terms of $\sA_1$ and $\sA_2$.

\begin{defn}
Given hyperplane arrangements $\sA$ consisting of $n$ hyperplanes in
$\R{d}$ and $\sAp$ consisting of $n^\prime$ hyperplanes in $\R{d^\prime}$,
the \emph{disjoint union} $\sA \bigsqcup \sA^\prime$ is the
arrangement of $n+n^\prime$ hyperplanes in $\R{d+d^\prime}$ formed by
embedding $\R{d}$ in $\R{d+d^\prime}$ as the first $d$ coordinates and $\R{d'}$
in $\R{d+d^\prime}$ as the last $d^\prime$ coordinates. We say that a
hyperplane arrangement is \emph{reducible} when it can be written as
$\sA \bigsqcup \sAp$ for some $\sA,\sAp$ in $\R{d},\R{d'}$
and \emph{irreducible} when there are no such $\sA$ and $\sAp$.
When $\sA$ or $\sAp$ is a single vector $\ai{n+1}$ in $\R{1}$ we say $\ai{n+1}$ is an \emph{isthmus}. 
\end{defn}

We remark that $\sA$ is reducible when it can be written as a block
matrix and functionals $\fl{}:\R{d+d^\prime} \to \R{}$ can be
decomposed into two functions $f_1:\R{d} \to \R{}$ and
$f_2:\R{d^\prime} \to \R{}$ with $\fl(x,x')=f_1(x)+f_2(x')$. We ask:
what is the structure of $\Z{\sA\sqcup \sAp}$ in terms of $\Z{\sA}$
and $\Z{\sAp}$. Isthmuses are an important example of this because the
dual of an isthmus is the zero vector. When $\sAp$ is an isthmus, we have:
\[
\left(\sA \bigsqcup \sA^\prime\right)^*=\sAd \cup \setof{
\begin{pmatrix}0 \\ \vdots \\ 0 \end{pmatrix}}.
\]


\begin{example} Our first example of disjoint unions is an isthmus, in
which $\sAp$ is a single hyperplane in $\R{1}$, and $\sA$ is the
arrangement of $3$ hyperplanes in $\R{2}$ from Example
~\ref{ex:hyperplane-ex}. We embed both $\sA$ and $\sAp$ into $\R{3}$
as
\[
\sA=
\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} \cr
& 1 & 0 & 1 \cr
& 0 & 1 & 1 \cr
& 0 & 0 & 0 \cr
},
\quad
\text{and}
\quad
\sAp=
\bordermatrix{
& \ai{4} \cr
& 0 \cr
& 0 \cr
& 1 \cr
}.
\]
We draw attention to $\aid{n+1}=0$, the dual of $\ai{n+1}$. The zonotope $\Z{\sAp}$ is simple; it is
the line segment. The zonotope $\Z{\sA}$ is already familiar
to us from Example ~\ref{ex:zonotope-simple} and consists of $6$
vertices and $6$ edges arranged as a hexagon in $\R{2}$. The zonotope
$\Z{\sA \sqcup \sAp}$ has $12$ vertices and $18$ edges; it is a prism
with a hexagonal base, as illustrated in Figure
~\ref{fig:product-zonotope}.
\label{ex:product-zonotope}
\end{example}
\begin{figure}
\centering
%% \begin{minipage}{.3\linewidth}
%% \input{figures/zonotope-disjoint-unionA.tex}
%% %\includegraphics[width=\linewidth]{figures/zonotope-disjoint-unionA}
%% \end{minipage}
%% +
%% \begin{minipage}{.1\linewidth}
%% \input{figures/zonotope-disjoint-unionB.tex}
%% %\includegraphics[width=\linewidth]{figures/zonotope-disjoint-unionB}
%% \end{minipage}
%% =
%\begin{minipage}{.5\linewidth}
\input{figures/zonotope-disjoint-unionC.tex}
%\includegraphics[width=\linewidth]{figures/zonotope-disjoint-unionC}
%\end{minipage}
\caption{Zonotopes of $\Z{\sA \sqcup \sAp}$ in Example ~\ref{ex:product-zonotope}}
\label{fig:product-zonotope}
\end{figure}

This motivates Lemma ~\ref{l:product-zonotope}, which identifies
$\Z{\sA \sqcup \sAp}$ as the Cartesian product of $\Z{\sA}$ and
$\Z{\sAp}$. While $\Z{\sA}$ is a polytope, we state our lemma in
purely graph theoretic terms. A zonotope version will also be true
but we limit our attention to the $1$-skeleton to use Definition
~\ref{def:gallery-1}.

Before we state Lemma ~\ref{l:product-zonotope}, we remind the reader
of the graph theoretic notion of a Cartesian product
~\cite{Hammack11}. Given graphs $G$ and $H$, the graph Cartesian
product $G \square H$ is the graph with
\[
\begin{split}
V(G \square H) &= V(G \square H) = V(G) \times V(H)\\
E(G \square H) &=
  \setof{
\left((v,w), \left(v^\prime,w^\prime\right)\right) \text{ with } \quad
\begin{split}
v=v^\prime\text{ and }&\left( w , w^\prime \right) \in E(H) \\
 & \text{ or }\\
w=w^\prime\text{ and }&\left( v , v^\prime \right) \in E(G)
\end{split}
}.
 \\
\end{split}
\]

\begin{lemma} If 
  $\sA=\setof{\ai{i},\ldots \ai{n}}$ and 
  $\sAp = \setof{\ai{n+1},\ldots \ai{n+n^\prime}}$ 
  in $\R{d^\prime}$ and $\R{d}$, with zonotopes $\Z{\sA}$ and
  $\Z{\sA^\prime}$ then 
\[
\gone{\sA \sqcup \sA^\prime} = \gone{\sA} \square \gone{\sA^\prime}.
\]
\label{l:product-zonotope}
\end{lemma}

\begin{proof} Covectors of $\mat{\sA \sqcup \sAp}$ are the vertices of
  $\gone{\sA \sqcup \sAp}$ and
  have the form
  $(c,c^\prime)$ in which $c$ is a covector of $\mat{\sA}$ and
  $c^\prime$ is a covector of $\mat{\sAp}$. 
   The vertex set of
  $V(\gone{\sA \sqcup \sA^\prime})$ is then the Cartesian product
  $V(\gone{\sA}) \times V(\gone{\sAp})$.

We know that two vertices $(c,c^\prime)$ and $(d,d^\prime)$
  are adjacent in $\gone{\sA \sqcup \sAp}$ when 
  ~$\abs{\sep{(c,c^\prime),(d,d^\prime)}}=1$ so $c=d$
  and $c^\prime$ and $d^\prime$ are adjacent in $\gone{\sAp}$ or
  $c^\prime = d^\prime$ and $c$ and $d$ are adjacent in
  $\gone{\sA}$. Thus $\gone{\sA \sqcup \sA^\prime}
  = \gone{\sA} \square \gone{\sA^\prime}$.
\end{proof}

Understanding $\gone{\sA\sqcup \sAp}$ as a graph product gives us a
way to understand the graph $\gtwo{\sA\sqcup\sAp,(c,c')}$. Vertices of $\gtwo{\sA,c}$ are paths on $\gone{\sA}$; we
investigate how paths on $\gone{\sA\sqcup \sAp}$ relate to paths on
$\gone{\sA}$ and $\gone{\sAp}$ before proceeding to
state and proving our next lemma.

\begin{example} Continuing from Example ~\ref{ex:product-zonotope} we
again have
\[
\sA=
\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} \cr
& 1 & 0 & 1 \cr
& 0 & 1 & 1 \cr
& 0 & 0 & 0 \cr
}
\quad
\text{and}
\quad
\sAp=
\bordermatrix{
& \ai{4} \cr
& 0 \cr
& 0 \cr
& 1 \cr
}
\]
Using the vertices $v=+++$ and $v'=+$ we compute $\gtwo{\sA \sqcup \sAp,(v,v')}$ from $\gtwo{\sA,v}$ and
$\gtwo{\sAp,v'}$. The graph $\gtwo{\sAp,+}$ consists of a
single gallery $\gamma=4$ and no edges. The monotone path graph of
$\sA$ consists of the two galleries $\gal=132$ and $\galp=231$ with a
single flip between them as we saw in Example ~\ref{ex:gallery-simple}.

The monotone path graph $\gtwo{\sA \sqcup \sAp}$ consists of $8$
galleries and $8$ edges (Figure ~\ref{fig:product-mpg}). We see that
the galleries of $\gtwo{\sA \sqcup \sAp,(c,c')}$ are the galleries of
$\gtwo{\sA,c}$ with a $4$ shuffled in.
\label{ex:product-shuffles}
\end{example}

\begin{figure}
\input{figures/mpg-n4-disjointC.tex}
\caption{Monotone path graph of the disjoint union}
\label{fig:product-mpg}
\end{figure}

We suspect that galleries of $(\sA \sqcup \sAp,(c,c'))$ will be shuffles of the
galleries of $(\sA,c)$ and the galleries of $(\sAp,c')$. Shuffles are
intuitive but need a precise definition that we adapt from ~\cite[p.70]{Stanley97} and ~\cite[p.482]{Stanley99}.

\begin{defn} A gallery $\gall$ of $(\sA \sqcup \sAp,(c,c'))$ is a
shuffle of a gallery $\gal$ of $(\sA,c)$ and
$\galp$ of $(\sAp,c')$ when both $\gal$ and $\galp$ are subsequences of $\gall$.
\end{defn}

Examples ~\ref{ex:product-zonotope} and
~\ref{ex:product-shuffles} made galleries clear using shuffle
products, but did not explain the flips of $\gtwo{\sA,c}$. We now
give a more in-depth example to illustrate the flips. 


\begin{example}  We extend Example ~\ref{ex:product-zonotope} to
better illustrate flips. We start with the hyperplane arrangement
\[
\begin{split}
\sA=\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} \cr
& 1 & 0 & 1 & 0 \cr
& 0 & 1 & 1 & 0 \cr
& 0 & 0 & 0 & 1 \cr
& 0 & 0 & 0 & 0 \cr
}
\quad \text{ and } \quad
\sA^\prime=
\bordermatrix{
 & \ai{5} \cr
 & 0 \cr
 & 0 \cr
 & 0 \cr
 & 1 \cr
}.
\end{split}
\]
We recognize $\sA$ from Example
~\ref{ex:product-shuffles}, so we understand $\gtwo{\sA,c}$ and $\sAp$
is an isthmus. Likewise, we know the
galleries of $\sA \sqcup \sAp$ are galleries $\gtwo{\sA,c}$ with
$5$ inserted. Figure ~\ref{fig:ptheorem-shuffleproduct} shows
$\gtwo{\sA \sqcup \sA^\prime}$, with vertices colored
according to how $5$ was shuffled into $\gal$. Notice that the top and
bottom, where galleries take the form $5\gal$ and $\gal5$, are copies
of $\gtwo{\sA,c}$
%sigma=(3,5,4) sigma^-1=(4,5,3)
In this picture we see that there is an edge $X_{1,2,3}$ between
$51324$ and $52314$ but not between 
$15324$ and $25314$ since $5$ separates the $132$ substring.
\end{example}

\begin{figure}
\begin{center}
\input{figures/mpg-disjoint-union.tex}
%\only<1>{\includegraphics[scale=.2]{figures/pretty-graph-pre-1} &}
%\includegraphics[scale=.2]{figures/pretty-graph-pre-2} &
%\only<1>{\includegraphics[scale=.2]{figures/pretty-graph-1} \\}
%\includegraphics[scale=.2]{figures/pretty-graph-2} \\
\end{center}
\caption{$\gtwo{\sA,c}$ illustrating flips for shuffle products.}
\label{fig:ptheorem-shuffleproduct}
\end{figure}

The flips of $\sA \sqcup \sAp$ are a subset of
the flips of $\sA$, the flips of $\sAp$, and changes in the shuffle. We
cannot apply flip $X_{i,\ldots k}$ to $\gal$ if $i$ and $k$ are not
adjacent in $\gal$. This allows us to define a flip between two
shuffles. 

\begin{proposition} Given two pointed hyperplane arrangements $(\sA,c)$
and $(\sAp,c')$, a flip $X_{\gall(i)\ldots,\gall(k)}$ of a gallery
$\gall=\gal \shuffle \galp$ for $\gal$ a gallery of $(\sA,c)$ and
$\galp$ a gallery of $(\sAp,c')$ has one of the three following forms:
\begin{itemize}
\item A flip $X$ from $(\mat{\sA},c)$ (with
  $\gal$ and $\galp$ both refinements of $X$).
\item A flip $X$ from $(\mat{\sAp},c')$ (with
  $\gal$ and $\galp$ both refinements of $X$).
\item A flip $X_{i,j}$ with $c_i$ a corank 1 covector of $\sA$ and
$c_j$ a corank 1 covector of  $\sAp$. 
\end{itemize}
\label{p:product-flips}
\end{proposition}

\begin{proof} 
This is clear from our understanding of the
galleries of $(\sA \sqcup \sAp,(c,c'))$. The corank two covectors of
$\sA \sqcup \sAp$ are the corank $2$ covectors of $\sA$, the corank
$2$ covectors of $\sAp$, and the new corank $2$ covectors corresponding to
the separation $\sep{c,c'}$ of corank $1$ covectors of $c \in \mat{\sA}$ and
$c' \in \mat{\sAp}$ respectively. Cellular strings are likewise built from
the cellular strings of $\sA$ and $\sAp$, giving flips the form
described above.
\end{proof}

Now that we understand galleries and the flips of
$(\sA \sqcup \sAp,(c,c'))$ we have a complete description of 
$\gtwo{\sA \sqcup \sAp,(c,c')}$.


\begin{lemma} For two pointed hyperplane arrangements $(\sA,c)$
and $(\sAp,c')$, the vertices of the graph 
$\gtwo{\sA\sqcup \sAp,(c,c')}$ are shuffles
$\gall=\gal \shuffle \galp$ for galleries $\gal$ of $(\sA,c)$ and
$\galp$ of $(\sAp,c')$. The edges of $\gtwo{\sA\sqcup \sAp,(c,c')}$
are the flips $X_{\gall(i)\ldots,\gall(k)}$ described in proposition ~\ref{p:product-flips}
\label{l:product-graph}
\end{lemma}

\begin{proof} The proof follows the construction. Galleries of
$\sA \sqcup \sAp$ are paths on $\gone{\sA \sqcup \sAp}$, which are in
turn shuffles of galleries of $(\sA,c)$ and $(\sAp,c')$. Adjacency
between monotone paths in $\sA \sqcup \sAp$ is either adjacency in
$(\sA,c)$, adjacency in $(\sAp,c')$, or a change in shuffle from
between $\sA$ and $\sAp$.
\end{proof}

While the proof of Lemma ~\ref{l:product-graph} is simple, it has a
important consequence. Since we understand the monotone path
graph of $\sA \sqcup \sAp$, we can write the graph distance
and the $L_2$ distance and understand $L_2$-accessibility for some
galleries.  

\begin{corollary} If $\gal$ is an $L_2$-accessible gallery in
  $\sA$ and $\galp$ is an $L_2$-accessible gallery in
  $\sAp$ then the trivial shuffle gallery $\gal\galp$ is $L_2$-accessible
  in $\sA \sqcup \sA^\prime$
\label{c:l2-product}
\end{corollary}

\begin{proof}
To show that $\gal=\gal_\sA\gal_\sAp$ is $L_2$-accessible we must describe the
separation set and the graph distance between $\gal\galp$ and another
gallery $\galp=\galp_\sA\shuffle\gal_\sAp$. Since both $\gal_\sA$ and
$\gal_\sAp$ are $L_2$-accessible we know the graph distance equals the
$L_2$ distance between $\gal_\sA$ and $\galp_\sA$ as well as
$\gal_\sAp$ and $\galp_\sAp$.  

To find a path between $\gal$ and $\galp$ we first take the path from
$\gal_\sA$ to $\galp_\sA$ in $\gtwo{\sA,c}$ which has graph distance
equal to $L_2$ distance. We then take the path from
$\gal_\sAp$ to $\galp_\sAp$ in $\gtwo{\sAp}$ which also has $L_2$
distance equal to graph distance. Finally, we shuffle $\galp_\sA$
$\galp_\sAp$ using a minimal number transpositions ~\cite{Stanley97}:
each transposition is a flip in
$\gtwo{\sA \sqcup \sAp}$ and that each flip is distinct from both
previous transpositions the paths in $\gtwo{\sA,c}$ and $\gtwo{\sAp,c'}$:
\[
\begin{split}
\gal&=\gal_\sA\gal_\sAp \\
    &\rightarrow \galp_\sA\gal_\sAp \\
    &\rightarrow \galp_\sA\galp_\sAp \\
    &\rightarrow \galp_\sA\shuffle\galp_\sAp.
\end{split}
\]
With an expression for distances of subpaths we can  compute both the
$L_2$ distance and the graph distance as a sum~\cite{Dijkstra59}. Both
the graph and $L_2$ distances were equal at every step so we 
conclude that $d(\gal,\galp)=d_{L_2}(\gal,\galp)$, so $\gal$ is $L_2$
accessible. 
\end{proof}

Lemma ~\ref{l:product-graph} provides a tool to compute diameter
by way of $L_2$-accessibility, but it is not powerful
enough for coherence. The following example shows that coherence
need not be preserved by disjoint unions.

\begin{example} We illustrate that a shuffle of two coherent monotone paths
may be incoherent. We use $\f{}_{\sA_1}=(1,1,1)=\f{}_{\sA_2}$ and 
\[
\sA_1=\sA_2=
\begin{pmatrix}
1 & 0 & 1/2 \\
0 & 1 & 1/2 \\
\end{pmatrix}
\]
So that $\f{}=(1,1,1,1,1,1)$ and 
\[
\sA=
\sA_1\bigsqcup \sA_2 =
\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} & \ai{5} & \ai{6} \cr
& 1 & 0 & 1/2 & 0 & 0 & 0 \cr
& 0 & 1 & 1/2 & 0 & 0 & 0 \cr
& 0 & 0 & 0 & 1 & 0 & 1/2 \cr
& 0 & 0 & 0 & 0 & 1 & 1/2 \cr
}.
\]

Since both $\sA_1$ and $\sA_2$ are arrangements in dimension $2$ each
arrangement only has two galleries. The $\f{}$-monotone paths $132$
and $465$ of $\sA_1$ and $\sA_2$ respectively are both coherent and, have $20$ shuffles,
which we illustrate in Figure ~\ref{fig:shuffle-segment}. We then know
that the shuffle $\gal=146325$ is an $\f{}$-monotone paths of
$\sA_1 \sqcup \sA_2$, but is not
coherent itself. Were $\gal$ coherent, there would be a function
$\g{}(x_1,x_2,x_3,x_4)=\g{1}x_1+\g{2}x_2+\g{3}x_3+\g{4}x_4$ whose
valuations would satisfying the following chain of inequalities.
\[
\g{1} < \g{4} < \frac{\g{3}+\g{4}}{2} < \frac{\g{1}+\g{2}}{2} < \g{2} < \g{4}.
\]
The left and right terms inequalities, $\g{1}<\g{3}$ and $\g{2} < \g{4}$ imply
$\g{1}+\g{2} < \g{3}+\g{4}$ but above we have
$\g{3}+\g{4}<\g{1}+\g{2}$, a contradiction, so $\gal$ is
incoherent. 
\label{ex:product-incoherent}
\end{example}

This example is interesting and complete enough that it can be
generalized to any disjoint union, however we do not yet have all the
tools necessary to deal with the proper generalization. Corollary
~\ref{c:incoherent-product} will extend this example to the disjoint
union of arbitrary hyperplane arrangements. Disjoint unions are a good
first example for inductive reasoning about monotone path graphs,
however they are not enough. To work towards a classification, we will
need to use the more powerful tools of single-element liftings
and single-element extensions.

It is worth commenting here that working with coherence seems
more subtle than $L_2$-accessibility. Example
~\ref{ex:intro-reflections} indicates that
coherent galleries will be more common than $L_2$-accessible
galleries however $L_2$-accessibility is the primary tool we have for bounding
diameter. Further, we are able to use $L_2$-accessibility
without any reference to functional $f$ realizing the acyclic
orientation of $\sA$. When we talk about coherence
properties, we must make reference to $f$.
