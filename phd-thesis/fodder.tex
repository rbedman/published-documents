
\begin{lemma} If $\mat{}=\mat{\sA}$ is an oriented matroid realized by
a hyperplane arrangement $\sA$ with an acyclic orientation induced
by $\f{}$ then the following are equivalent. 

\begin{itemize}
\item $\gal$ is a gallery of $\mat{}$ in the sense of Definition
~\ref{def:gallery-3},
\item $\gal$ is a gallery of $Z(\sA)$ in the sense of Definition
~\ref{def:gallery-2},
\item $\gal$ is a $\f{}$ monotone path in the sense of Definition 
~\ref{def:gallery-1}. 
\end{itemize}
\end{lemma}

\begin{proof} The proof follows from the definitions. In
  particular Definitions ~\ref{def:gallery-3} and
~\ref{def:gallery-2} are equivalent as each vertex of the
$1$-skeleton of $Z(\sA)$ is a maximal covector. Equivalence of
~\ref{def:gallery-2} requires us to understand that vertex
$v_i$ has coordinates
\[
\begin{split}
v_i=-\sum_{\gal(j)\le \gal(i)} \ai{i} &+ \sum_{\gal(j) > \gal(i)}
    \ai{i}\\
&\text{ and } \\
f(v_i)=-\sum_{\gal(j)\le \gal(i)} f(\ai{i}) &+ 
\sum_{\gal(j) > \gal(i)} f(\ai{i})
\end{split}
\]
Since $f$ induces an acyclic orientation we know that $f(\ai{i})>0$ so
moving from $i$ to $i+1$ changes the values of $f$ by $f(v_{\gal(i+1)}-v_{\gal(i)})=2f(\aig{i})>0$. 
\end{proof}


\begin{example} Continuing Example ~\ref{ex:gallery-g2} we recall 
$\sAd=\begin{pmatrix} -1 & -1 & 1 & 1 \end{pmatrix}$. Using
$f(x,y,z)=3x+2y+z$ we see that $f(\ai{i})>0$ so $f$ induces an acyclic
   orientation. The gallery $1423$ is coherent because the functional
   $g(x,y,z)=-33x+34y+31z$ selects it in the sense that 
\[
g(\ai{1})=\frac{-33}{3} < g(\ai{4})=\frac{-30}{4}< g(\ai{2})
= \frac{34}{2} < g(\ai{3}) = \frac{31}{1}
\]
We can check computationally that $\setof{1324,2314, 4132, 4231}$
is a complete list of incoherent galleries for $f(x,y,z)=3x+2y+z$.  All
other galleries are coherent and the subgraph of coherent galleries in
$\gtwo{\sA}$ is an embedding of the fiber zonotope of $\Sigma(f:Z(\sA)\to \R{})$.

In contrast, $f(x,y,z)=x+y+z$ also induces an acyclic orientation of
$\sA$ since $f(\ai{i})=1>0$, however there is no $g(x,y,z)$ which
selects $\gal=1423$. Suppose that $g(x,y,z)=Ax+By+Cz$ selects $\gal$,
then we would have
\[
A<A+B-C<B<C 
\]
but $B-C<0$ so $A+B-C<A$ which contradicts $A<A+B-C$. 
We will explore this example greater depth in Section
~\ref{ss:cr1-non-zonotopal}. 
\label{ex:gallery-coherent}
\end{example}




Thinking of galleries as paths on the zonotope $\Z{\sA}$ we think of
$\emph{coherent}$ galleries as paths that maximize a linear
functional $g \in \Rd{d}$. 


We will later show that when every
$\f{}$-monotone path is coherent for every $\f{}$ realizing $(+)^n$,
that 

paths because $\f{}$-monotone paths may be coherent
(for $\f{}$) while galleries cannot be coherent because they make no
reference to $\f{}$.



Notice that while hyperplane arrangements $\sA$ may
have no repetition, Example ~\ref{ex:hyperplane-cd1} illustrates that
we have no such condition on $\sAd$. We will fall back on our understanding of vector
configurations when geometric intuition fails, and to
understand $\sAd$.




\begin{defn} Given an oriented matroid $\mat{}$ of rank $d$ consisting
of sign vectors in $\setof{-,0,+}^n$ with an acyclic orientation, a
gallery $\gal$ of $\mat{}$ is a sequence
$\gal=((-)^n=c^{(0)},c^{(1)},\ldots,c^{(n)}=(+)^n)$ of $n+1$ of
maximal covectors of $\mat{}$ with $\abs{\sep{c^{(i)},c^{(i+1)}}}=1$. We
will use $\gal^{(i)}=c^{(i)}$ to denote the covectors occurring in
$\gal$ and $\gal(i)=\sep{c^{(i-1)},c^{(i)}}$ to denote where
$c^{(i-1)}$ and $c^{(i)}$ differ. For notation ease we refer
to the set of all galleries of $\mat{}$ as $\Gamma(\mat{})$.

When talking about $\gal$ globally, we will write
$\gal=(\gal(1),\ldots,\gal(n))$ and when it is unambiguous we will
shorten this to $\gal=\gal(1)\ldots\gal(n)$.
\label{def:gallery-omatroid}
\end{defn}

We pause briefly for to illustrating the two different notations of
$\gal$ are equivalent. 

\begin{example}
Looking at the corank 1 example in which $d=5$ and $k=1$, we will look
carefully at 
$\gal=614352$ which we will present as a table listing $\gal^(i)$ for
each $i$. 
\[
\begin{array}{c|cccccc}
\gal^{(0)} & - & - & - & - & - & - \\
\gal^{(1)} & - & - & - & - & - & + \\
\gal^{(2)} & + & - & - & - & - & + \\
\gal^{(3)} & + & - & - & + & - & + \\
\gal^{(4)} & + & - & + & + & - & + \\
\gal^{(5)} & + & - & + & + & + & + \\
\gal^{(6)} & + & + & + & + & + & + \\
\end{array}
\]
We can build a table like this for any permutation of $n$ and with no
reference to $\sA$. What makes
a permutation different from a gallery is that each row is in fact a
covector of $\mat{\sA}$. 
\end{example}

The hypothesis that $\mat{}$ has an acyclic orientation is important;
it distinguishes a maximal covector $c$ of $\mat{}$; as long as
$\mat{}$ has an acyclic orientation, we can label any maximal covector as
$+^n$ by reorienting $\mat{}$. When $\gal$ is of minimal
length, we call $\gal$ a minimal gallery. Unless otherwise noted we
assume that all galleries are minimal. When an
oriented matroid $\mat{}$ is realized by a hyperplane arrangement
$\sA$ we can use the idea of  paths on the zonotope from Example
~\ref{ex:hyperplane-diameter} to give a geometric interpretation to galleries.

\begin{defn} Given a hyperplane arrangement $\sA$ with 
an acyclic orientation induced by $\f{}$, 
a gallery $\gal$ is a path of minimal length on
$\gone{\sA}$ from $-^n$ to $+^n$.
\label{def:gallery-rmatroid}
\end{defn}

%% Added in Definition ~\ref{def:gallery-rmatroix}
%% ~\ref{def:gallery-rmatroid} are the functionals $\f{}$ and the
%% $1$-skeleton of $\Z{\sA}$.
Definition ~\ref{def:gallery-rmatroid} is a
bridge between the fully abstract oriented matroid notion of
~\ref{def:gallery-omatroid} and the more geometric notion which we will
introduce in Definition ~\ref{def:gallery-rmatroid-2}. This
intermediate definition is useful to use because it is the closest to
 cellular strings ~\cite{Billera94}, which we  will discuss in
Section ~\ref{ss:cr1-zonotopal}, ~\ref{ss:cr2-zonotopal-1}, and
~\ref{ss:cr2-zonotopal-2}. The difference between cellular strings
and galleries is cellular strings are paths on $\Z{\sA}$ while
galleries are paths only on the edges of $\Z{\sA}$. 

\begin{defn} A cellular string
$\sigma=\cell{}=\cell{1}\big|\cell{2}\big|\ldots\big|\cell{m}$ is a
path on $\Z{\sA}$ from $-^n$ to $+^n$ in which each block $\cell{i}$
is a face of $\Z{\sA}$ and $\cell{i}$ and $\cell{i+1}$ are adjacent in
$\Z{\sA}$.
\label{def:gallery-cellular-string} 
\end{defn}

To relate cellular strings and galleries we note that a gallery is a
cellular string with exactly $n$ blocks. Taking the
geometric viewpoint of ~\cite{Athanasiadis01}, we will identify
galleries with \emph{monotone paths} on the polytope $\Z{\sA}$. For
sufficiently generic $\f{}$ look at paths on $\Z{\sA}$.



Our example for Definition ~\ref{def:gallery-rmatroid} will be a
hyperplane arrangement we will come back to repeatedly. This
hyperplane serves as our first obstruction class in corank 1 and
discussed in detail in Section ~\ref{ss:cr1-non-zonotopal}.


 
This illustrates that Definitions
~\ref{def:gallery-omatroid}, ~\ref{def:gallery-rmatroid}, and
~\ref{def:gallery-rmatroid-2} are all equivalent, which
will now formalize with a lemma.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


We are building towards a graph analogous to
the $\gone{\sA}$, which we will call the \emph{monotone
path graph}. We have just introduced monotone paths, so we now 
discuss when two galleries are adjacent. 


This definition is becomes transparent when viewed geometrically and
illustrated with an example. 

\begin{example} Continuing Example ~\ref{ex:gallery-coherent}, using
\[
\sA=
\begin{pmatrix}
1 & 0 & 0 & 1 \\
0 & 1 & 0 & 1 \\
0 & 0 & 1 & -1 \\
\end{pmatrix}
\]
It is easy to check that both $\gal=1423$ and $\galp=4123$ are
galleries of $\sA$ using $\sAd=\left( -1, -1, 1, 1\right)$. Based on
Example ~\ref{ex:gallery-coherent} we have the following sequences of
covectors
\[
\begin{split}
\gal&=(----,+---,+--+,++-+,++++) \\
\galp&=(----,---+,+--+,++-+,++++).
\end{split}
\]
We can now say that $\gal$ and $\galp$ are adjacent by a flip in
position $1$ because $0--0$ is a covector of $\mat{\sA}$, an
intersection of hyperplanes $\sH_1$ and $\sH_4$, and a face of
$\Z{\sA}$. Hyperplane arrangements $\sH_1$ and $\sH_4$
intersect at the covector $0--0$. Galleries $\gal$ and $\galp$ simply
go around $0--0$ in two different ways. The same is true for the
zonotope where $0--0$ is a face of the zonotope and $\gal$ and $\galp$
are paths along the opposing boundaries of $0--0$. We see both of
these geometric interpretations illustrated in Figure
~\ref{fig:gallery-flip}.
\end{example}

We have now defined galleries and way to say when galleries are
adjacent so we introduce the fundamental object of study for this
dissertation, the \emph{monotone path graph}.

\begin{example} We now illustrate a hyperplane arrangement of interest in
  dimension $3$.
\[
\sA=
\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} \cr
& 1 & 0 & 0 & 1 \cr
& 0 & 1 & 0 & 1 \cr
& 0 & 0 & 1 & 1 \cr
}
\]
Since visualizing subdivisions of $\R{3}$ is tricky, we will look at
how the hyperplanes of $\sA$ intersect a unit sphere, and visualize
the intersection under Stereographic projection. Figure
~\ref{fig:hyperplane-stereo} shows the sterographic
projection of $\sA$ and Figure ~\ref{fig:hyperplane-zonotope} shows
the zonotope of $\sA$. In both Figures we see that chambers of $\sA$ are
\[
\mcC{}=\setof{
\begin{aligned}
++++,\quad%+++-,\quad
&++-+,\quad++--,\quad+-++,\quad\\
+-+-,\quad&+--+,\quad+---,\quad-+++,\quad\\
-++-,\quad&-+-+,\quad-+--,\quad--++,\quad\\
&--+-,\quad%---+,
----
\end{aligned}
}
\]
To make $\sA$ and $\Z{\sA}$ pointed we distinguish the chamber $c=++++$
and see that both $\gal=1423$ and $\galp=1243$
are galleries of $(\sA,c)$ and that $\gal$ and $\galp$ are adjacent by a
flip $X_{24}$ in $\sA$.
\label{ex:hyperplane-cd1}
\end{example}

\begin{figure}
\begin{tikzpicture}[scale=3.0]
\draw (-2,0) -- (2,0);
\draw (0,-2) -- (0,2);
\draw (0,0) circle (1);
\draw (-.7,-.7) circle (1.45 and 1.25);
%\draw [->] (1.5,0) -- (1.5,.25);
\draw (1.5,0.01) node {$\sH_{1}$};
\draw (0,1.5) node {$\sH_{2}$};
\draw (.72,.72) node {$\sH_{3}$};
\draw (-1.65,-1.65) node {$\sH_{4}$};
\draw (1.25,1.25) node[label] {$++++$};
\draw (-1.25,1.25) node[label] {$+-++$};
\draw (1.25,-1.15) node[label] {$-+++$};
\draw (.4,.5) node[label] {$++-+$};
\draw (-2,-2) node[label] {$--++$};
\draw (-1,-1) node[label] {$--+-$};
\draw (-.45,-.3) node[label] {$----$};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\draw (-1.3,.1) node[label] {$+-+-$};
\draw (-.35,.7) node[label] {$+--+$};
\draw (-.45,.1) node[label] {$+---$};
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\draw (.27,-1.10) node[label] {$-+-+$};
\draw (.27,-.5) node[label] {$-++-$};
\draw (.8,-.1) node[label] {$-+++$};
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\draw (.2,.1) node[label] {$++--$};
\end{tikzpicture}
\caption{hyperplane arrangement of Example ~\ref{ex:hyperplane-cd1} with sign vectors labeling chambers}
%      \caption{Oriented matroid $\mat{\sA}$.}
\label{fig:hyperplane-stereo}
\end{figure}



Both the nodes (galleries) and edges (flips) of the monotone path
graph have geometric meaning using $\Z{\sA}$ giving us two ways
to describe distance between galleries:
\begin{enumerate}
\item the graph distance $d(\gal,\galp)$ of $\gal$ and $\galp$ in the
monotone path graph, and
\item the geometric distance $d_{L_2}(\gal,\galp)$ of $\gal$ and $\galp$
using  the size of the set of all flips as a metric between in any
path between $\gal$ and $\galp$.  
\end{enumerate}

It is not immediately clear that the second measure is well-defined, however
this geometric distance gives a set valued metric $d_{L_2}$ on the
monotone path graph ~\cite{Reiner12}. We imagine these two
metrics might be equal but graph distance allows
the same intersection of hyperplanes to occur twice. The geometric
distance is not the focus of this dissertation, but the notion will be
used in Theorem ~\ref{l:cr1-existsL2}.

\begin{defn} We say that a gallery $\gal$ is $L_2$-accessible when 
$d(\gal,\galp)=d_{L_2}(\gal,\galp)=$ for all galleries $\gal$.
\end{defn}

$L_2$ accessibility is a labor-saving device, but one which is
tiresome to check by hand. The reason the definition is useful is the
result of Reiner and Roichman which says that ``if the monotone
path graph has an $L_2$ accessible node then the diameter is exactly
$\abs{L_2}$~\cite{Reiner12}.'' We will use this proposition in the
proof of Theorem ~\ref{l:cr1-existsL2}.

\begin{example} To illustrate we return to the monotone path graph of
Example ~\ref{ex:gallery-g2}. We check that the gallery $1432$ is
$L_2$-accessible, while the gallery $4132$ is not $L_2$ accessible;
$d(4132,3214)=6$ but $d_{L_2}(4132,3214)=4$.
\end{example}

If the zonotope is an adjacency structure on covectors and the
monotone path graph is an adjacency structure on galleries, a good
question is ``why does $Z(\sA)$ have the structure of a \emph{polytope}
while the monotone path graph has only the structure of
a \emph{graph}?'' The answer, as suggested by Figure
~\ref{fig:gallery-g2}, is that the monotone path graph looks like a
zonotope with extra ``bubbles''; the zonotope is the projection of a
polytope where each vertex corresponds to a \emph{coherent}
gallery~\cite{Billera92}, while not every gallery of the monotone path
graph is coherent.

It is possible to define a fiber polytope of $\f{}$
$\Sigma(\f{}:Z(\sA)\to \R{})$ and it is well-known that the monotone
path graph always contains $\Sigma(f:Z(\sA)\to \R{})$ as a
subgraph. The vertices of $\Sigma(f:Z(\sA)\to \R{})$ correspond to
$f$-monotone paths which are \emph{coherent} in the sense of
Definition ~\ref{def:gallery-coherent}.
%, which we now present.
%%  and that these subgraphs
%% are in fact the 1-skeleton of a particular zonotope constructed as the
%% \emph{fiber zontope} of $Z(\sA)$. 









%% It is now
%% possible to classify all possible monotone path graphs in a
%% manner similar to the classification for vector configuration. In fact,
%% we start with the classification for vector configurations, then
%% discard those which are not acyclic, those that are not essential, and
%% those that are not repetition free. The classification is included in
%% Appendix ~\ref{a:agd-classification}. 

We now have all the definitions necessary for this dissertation and
are ready to start proving things. Stating and proving our technical
lemmas will be the goal of the next chapter. 





%% is an obvious thing to try, as duality allows us to
%% understand the maximal covectors of $\mat{\sA}$ in terms of the linear
%% dependences of $\sAd$ as we already started doing in
%% ~\ref{ex:hyperplane-cd1}. Duality also makes it clear when two
%% chambers are \emph{adjacent}, in the sense that they are seperated a
%% hyperplane. 


%% What we would like in order to understand hyperplane
%% arrangements 

%%  in higher
%% dimensions, so we would like think of other ways to visualize the
%% geometry of hyperplane arrangements.



%% Talk of chambers and adjacency leads naturally to the
%% introduction of the graph $\gone{\sA}$ and of the zonotope $Z(\sA)$
%% which will both encode the oriented matroid information of
%% $\mat{\sA}$. We will present both definitions together before we
%% proceed to illustrate their connection.

%% \begin{defn} The graph $\gone{\sA}$ is the graph whose vertcies are
%%   chambers $c$ of $\sA$ and in which two vertcies are adjacent when 
%%   $\abs{\sep{c,c^\prime}}=1$. 
%% \end{defn}

%% In the case of cyclic, co-rank 1, we can further classify the chambers
%% by the number of \emph{negative} vectors in the Gale dual $\sA^*$. In
%% this setting we will use the notation $G_2(\sA,k)$ to refer to the
%% $G_2$ graph corresponding to a chamber with exactly $k$ negative
%% vectors.

%% To be concrete we will look at a specific presentation of
%% $Cyclic(d,d+1)$, a cyclic arrangement of co-rank 1. We will give normal
%% vectors for the hyperplane arrangements as
%% \[
%% \sA=\setof{e_1,e_2,\ldots,e_d,e_1+e_2+\cdots+e_d}.
%% \]

%% The zonotope of this hyperplane arrangement is well known and
%% described in  ~\cite{MR1166643} and the Zonotope for $d=3$ is pictured
%% in Figure ~\ref{fig:cd1g2}.

%% The Gale dual of this configuration is
%% $\sA^*=\setof{e^*_1=e^*_2=\ldots=e^*_d=+,e^*_{d+1}=-}$ and the
%% covectors of $\sA$ are the vectors of $\sA^*$. Vectors of $\sA^*$ are
%% (non-trivial) linear dependencies among
%% $\setof{e^*_1,\ldots,e^*_{d+1}}$ which makes it straightforward to see
%% the covectors of $\sA$ - they are the covectors whose support is not
%% all positive or all negative. 



The classification of $G_2$ graphs mirrors the classification of
acyclic vector configurations, which is a classic exercise in the use
of Gale duality. 


%
% how much do I want to say here?
%
% Include in appendix
%

Understanding the classification $G_2$ graphs in terms of Gale duals leads to 

\begin{lemma} If $\sA^+$ is a single element lifting of $\sA$ and $\gamma$
  is a gallery in $G_2(\sA)$ then any $\gamma^+$ is a gallery of
  $G_2(\sA^+)$. 
\label{l:gallerylifting}
\end{lemma}

\begin{proof} Since $A^+$ is a single element lifting of $A$, we know
  that the Gale ${\sA^+}^*$ is a single element extension of $\sA^*$,
  denote the duals by $\sA^*=\setof{a_1^*,\ldots,a_n^*}$ and 
  ${\sA^+}^*=\setof{a_1^*,\ldots,a_n^*,a_{n+1}^*}$. We must show that
  any acyclic orientation of $\sA$ can be lifted to an acyclic
  orientation of $\sA^+$.

  Suppose $c_1,\ldots,c_n$ are strictly positive and satisfy  $\sum
  c_i a_i^*=0$. Since $n>d$ and $\sA$ is essential~\footnote{is this
    the right condition} we know $a_{n+1}^*$ is a linear combination
  of $\setof{a^*_1,\ldots,a^*_n}$ so there exist
  $\alpha_1,\ldots,\alpha_{n+1}$ (not necessarily strictly positive)
  such that $\sum_{i=1}^n \alpha_i a_i^*-a_{n+1}^*=0$. 
  \[
  c_i^\prime = 
  \begin{cases}
    c_i-\frac{\alpha_i}{N}  & \mbox{ if } 1 \le i \le n \\
    \frac{1}{N}             & \mbox{ if } i=n+1 
  \end{cases}.
  \]
  Notice that for $N$ sufficiently large, $c_i^\prime>0$. For such an
  $N$ we then have
  \[
  \begin{split}
    \sum_{i=1}^{n+1}c_i^\prime a_i^*  
    &= 
    \sum_{i=1}^{n}\left(c_i -\frac{\alpha_i}{N}\right) a_i^* +
    \frac{1}{N}a_{n+1}^* \\
  &=
  \sum_{i=1}^{n}c_i
  a_i^*+\frac{1}{N}\left(a_{n+1}^*-\sum_{i=1}^{n}\alpha_i a_i^*\right)
  \\
  &= 0 + 0 = 0
  \end{split}
  \]
  So $c_i^\prime$ is an acyclic orientation of ${\sA^+}^*$.
\end{proof}

%% \begin{corollary} If $\gamma$ is a cohearent gallery for $A$ then and
%%   $A^+$ is a single element lifting of $A$ then any lifting $\gamma^+$
%%   of $\gamma$ is cohearent.
%% \end{corollary}

%% \begin{proof}
%% Using the same argument we used for Lemma \label{l:gallerylifting} we
%% can pick $N$ to be large enough that $\frac{1}{N} \alpha_i < c_i-c_j$
%% for all $\gamma(i)<\gamma(j)$  which means that the ordering of $c_i$
%% is not changed.
%% \end{proof}

\begin{lemma} If $A^+$ is a single element lifting of $A$ and $\gamma$
  is an incoherent gallery in $G_2(A)$ then there is lifting of
  $\gamma$ which is incoherent.
\end{lemma}


\begin{proof} We will prove using the contrapositive. Suppose $A^+$ is
  a single element lifting of $A$ and $f^+$ is a lifting of the
  functional $f$, obtained by using lemma ~\ref{l:gallerylifting}. 


  Lemma ~\ref{l:gallerylifting} additionally tells us that both
  \[
  \begin{split}
  \gamma_1 &= (n+1,\gamma(1),\gamma(2),\ldots,\gamma(n)) \\
  \gamma_2 &= (\gamma(1),\gamma(2),\ldots,\gamma(n),n+1)
  \end{split}
  \]
  are both liftings of $\gamma$, which we are assuming to be
  coherent. Our goal is to show that $\gamma$ is coherent.  

  Since
  $\gamma_1$ and $\gamma_2$ are coherent there exist
  $c_{\gamma_1(n+1)}/f(a_{n+1}) < c_{\gamma_1(1)}/f(a_{1}) < \cdots < c_{\gamma_1(n-1)} <
  c_{\gamma_1(n)}/f(a_{\gamma(n)})$ and 

$d_{\gamma_2(1)} < d_{\gamma_2(2)} < \cdots <
  d_{\gamma_2(n)} < d_{\gamma_2(n+1)}$ such that
  \[
  \begin{split}
    \sum_{i=1}^{n+1} c_i a_i^* = 0 \\
    \sum_{i=1}^{n+1} d_i a_i^* = 0 \\
  \end{split}
  \]
  And without loss of generality we assume that $c_{n+1} < 0 <
  d_{n+1}$. We also notice that for sufficiently large $N$ we can
  control $f_(a_i)$ to be as close to $1$ as we'd like. We choose $N$
  to be large enough\footnote{$N>\mbox{max}(-c_{i} \alpha_{i+1} + c_{i+1}\alpha_i)/(c_i - d_i)$ bounds go here} that so that $c_{\gamma(1)} < c_{\gamma(2)} <
  \ldots c_{\gamma(n)}$. This will only decrease $c_{n+1}$ while increasing $d_{n+1}$. 

%% We assume that $c_{n+1}$ and
%%   $d_{n+1}$ are non-zero, since $c_{n+1}=0$ would imply a linear
%%   dependence supported on $\setof{a_i}$ with $1\le i \le n$,
%%   and $\gamma$ would be cohearent already.

  Since the sequences $(c_i/f(a_i))$ and $(d_if(a_i))$ are $\gamma$ monotone, we
  know that sums and scalar products are also $\gamma$
  monotone. Using this idea we define 
  \[
  \alpha_i=c_i-\frac{c_{n+1}}{d_{n+1}}d_i \mbox{ for } 1 \le i \le n
  \],
  which we claim is a $\gamma$ monotone sequence and that it is a
  linear dependence on $\sA^*$. 

  As $(c_n)$ and $(d_n)$ are both
  $\gamma$ monotone $(c_n+kd_n)$ is also monotone for $k>0$. We set 
  $k=\frac{c_{n+1}}{d_{n+1}}$, which %we know $k>0$ since 

  This is easy to check
  \[
  \begin{split}
  \sum_{i=1}^n \alpha_i a_i^* &= \sum_{i=1}^n \left( c_i -
  \frac{c_{n+1}}{d_{n+1}}\right)a_i^* \\
  &= \sum_{i=1}^n c_i a_i^* - \frac{c_{n+1}}{d_{n+1}} \sum_{i=1} d_i
  a_i^* \\
  &=
  -c_{n+1}a_{n+1}^* + \frac{c_{n+1}}{d_{n+1}}d_{n+1}a_{n+1}^* \\ &= 0
  \end{split}
  \]
  Thus, $\gamma$ is coherent. 
\end{proof}

By using the contrapositive for the proof of this lemma we obtain an
elegant proof, however we obscure the constructive nature of the
lemma, and some of the geometric motivation is lost. The following
example provides an illustration of the geometry and how one goes
about constructing incoherent galleries. 


%% In this light we view galleries as sequences of covectors. Each
%% hyperplane will be supported in exactly $d$ positions and moving
%% across hyperplane $H_i$ flips the sign of the covector in position
%% $i$. 

%% \begin{example}
%% Looking at $d=5$ and $k=1$, we will look carefully at the covectors
%% which are crossed in the gallery $(6,1,4,3,5,2)$ which we will present
%% as a table. The gallery is listed in the first column of the table,
%% and each row of the table corresponds to the covector which must be
%% crossed by that gallery.

%% \[
%% \begin{array}{c|cccccc}
%%   & + & + & + & + & + & + \\
%% \hline
%% 6 & + & + & + & + & + & 0  \\
%% 1 & 0 & + & + & + & + & -  \\
%% 4 & - & + & + & 0 & + & - \\
%% 3 & - & + & 0 & - & + & -  \\
%% 5 & - & + & - & - & 0 & -  \\
%% 2 & - & 0 & - & - & - & -  \\
%% \hline
%%   & - & - & - & - & - & - \\
%% \end{array}
%% \]
%% \end{example}

%% This allows us to see whether an arbitrary ordering of the hyperplanes
%% is a gallery or not based on analysis of the Gale dual. For the $\sA$ the only negative element of
%% $\sA^*$ is $e^*_{d+1}$ This allows us to rapidly decide that
%% $(1,2,\ldots,d,d+1)$ is not a gallery of $G_2(d,1)$ as the final
%% hyperplane crossing  be $(-,-,\ldots,-,0)$, which is not a covector of
%% $\sA$. 


\begin{lemma} A gallery $g_0$ in $G_2(C(d,d+1),k)$
  admits a flip $f_0$ if and only if $f_0(g_0)$ is a
  gallery. 
\end{lemma}

\begin{proof} The key observation for this lemma is that if a gallery is valid, then each wall the gallery passes through is valid.

Suppose a gallery $g_0$ admits a flip $f_0$ but $f_0(g_0)$ is not a
valid gallery. Further suppose that the flip $f_0$ is of length $k$
and located at position $i$ in $g_0$. In order to guarantee that flips and walls are valid, we must
guarantee that the covector associated to them is never all positive
(or all negative). 

Since the flip $f_0$ is valid, its covector is guaranteed to an
acyclic orientation of $\sA$, and contain at least one positive and one
negative element. Since $g_0$ is a valid gallery, we know
that each hyperplane intersection before $f_0$ is an acyclic
orientation of $\sA$.

\end{proof}



%% It is well known that a hyperplane arrangement  has the
%% structure of an oriented matroid ~\cite{MR1744046},  which assigns string of
%% length $n$ consisting of the symbols  
%% $\setof{+,0,-}$  to each element (chamber, wall, intersection
%% of walls, etc) in the hyperplane arrangement. The string corresponding
%% to any element of the hyperplane arrangement is known as a
%% \emph{covector}. The collection of all
%% covectors is known as an \emph{oriented matroid} and satisfy axioms
%% which are discussed in . Each hyperplane
%% divides a space into a positive halfspace and a negative halfspace. The covector
%% given to a an element $c$ in a hyperplane arrangement is $+$ in position
%% $i$ if $c$ lives in the positive halfspace defined by hyperplane
%% $H_i$, $-$ if $c$ lives in the negitive halfspace, and $0$ when $c$ is
%% contained in $H_i$. We will use $\sM(\sA)$ to denote the oriented matroid of the
%% arrangement $\sA$. 
