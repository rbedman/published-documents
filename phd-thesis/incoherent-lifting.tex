Our fifth lemma is the analog of Lemma
~\ref{l:coherent-projection} for single-element liftings. This lemma
will be the key to our classification of
universally all-coherent hyperplane arrangements in both corank $1$ and corank
$2$. We recall from Section ~\ref{ss:orientation-lifting} that when $\sAl$ is
a single-element lifting of $\sA$ we say a gallery $\gall$ of
$(\sAl,\widehat{c})$ is a lifting of a gallery $\gal$ of $(\sA,c)$
when  $\gall \contract (n+1)=\gal$

Our goal in this section is to prove that if $\gal$ is an incoherent
$\f{}$-monotone path for every $(\Z{\sA},\f{})$ realizing $(\sA,c)$
then there is an incoherent $\fl{}$-monotone path $\gall$ for every
$(\Z{\sAl},\fl{})$ realizing $(\sAl,\widehat{c})$. 

\begin{example} Following Example ~\ref{ex:gallery-coherent} we use
  the hyperplane arrangement introduced in Example
  ~\ref{ex:gallery-coherent}. While we previously worked with $\sA$
  directly, we now revisit this example using $\sAd$.
\[
\sAd=
\begin{pmatrix} 
-1 & -1 & 1 & 1 \\
\end{pmatrix}.
\]
For the function $\f{}(x,y,z)=x+y+z$ we have $\f{i}=1$ for all $i$. We
were able to pick an incoherent $\f{}$-monotone path $\gal$ by 
ordering $\gal$ negative, positive, negative, positive. This
guaranteed that no matter what $\g{i}$ we picked, we had
$\sum_{i=1}^4 \g{i}\aid{i}>0$. We see that there are $8$ such
incoherent galleries for $\f{}$.

We now look at a lifting $\sAl$ of $\sA$ and in particular look at its
dual $\sAl^*$
\[
\sAl^*=
\begin{pmatrix} 
-1 & -1 & 1 & 1 & 1 \\
\end{pmatrix}.
\]
Using the lifting $\fl{}$ guaranteed by Lemma
~\ref{l:orientation-lifting}; with $N=10$ we have
$\fl{}^*=(1,1,1,9/10,1/10)$. Our insight here is that
$\sum_{i=1}^4 \g{i}\aid{i}>0$ so by making $\g{5}\aid{5}>0$ we can
make $\gall$ incoherent, and we can make $\g{5}\aid{5}>0$ by making
$\g{5}>\g{3}$ using $\gall=14235$. At the same time we
think we can make $\gal=51423$ a coherent $\fl{}$-monotone path of
$(\Z{\sA},\fl{})$ by making $\g{5}$ as negative as we want. Our
intuition is good; $\gall=14235$ is incoherent for ($\sAl$,$\fl{}$)
and $\gal=51423$ is coherent for ($\sAl$,$\fl{}$). That 
$51423$ is coherent for $(\sAl,\fl{})$ is interesting and important;
not every lifting of an incoherent $\f{}$-monotone path is incoherent.
\label{ex:incoherent-lifting}
\end{example}

The lesson here is both the galleries $(n+1,\gal)$ and
$(\gal,n+1)$ are important and we must check both when looking
for incoherent galleries. 
%% We are reminded here that coherence
%% depends on $\f{}$ so we should pick $\fl{}$ carefully, not just
%% according to Lemma ~\ref{l:orientation-lifting}. 
As with Lemma ~\ref{l:coherent-projection} we will find 
coherence properties easier to deal with and use the contrapositive.%%  Our standard
%% simplifying assumption that $\f{i}=1$ will hold here and without loss
%% of generality we assume that $\sA$ is ordered so that
%% $\gal=(1,2,\ldots,n)$.

\begin{lemma} Let $\sA$ be a hyperplane arrangement and $\sAl$ a
single-element lifting of $\sA$. Suppose 
  \[
  \begin{split}
  \gallg  &= (n+1,1,2,\ldots,n), \text{ and} \\
  \gallgp &= (1,2,\ldots,n,n+1) \\
  \end{split}
  \]
  are coherent $\fl{}$-monotone paths of $(\Z{\sAl},\fl{})$ for some
  $\fl{}$. Then there is a generic functional $\f{}$ on $\Z{\sA}$ for
  which $\gal$ is a coherent $\f{}$-monotone path.
\label{l:incoherent-lifting}
\end{lemma}

\begin{proof}
We assume we have ordered $\sAl$ so that 
$\sAld=\sAl \cup \setof{\ali{n+1}}$. We know that $(\mat{\sA},(+)^n)$
is an acyclically oriented matroid and that $\gal$ is a gallery of
$(\mat{\sA},(+)^n)$ we want to show that if 
\[
\begin{split}
\gallg=&(n+1,1,2,\ldots,n), \mbox{ and } \\
\gallgp=&(1,2,\ldots,n,n+1)
\end{split}
\]
are both coherent $\f{}$-monotone paths for some generic $\fl{}$ on
$\Z{\sA}$ then there exists some generic $\f{}$ on $\Z{\sA}$ for which
$\gal$ is coherent. We may that $\sAl$ is chosen so 
$\fl{i}=\fl{}(\ali{i})=1$ for all $i$.
Both $\gallg$ and $\gallgp$ are coherent $\f{}$-monotone paths so,
there are linear dependencies $\setof{\gl{i}}$ and $\setof{\glp{i}}$
of $\sAd$ selecting $\gallg$ and $\gallgp$ chosen according to
Corollary ~\ref{cor:pick-gs} so that
\[
\begin{split}
0=\gl{n+1} < & \gl{1} < \gl{2} < \cdots < \gl{n}, \quad \mbox{ and}\\
           0 < & \glp{1} < \glp{2} < \cdots < \glp{n}<\glp{n+1}. \\
\end{split}
\]
We want to realize $(\mat{\sA},(+)^n)$ with a generic 
functional $\f{} \in \Rd{d}$ on $\Z{\sA}$. 
\[
\mbox{Define }\quad
\f{i} = 1-\frac{\glp{i}}{\glp{n+1}}>0.
\]
We check that $\setof{\f{i}}$ are induced by a functional $\f{i}=\f{}(\ai{i})$ by computing
\[
\begin{split}
\sum_{i=1}^n \f{i} \aid{i}=&
\sum_{i=1}^n\left(1-\frac{\glp{i}}{\glp{n+1}}\right)\aid{i} \\
=& \sum_{i=1}^n \aid{i} - \frac{1}{\glp{n+1}} \sum \glp{i}\aid{i} \\
=& -\aid{n+1}-\frac{-\glp{n+1}}{\glp{n+1}} \aid{n+1} \\
=& 0.
\end{split}
\]

This gives $\f{}$ two important properties:
\begin{itemize}
\item Since $\glp{i}<\glp{n+1}$ for $1\le i \le n$, we know $\f{i}>0$.
\item For $1 \le i < k \le n$ we know $\f{i}>\f{k}$ (since $\glp{i}<\glp{k}$). 
\end{itemize}

We claim that $\gal$ is a coherent $\f{}$-monotone path of $\sA$ and 
we must find $\g{}$ which selects $\gal$ in the sense of Definition ~\ref{def:gallery-coherent}.

Pick $\g{i}=\gl{i}$ for $1 \le i \le n$.
Since $\gl{n+1}=0$, we know that 
\[
\sum_{i=1}^n\g{i}\aid{i}
=\sum_{i=1}^{n}\gl{i}\aid{i}+0\aid{n+1}
=\sum_{i=1}^{n+1}\gl{i}\aid{i}=0,
\]
and $\g{i}=\g{}(\ai{i})$ are induced by a function $\g{}\in \Rd{d}$. Finally we
must check that $\setof{\g{i}}$ have a $\gal$ ordering. Given $1 \le i
< k \le n$ we know 
\[
\frac{\g{i}}{\f{i}} < \frac{\g{k}}{\f{i}} < \frac{\g{k}}{\f{k}}
\]
as desired. Thus $\gal$ is a coherent $\f{}$-monotone path of $(\Z{\sA},\f{})$.
\end{proof}

The way we intend to use this lemma is not for coherence, but as an
inductive tool to show that hyperplane arrangements must have incoherent
galleries. The logical statement we want is the contrapositive of Lemma ~\ref{l:incoherent-lifting}. 

\begin{corollary} For a $\sAl$ a single-element lifting of $\sA$, if
$(\Z{\sA},\f{})$ has an incoherent $\f{}$-monotone path for 
every $\f{}$ then $(\Z{\sA},\f{})$ has an incoherent $\fl{}$-monotone
path for every $\fl{}$. 
\label{c:incoherent-lifting}
\end{corollary}

This is the lemma we are searching for. Corollary
 ~\ref{c:incoherent-lifting} will be our most powerful tool in
 classifying monotone path graphs in coranks $1$ and $2$. It is
worth keeping in mind, however that none of our lemmas are specific to
 coranks $1$ or $2$; our technical lemmas are powerful enough to
use in arbitrary corank.


