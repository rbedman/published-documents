%% Figures created with thesis/software/classify_gvoutput.py, in the file
%% change n to create different parallelism classes. Created a svg or pdf
%% output file using 
\label{a:agd-classification}

This thesis is not complete without a classification of pointed zonotopes. Our starting point is a vector
configuration $V=\setof{v_1,\ldots,v_n}$. We recall that the covectors
of $\mat{V}$ are sign vectors of valuations of functionals
$\f{}$ on $V$ and the maximal covectors are sign vectors of generic
functionals for which $\f{}(v_i)\ne 0$. The zonotope
$\Z{V}\sum_{i=1}^n [-v_i,v_i]$ has vertices corresponding to maximal
covectors of $V$ and the Gale dual $V^*$ of $V$ is a
vector configuration whose linear dependencies correspond to the
maximal covectors of $V$. Our strategy for classification of pointed
zonotopes will be to classify all affine Gale diagrams. The choice of
an affine gale diagrams includes the choice of an acyclic orientation
of $\mat{V}$ so affine Gale diagrams will correspond to pointed
zonotopes and pointed hyperplane configurations. 

We give a partial ordering on affine Gale diagrams using
single-element extensions which we recall from
Chapter ~\ref{ss:level2}. 
 
\begin{defn} We say that a vector configuration $V^+$ is a
  single-element extension of $V$ if $V^+=V \cup \setof{v_{n+1}}$
%\label{def:matroid-extension}
\end{defn}

\begin{defn} We say that a vector configuration 
  $\widehat{V} = \setof{\widehat{v}_i\in \R{d+1} \,\big|\, 1\le i \le n+1}$ is a
  single-element lifting of $V=\setof{v_i \in \R{d} \,\big|\, 1 \le i \le n}$ 
  if there is a linear surjection $\R{d+1} \xmapsto{\pi} \R{d}$ so
  that
  \[
  \begin{split}
    \pi(\hat{v}_{n+1})&=0 \\
    \pi(\hat{v}_i)&=c_i\cdot v_i \, 1 \le i \le n
  \end{split}
  \]
Without loss of generality, we typically assume that $c_i=1$. 
%\label{def:matroid-lifting}
\end{defn}

The important things to recall about single-element liftings and
extensions are:
\begin{itemize}
\item Single element extensions of $V^*$ correspond to single-element
liftings of $V$. 
\item Single element extensions increase corank.
\item Any function $h\in \Rd{d}$ of $V$ can be extended to $V^+$,
  however acyclic orientations of $V$ do not necessarily extend to acyclic
  orientations of $V^+$ as $h(v_{n+1}^*)$ may not be positive.
\item Single element liftings preserve corank.
\item Acyclic orientations of $\sA$ can always be lifted to acyclic
  orientations of $\sAl$, using Lemma ~\ref{l:orientation-lifting}. 
\item If $\widehat{V}$ is a single-element lifting of $V$ then 
$\widehat{V}^*$ is a single-element extension of $V^*$
  \[
  \left(\widehat{V}\right)^*=\left(V^*\right)^+
  \]
\item If $V^+$ is a single-element extension of $V$ then 
$\left(V^+\right)^*$ is a single-element lifting of $V^*$
  \[
  \left(V^+\right)^*=\widehat{V^*}
  \]
\end{itemize}

\section{Corank 1}
For $V$ a vector configuration of $d+1$ vectors in $\R{d}$ the
dual configuration $V^*=\setof{v_1^*,\ldots,v_{d+1}^*}$ is a
configuration of $d+1$ vectors in $\R{1}$. These vectors can either
point in the \emph{positive} direction or the \emph{negative}
direction. We know from Definition
~\ref{def:matroid-dependence} that the magnitudes $\abs{v_i^*}$ do not
change $\mat{V}$, so the positive vectors are interchangeable and the
negative vectors are interchangeable. The vector configuration $V$ is
uniquely determined by the number $k$ of \emph{negative} vectors in
$V^*$. Without loss of generality, we can assume that $0 \le
k \le\frac{d+1}{2}$. 

The $k=0$ vector configuration will have no acyclic orientation; all
$v_i^*$ will be positive and thus there no $\setof{h_i>0}$ such that
$\sum h_i v_i^*=0$. All other vector configurations will have an
acyclic orientation. Figure ~\ref{fig:agd-classification-cr1}
illustrates corank 1 vector configurations for small $n$, ordered by
single-element extension, which will be lifting in the dual.
\label{ex:matroid-cr1-classification}

We represent each pointed zonotope with a series of black and white
dots. We draw positive vectors as black dots and negative vectors as
white dots. We denote vector configurations with an acyclic
orientation using a circle diagram, while those with no acyclic
orientation are contained in an square.

\begin{figure}
  \includegraphics[scale=.75]{figures/agd-classification-cr1}
  \caption{Affine Gale diagrams in corank 1.}
  \label{fig:agd-classification-cr1}
\end{figure}

%\newpage
\section{Corank 2}
%% Following Example~\ref{ex:cr2-classification} we have arranged our
%% classification by the number of parallelism classes. 

To give an equivalent classification in corank 2 we extend these
tricks by writing vectors as elements of $V^*$ in standard position
and calling them positive or negative. This was accomplished by
~\cite{Gale56} and more fully developed in ~\cite{Grünbaum67} and
~\cite{McMullen71} but is presented in many survey works of which we
are particularly fond of ~\cite{Ziegler95} and ~\cite{Björner93}. We first write $V^*$ in a canonical form.

\begin{defn} When $d\ge 3$ and $n-d=2$,
  we say that the vectors of $V^*$ are in standard position when 
  $v_1^*=\pm\begin{pmatrix} 0 \\ 1 \end{pmatrix}$,
  $v_2^*=\pm\begin{pmatrix} 1/2 \\ 1/2 \end{pmatrix}$, 
  and $v_3^*=\pm\begin{pmatrix} 1 \\ 0 \end{pmatrix}$.
  Any remaining $v_i$ are given coordinates
  $\pm\begin{pmatrix}1\\-\mu\end{pmatrix}$ and we call $\mu$ the
  cross-ratio of $v_i^*$. 
\label{def:matroid-cross-ratio}
\end{defn}

%% understand what
To classify the elements of $V^*$ as positive or negative vectors in
$2$ dimension, we introduce an affine hyperplane and look at which
half-space contains each element of $V^*$. This gives a picture known as an affine Gale diagrams.

\begin{defn} Given a vector
  configuration $V$ consisting of $n$ vectors in $\R{d}$, the \emph{Affine Gale
    Diagram} is a set of vectors in $\R{n-d-1}$ each 
  labeled as ``positive'' or ``negative''. The affine Gale diagram
  encodes the geometric information of $V$ which we construct as follows:
\begin{enumerate}
\item Find $V^*$ so that $\matd{V}=\mat{V^*}$.
\item Pick $\mathcal{L}$, a generic affine hyperplane of $\R{n-d}$ with
  normal vector $\ell$.
\item Project $v_i^*\in V^*$ onto $\mathcal{L}$.
\item Label $v_i^*\in V^*$ as positive or negative based on the sign
  of $v_i^*\cdot \ell$. 
\end{enumerate}
\label{def:matroid-affine-gale-diagram}
\end{defn}

Our convention when drawing pictures of affine Gale diagrams is
to draw positive vectors as black dots and negative vectors as white
dots. We will draw our affine gale diagrams so
that classes of parallel vectors in $V^*$ will ``line up'' on the $x$ axis,
while non parallel vectors have distinct $y$ values form a
``pile''. We recognize that the absolute position of the remaining
vectors may depend on the cross-ratio, even when the oriented matroid
does not.

Affine Gale diagrams make it possible to classify corank 2 vector
configurations, however we can not tell when two affine
Gale diagrams represent the same vector configuration. We address this
with a brief lemma.

\begin{lemma} Two affine Gale diagrams are equivalent if they differ
  by any sequence of moves of the form :
\begin{itemize}
\item Reversing the order of the vectors.
\item Pulling a vector from the top, switching its sign, and
  putting it back on the bottom. 
\end{itemize}
\label{l:matroid-agd-equiv}
\end{lemma}

\begin{proof} This proof follows from the definition of affine Gale
  diagrams. Reversing the signed vectors corresponds to
  flipping the normal vector of $\mathcal{L}$. Cycling signed vectors
  corresponds
  to rotating the hyperplane $\mathcal{L}$. When $\mathcal{L}$ rotates
  through a vector $v_i^*$ the sign of $v_i^*$ and it switches from
  being first to last, thus the signed vector changes color and moves
  from first to last.
\end{proof} 

%% In corank 1 we organize our classification by $n$ (total number of
%% vectors in $V$) and by $k$ (number of negative vectors of $V$). The
%% organization in corank 2 is by single-element liftings and single
%% element extensions. We begin with single-element extensions, which
%% will be the foundation of Lemma ~\ref{l:coherent-projection}. The idea
%% is to add more vectors to $V$, creating $V^+$ with $n+1$ vectors in
%% $\R{d}$ from $V$ of $n$ vectors in $\R{d}$.

%% \begin{defn} We say that a vector configuration $V^+$ is a
%%   single-element extension of $V$ if $V^+=V \cup \setof{v_{n+1}}$
%% %\label{def:matroid-extension}
%% \end{defn}

%% We illustrate this with a simple example.
%% \begin{example} The vector configuration
%% \[
%% V^+=
%% \begin{pmatrix}
%% 1 & 0 & 1 & 1 \\
%% 0 & 1 & 1 & 2 \\
%% \end{pmatrix}
%% \]
%% is a single-element extension of the vector configuration from Example
%% ~\ref{ex:matroid-realized}
%% \[
%% V=
%% \begin{pmatrix}
%% 1 & 0 & 1 \\
%% 0 & 1 & 1 \\
%% \end{pmatrix}.
%% \]
%% \end{example}


%% \begin{defn} We say that a vector configuration 
%%   $\widehat{V} = \setof{\widehat{v}_i\in \R{d+1} \,\big|\, 1\le i \le n+1}$ is a
%%   single-element lifting of $V=\setof{v_i \in \R{d} \,\big|\, 1 \le i \le n}$ 
%%   if there is a linear surjection $\pi:\R{d+1} \xmapsto{\pi} \R{d}$ so
%%   that
%%   \[
%%   \begin{split}
%%     \pi(\hat{v}_{n+1})&=0 \\
%%     \pi(\hat{v}_i)&=c_i\cdot v_i \, 1 \le i \le n
%%   \end{split}
%%   \]
%% Without loss of generality, we typically assume that $c_i=1$. 
%% %\label{def:matroid-lifting}
%% \end{defn}


%% We illustrate with an example of single-element liftings.

%% \begin{example}
%% The configuration
%% \[
%% V=
%% \begin{pmatrix}
%% 1 & 0 & 0 & 1 \\
%% 0 & 1 & 0 & 1 \\
%% 0 & 0 & 1 & 1 \\
%% \end{pmatrix}
%% \]
%% is a single-element lifting of the vector configuration of Example
%% ~\ref{ex:matroid-realized}. The projection map is
%% \[
%% \pi(x)=
%% \begin{pmatrix}
%% 1 & 0 \\
%% 0 & 1 \\
%% 1 & 1 \\
%% 0 & 0 \\
%% \end{pmatrix}
%% x.
%% \] In the corank 1 classification, $V$ is $n=3,k=1$
%% while $V^+$ is $n=4,k=1$ because liftings preserve corank.
%% %\label{ex:matroid-lifting}
%% \end{example}


We now classify realized oriented matroids in corank $2$
using affine Gale diagrams. This classification is well-known
~\cite{Gale56}, however a complete classification is tedious and
typically left to the reader ~\cite{Ziegler95} or treated as a counting
problem ~\cite{Las77}. We include our
classification because we found it useful as a written
reference, and having the complete classification ordered by single
extensions liftings makes patterns clear.


\begin{figure}
\includegraphics[scale=.9]{figures/agd-classification-cr2-p3}
\caption{Affine Gale diagrams in corank 2 with exactly 3 parallelism classes}
\label{fig:agd-classification-cr2-3}
\end{figure}

\newpage

\begin{figure}
\includegraphics[scale=.9]{figures/agd-classification-cr2-p4}
\caption{Affine Gale diagrams in corank 2 with exactly 4 parallelism classes}
\label{fig:agd-classification-cr2-4}
\end{figure}

\newpage

\begin{figure}
\includegraphics[scale=.9]{figures/agd-classification-cr2-p5}
\caption{Affine Gale diagrams in corank 2 with exactly 5 parallelism classes}
\label{fig:agd-classification-cr2-5}
\end{figure}

