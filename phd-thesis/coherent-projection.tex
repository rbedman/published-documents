This section presents our first tool to prove existence of
incoherent galleries; when $\sAe$ is a single-element extension of
$\sA$ we prove that when $\sA$ has incoherent $\f{}$-monotone paths
$\sAe$ does too. The intuition here is clear: 
\[
\frac{\gga{1}}{\fg{1}} < \cdots < \frac{\gga{n}}{\fg{n}} < \frac{\gga{n+1}}{\fg{n+1}}
\]
cannot be satisfied without also satisfying 
\[
\frac{\gga{1}}{\fg{1}} < \cdots < \frac{\gga{n}}{\fg{n}}. 
\]
Using a contrapositive statement and interpreting these inequalities
as coherence we will show that if $\gal$ is incoherent then $\gal$ has
a extension $\gale$ which is also incoherent.

\begin{defn} For $\sAe=\setof{\ai{i},\ldots,\ai{n+1}}$, a
single-element extension of $\sA$, an $\f{}$-monotone path $\gale$ of 
  $(\Z{\sAe},\f{})$ is an extension of an $\f{}$-monotone path $\gal$
  of $(\Z{\sA},\f{})$ when $\gale \delete (n+1) = \gal$.
\end{defn}

Lemma ~\ref{l:coherent-projection} will be geometrically intuitive yet
only useful in corank 2 and above. Using Lemma
~\ref{l:coherent-projection} will require finding a projection 
$\left(\sAe\right)^*\to\sAd$. Unlike ~\ref{l:product-graph} this
will not provide structural insight into the monotone path
graph, it will only allow us to efficiently prove incoherence.

\begin{lemma} Suppose $\sAe=\setof{\ai{i},\ldots,\ai{n+1}}$ is a single-element extension of $\sA$ and $\f{}$ is a generic function on both
  $\Z{\sA}$ and $\Z{\sAe}$.  If $\gale$ is a coherent $\f{}$-monotone
  path of $(\sAe,\f{})$  then  
 $\gal=\gal^+\delete (n+1)$ is a coherent $\f{}$-monotone path of
  $(\sA,\f{})$.
\label{l:coherent-projection}
\end{lemma}

\begin{proof} Without loss of generality, we assume $f_i=1$ for
  all $i$. Further
  $\sAe$ is a single-element extension of $\sA$ so duality tells  us
  that $\left(\sAe\right)^*=\setof{\widehat{\aid{i}}}$ is a
  single-element lifting of $\sAd=\setof{\aid{i}}$ 
  with a projection $\pi:\widehat{\sA^*} \to \sA^*$
  mapping $\pi(\widehat{\aid{i}}) \to \aid{i}$ for $i\ne n+1$ and
  $\pi(\widehat{\aid{n+1}})=0$.

We first show that $\gal$ is an $\f{}$-monotone path of $(\Z{\sA},\f{})$. Each
covector $\gale^{(i)}$ is an acyclic orientation of $\sAe$ of the form:
\[
0=\sum_{i=1}^{n+1} c_i \widehat{\aid{i}},
\]
with $c_i>0$ for all $i$. We use the projection $\pi$ to construct
$\gal$ one acyclic orientation of $\sA$ at a time:
\[
\begin{split}
  \pi( 0 ) &= \pi\left(\sum_{i=1}^{n+1} c_i \widehat{\aid{i}}\right) 
            = \sum_{i=1}^{n+1} c_i \pi(\widehat{\aid{i}}) 
%           &= \sum_{i=1}^{n+1} \f{i} \aid{i} 
           = \sum_{i=1}^{n} c_i \aid{i} = 0.
\end{split}
\]
So $\gal$ is a gallery of $\sA$. We can use projection again
to show that $\gal$ is coherent for $\f{}$. Since $\gale$
is coherent in $\sAe$ for $f$ we know there must exist 
\[
\gga{1}<\gga{2}<\cdots<\gga{n}<\gga{n+1}
\]
satisfying
\[
\sum_{i=1}^{n+1} \g{i} \aid{i} = 0. 
\]
Applying $\pi$ we see
\[
  \pi( 0 ) = \pi\left(\sum_{i=1}^{n+1} g_i \widehat{\aid{i}}\right)
            = \sum_{i=1}^{n+1} g_i \pi(\widehat{\aid{i}}) 
            = \sum_{i=1}^{n} g_i \aid{i} = 0 .
\]

Note in particular that $\pi$ does not change $g_i$ so that inequalities 
\[
\gga{1}<\gga{2}<\cdots<\gga{n}
\]
still hold and so $\gal$ a coherent $\f{}$-monotone $(\Z{\sA},\f{})$.
\end{proof}

%% This seems like a different statement than we promised. To put Lemma
%% ~\ref{l:coherent-projection} in a practical form we will use its
%% contrapositive, which gives us our first method of finding incoherent
%% galleries without explicitly setting up inconsistent systems of
%% inequalities.

\begin{corollary} Given $\gale$ an $\f{}$-monotone path of
$(\Z{\sAe},\f{})$ and $\gal=\gale\delete(n+1)$ an incoherent
$\f{}$-monotone path $(\sA,\f{})$ then $\gale$ is incoherent as well.
\label{c:coherent-projection}
\end{corollary}

The geometry here is obscured by duality. Since $\sA$
and $\sAe$ are both hyperplane arrangements of $\R{d}$ the extension
$\gale$ extending $\gal$ is exactly the same path in $\R{d}$ with
an $n+1$ inserted in the geometrically appropriate place. If $\gal$ is
incoherent on $\sA$, then any sequence of inequalities
\[
\frac{\gga{1}}{\fg{1}} <
\frac{\gga{2}}{\fg{2}} <
\cdots <
\frac{\gga{n}}{\fg{n}} 
\]
is inconsistent and remains inconsistent no matter the value 
of $\g{\gale(n+1)}$.

\begin{example} Consider the following two hyperplane arrangements
\[
\sA=\bordermatrix{
&\ai{1} & \ai{2} & \ai{3} & \ai{4} \cr
&1 & 0 & 0 & 1  \cr
&0 & 1 & 0 & 1  \cr
&0 & 0 & 1 & -1 \cr
}
\quad
\text{ and }
\quad
\sAe=\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} & \ai{5} \cr
& 1 & 0 & 0 & 1  & 1 \cr
& 0 & 1 & 0 & 1  & 2 \cr
& 0 & 0 & 1 & -1 & 3 \cr
}.
\]
We recognize $\sA$ as the hyperplane arrangement from Example
~\ref{ex:gallery-coherent} so we know that the $\f{}$-monotone paths 
$\setof{1324, 2314, 4132,4231}$ are incoherent for
$\f{}(x,y,z)=3x+2y+z$. We then know that 
$\setof{13524,23514,25314,41352,42531}$ are incoherent in
$\sAe$ for $\f{}$ since each $\f{}$-monotone path of $(\sAe,\f{})$ is
a lifting of an $\f{}$-monotone path of $(\sA,\f{})$.
\label{ex:coherent-projection}
\end{example}

We close by illustrating how we use Lemma ~\ref{l:coherent-projection}
in practice. Example ~\ref{ex:coherent-projection} was a
straightforward illustration of using Lemma
~\ref{l:coherent-projection}. To use Corollary
~\ref{c:coherent-projection} in our classification of corank $2$
monotone path graphs, we must understand it with duality.

\begin{example} The hyperplane arrangements from example
~\ref{ex:coherent-projection} have Gale duals
\[
\sAd=\bordermatrix{
&\aid{1} & \aid{2} & \aid{3} & \aid{4} \cr
&-1 & -1 & 1 & 1 \cr
}
\quad
\text{ and }
\quad
\sAe^*=\bordermatrix{
&\widehat{\aid{1}} & \widehat{\aid{2}} & \widehat{\aid{3}} & \widehat{\aid{4}} & \widehat{\aid{5}} \cr
&-1 & -1 & 1  & 1 & 0 \cr
&-1 & -2 & -3 & 0 & 1 \cr
}. 
\]
To show that $\sAe$ is a single-element extension of $\sA$ we must give a
projection from $\pi:\sAe^*$ onto $\sAd$. Figure
~\ref{fig:coherent-projection} shows $\sAd$ and $\sAe^*$. We see that
$\pi(v)=(1,0)\cdot v$ projects $\sAe^* \to \sAd$, allowing us to say
$\sAe$ has incoherent galleries for every $f$.
\end{example}

\begin{figure}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=1.5]
\draw[->,dual] (0,0) -- (-.7,-.7);
\draw (-.8,-.8) node {$\aid{1}$};
\draw[->,dual] (0,0) -- (-.45,-.9);
\draw (-.52,-1.02) node {$\aid{2}$};
\draw[->,dual] (0,0) -- (.32,-.95);
\draw (.41,-1.05) node {$\aid{3}$};
\draw[->, dual] (0,0) -- (1,0);
\draw (1.2,0) node {$\aid{4}$};
\draw[->, dual,red] (0,0) -- (0,1);
\draw (0,1.15) node[red] {$\aid{5}$};
\end{tikzpicture}
%\includegraphics[width=\linewidth]{figures/coherent-projection-ex1}
\end{minipage}
\begin{minipage}{.45\linewidth}
\begin{tikzpicture}[scale=1.5]
\draw[->,dual] (0,0) -- (-.7,0);
\draw (-.85,.15) node {$\aid{1}$};
\draw[->,dual] (0,0) -- (-.45,0);
\draw (-.5,.15) node {$\aid{2}$};
\draw[->,dual] (0,0) -- (.32,-0);
\draw (.32,.15) node {$\aid{3}$};
\draw[->, dual] (0,0) -- (1,0);
\draw (1,.15) node {$\aid{4}$};
\end{tikzpicture}
%\includegraphics[width=\linewidth]{figures/coherent-projection-ex2}
\end{minipage}
\caption{Single element extensions in the Dual}
\label{fig:coherent-projection}
\end{figure}

The trick in using Corollary ~\ref{c:coherent-projection} is to
see a projection $\sAe^*$ to $\sAd$. In spite of the challenges Lemma
~\ref{l:coherent-projection} will be our main labor-saving tool for
showing $\f{}$-monotone paths are incoherent. In corank $1$, Lemma
~\ref{l:coherent-projection} does not tell us anything useful because
all corank $0$ hyperplane arrangements are universally all-coherent ~\cite{Billera94}. Corollary ~\ref{c:coherent-projection} will be most
useful Section ~\ref{s:crank2} in order to quickly show that
$(\sA,\f{})$ has incoherent galleries for small cases, leaving one
case to deal with by hand, but the major extension tool we will need
is a Lemma ~\ref{l:incoherent-lifting} for single-element liftings.
