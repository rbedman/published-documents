Oriented matroids generalize the similar combinatorial descriptions of
directed graphs, orthogonal pairs of real vector subspaces, point
configurations, vector configurations, and hyperplane arrangements
which are all described by collections of $\setof{+,-,0}$
vectors. Each motivating example has its own system of axioms each of
which subtly different but eventually cryptomorphic. The axioms share
a common notion of signed sets or sign vectors. In the setting of
hyperplane arrangements sign vectors will be natural, so we restrict our
attention to them.

\begin{defn} A vector $v \in \setof{+,-,0}^n$ is
  a \emph{sign vector}. For any vector (signed or otherwise), we will
  refer to component $i$ of $v$ as $v_i$. The support of a sign vector
  is the \emph{set} of nonzero components, denoted
\[
\supp{v}=\setof{i \,|\, v_i\ne 0}. 
\]
The negative  $-v$ of the sign vector $v$ is the sign vector with all $+$'s
and $-$'s flipped:
\[
-v_i =
\begin{cases}
0 & \text{ if } v_i=0 \\
- & \text{ if } v_i=+ \\
+ & \text{ if } v_i=- \\
\end{cases}.
\]
\end{defn}

We typically omit commas when writing sign vectors leading us to a
literal notation; by viewing sign vectors as words in the symbols
$\setof{+,-,0}$ we write the sign vector $++++$ as $(+)^4$ or $--++$ as
$(-)^2(+)^2$. The symbols $\setof{+,-,0}$ have a partial order
in which $0<+$, $0<-$, and $+$ and $-$ are incomparable. The partial
order of $\setof{+,-,0}$ extends to a partial order on sign vectors in
which $u<v \iff u_i < v_i$ for all $i$. 

\begin{defn} For sign vectors $u$ and $v$, the separation set of
  $u$ and $v$ is the set of non-zero components where $u$ and
  $v$ are not equal. 
\[
\sep{u,v}=\setof{i\,:\, u_i = -v_j \ne 0}
\]
\label{def:matroid-seperation}
\end{defn} 


\begin{example} Figure ~\ref{fig:matroid-poset} illustrates that the
sign vectors $++-+-$ and $+--++$ are incomparable as they are not
equal and non zero in positions $2$ and $5$ while both $++-+-$ and
$+--++$ are greater than $+0-+0$. 
\label{ex:matroid-poset}
\end{example}

\begin{figure}
\centering
\usetikzlibrary{trees}
\tikzstyle{level 2}=[level distance=1.5cm, sibling distance=3.5cm]
\tikzstyle{level 1}=[level distance=1.5cm, sibling distance=4cm]
\tikzstyle{covector} = [text width=5cm, text centered]
\begin{tikzpicture}[grow=up]
\node[covector]{$+\,0-+\,0$}
  child{ node[covector]{$++-+-$}}
  child{ node[covector]{$+--++$}};
\end{tikzpicture}
\caption{Poset structure of Oriented Matroids}
\label{fig:matroid-poset}
\end{figure}


%% In Example ~\ref{ex:matroid-poset} we  see that $+0-+0$ has
%% ``cut out'' positions $2$ and $5$, which is an idea
%% we will formalize shortly, but only after we have discussed sign
%% vector composition. 

\begin{defn} For two sign vectors $u$ and $v$, composition of
  $u$ and $v$ decreases the separation set of $u$ and $v$  by building
  a sign vector with larger support. Formally we define composition as
\[
\left(u \circ v\right)_i=
\begin{cases}
u_i & \text{ if } u_i \ne 0 \\
v_i & \text{ else}
\end{cases}.
\]
We say that $w$ eliminates $j$ between $u$ and $v$ when
  $w_j=0$ and $w_i=(u \circ v)_i\,$ for $i \not \in \sep{u,v}$. 
\label{def:matroid-composition}
\end{defn}

Composition and Elimination are both geometrically motivated
operations and we will return to them in Section
~\ref{ss:level2}. For now we simply develop the intuition that
composition and elimination are the reverse of each other.

Oriented matroids are sets of sign vectors called either Vectors or
Covectors which satisfy axioms for oriented matroids. Vectors and
Covectors define the same combinatorial structure using different
axioms. Possible systems of axioms to choose from, and the objects which they most easily
describe, include
\begin{itemize}
\item Circuit axioms (coming from directed graphs),
\item Orthogonality axioms (orthogonal pairs of real vector spaces),
\item Chirotope axioms (point configurations and convex polytopes),
\item Covector axioms (real hyperplane arrangements).
\end{itemize}
Details for all these axioms can be found in ~\cite{Björner93} along with
equivalence proofs. We are interested in hyperplane arrangements so we
use the covector axioms. 

\begin{defn} A set $\mcV{} \subset \setof{+,0,-}^n$ of sign vectors 
 satisfies the covector axioms and is the set of covectors of an
 oriented matroid $\mat{}$ when 
\begin{itemize}
\item $0^n \in \mcV{}$,
\item (symmetry) $u \in \mV{} \implies -u \in \mcV{}$
\item (composition) for all $u,v \in \mcV{}$, $u \circ v \in \mcV{}$, 
\item (elimination) for all 
  $u,v \in \mcV{}$, $j \in S(u,v) \implies \exists w \in \mcV{}$ 
  so that $w$ eliminates $j$ between $u$ and $v$.
\end{itemize}
\label{def:matroid-covector}
\end{defn}

The \emph{rank} of an oriented matroid is defined as the length of the
longest chain of covectors $0 < c_1 < c_2 < \cdots < c_d$ with $c_i$
in $\mcV{}$. A covector also has rank and corank in $\mat{}$: The rank of
$c$ is is the length of the longest chain $0 < c_1 < \ldots < c_r=c$;
the corank of a vector $c$ is the length of the longest chain
$c=c_{d-r}<\cdots<c_d$ in which $c_d$ is a maximal covector of
$\mat{}$. The rank function gives oriented matroids a lattice
structure $L(\mat{})=\bigsqcup_{i=0}^d L_i(\mat{})$ in which
$L_i(\mat{})$ is the set of all corank $i$ covectors of $\mat{}$.

An oriented matroid also has a set of vectors $\mV{}$ which also
satisfy the covector axioms and which carry equivalent combinatorial
data. We will refer to $\mcV{}$ as the covectors of $\mat{}$ and
$\mV{}$ as the vectors of $\mat{}$. It is important to note that
through vectors and covector, oriented matroids have duality built-in.

\begin{defn} An oriented matroid $\matd{}$ is the dual of $\mat{}$ when
  $\mat{}$ and $\matd{}$ are related in the following way:
\begin{itemize}
\item The vectors of $\matd{}$ are the covectors of $\mat{}$.
\item The covectors of $\matd{}$ are the vectors of $\mat{}$.
\end{itemize}
\label{def:matroid-duality}
\end{defn}
We will not dwell on this duality but will return to it when working with vector
configurations.

\begin{example} We check the covector axioms of Definition 
 ~\ref{def:matroid-covector} for the set
\[
\mcV{}=\setof{
\begin{split}
+++,\quad0++,\quad-++,\quad&-+0,\quad-+-,\quad-0- \\
---,\quad0--,\quad+--,\quad&+-0,\quad+-+,\quad+0+ \\
&000
\end{split}
}.
\]
Looking at the axioms one at a time we see:
\begin{itemize}
\item Identity: $0 \in \mcV{}$.
\item Symmetry: $-c$ is directly above or below $c$. 
\item Composition: $u \circ v$ of $u$ and $v$ remains in the same row as $v$.
\item Elimination: $w$ which eliminates $u$ and $v$ is either $000$ or
  located  between $u$ and $v$.
\item The rank of $\mat{}$ is $2$. 
\end{itemize}
\label{ex:matroid-simple}
\end{example}

Oriented matroids are the combinatorial structure behind zonotopes and
hyperplane arrangements and we can understand galleries purely
oriented matroid terms. Galleries are paths from a distinguished
covector $(-)^n$ to its antipodal covector $(+)^n$ so we are
interested in the specific oriented matroids which have $(+)^n$ as a
maximal covector. Matroid which have $(+)^n$ as a maximal covector are
called acyclically oriented matroids. 

\begin{defn} A pair $(\mat{},(+)^n)$ is an acyclically oriented
matroid if $\mat{}$ is an oriented matroid and $(+)^n$ is a maximal
covector of $\mat{}$. 
\end{defn}

Galleries and flips are both cellular strings, so we first define
cellular strings then specialize the definition for galleries and
flips. 

\begin{defn} A cellular string of an acyclically oriented matroid
$(\mat{},(+)^n)$ is a sequence of covectors 
$\cell=\cell{1}\big|\cdots\big|\cell{m}$ 
of $\mat{}$ with
$\cell{i}\circ\left((-)^n\right)=\cell{i+1}\circ\left((+)^n\right)$
for $1 \le i \le m$ with $\cell{0}=(-)^n$ and $\cell{m+1}=(+)^n$. 
\end{defn}
A
gallery is a cellular string in which each cell is of corank 1. Galleries are
important enough to get their own notation: A gallery $\gal$ is a
sequence of corank $1$ covectors $c_i$ which we write $\gal^{(i)}=c_i$.
Similar to a gallery a flip is a cellular string in which a
distinguished cell has corank $2$ and all other cells are of corank
$1$. We will often refer to
a flip by its distinguished corank $2$ covector. We say that galleries
$\gal$ and $\galp$ are adjacent by a flip $X$ when both $\gal$ and $\galp$ are both refinements of $X$. 

\begin{defn} The vertices of the graph $\gtwo{\mat{},(+)^n}$ are
galleries of $\mat{}$. Two galleries are adjacent in
$\gtwo{\mat{},(+)^n}$ when they are adjacent by a flip.
\end{defn}

\begin{example} Using the oriented matroid of Example
~\ref{ex:matroid-simple} we see that there are two galleries which we
write as the rows of a matrix for clarity:
\[
\gal=\begin{pmatrix}
\gal^{(1)}= 0-- \\
\gal^{(2)}= +-0 \\
\gal^{(3)}= +0+ \\
\end{pmatrix}
\qquad
\galp=\begin{pmatrix}
\galp^{(1)}= -0- \\
\galp^{(2)}= -+0 \\
\galp^{(3)}= 0++ \\
\end{pmatrix}
\]
The two galleries are adjacent by the flip $000$ which is the unique
covector of corank 2. The graph $\gtwo{\sA,(+)^n}$ has two nodes
connected by a single flip. 
\end{example}




In this setting we have sign vectors encoding important
information about vector configurations.

\section{Realized covectors and Pointed configuration}
 %% and real hyperplane
%% arrangements. Our omission of abstract oriented matroids is no great
%% loss in light of ~\cite{Folkman78}.



\section{older stuff}





The last property of oriented matroids which we require is \emph{acyclic} orientations.

\begin{defn}We say an oriented matroid is \emph{acyclic}
  when $+^n$ is a covector of $\mat{}$. When $\mat{}=\mat{V}$ and
  $\mat{}$ is acyclic, we say
  that $V$ has an acyclic orientation. 

  For realized oriented matroids,
  the covectors of $V$ are valuations of functions $\f{} \in \Rd{d}$
  on $V$, so a realized acyclic orientation of $V$ is
  a function $\f{}$ such that $\f{}(v_i)>0$ for all $i$ and 
  we say that $V$ has an acyclic orientation induced by $\f{}$.
 %% and
 %%  will have the useful notation of $\f{}(v_i)=\f{i}$ for the
 %%  valuations of $V$.

  Acyclic orientations are dual to \emph{totally cyclic orientations},
  which means that $+^n$ is a vector  of $\mat{}$. 
\label{def:matroid-acyclic} 
\end{defn}

Duality gives a way to visualize acyclic orientations. We
know that $V$ has an acyclic orientation, so $+^n$ is a
covector of $\mat{V}$ and is realized by a functional $\f{}$ with
$\f{i}=\f{}(v_i)>0$. In the dual we have $\sum \f{i} v_i^*=0$ for
$\f{i}>0$ so the convex hull of $v_i^*$ captures the origin. 

To illustrate its power, we now use duality to recall the complete
classification of vector configurations in corank 1 due to
~\cite{McMullen71}. We say that vector configuration $V$ has corank 1
when it consists of $d+1$ vectors in $\R{d}$. Our classification in
corank 1 follows from oriented matroid duality.

\begin{example} 
\end{example}






