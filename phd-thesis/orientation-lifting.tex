Our second lemma discusses acyclic orientations and galleries of
single-element liftings. We defined single-element liftings in Section
~\ref{ss:level2} and we
recall the key idea: A single-element lifting $\sAl$ of $\sA$
preserves corank and when $\sA$ consists of $n$ vectors in $\R{d}$,
$\sAl$ will consist of $n+1$ vectors in $\R{d+1}$. Our goal is to
understand the galleries of $(\sAl,\widehat{c})$ from the galleries of
$(\sA,c)$. The answer is elegant and complete; every
gallery $\gal$ of $\sA$ has exactly $n+1$ liftings in $\sAl$, however
not every gallery of $\sAl$ will be a lifting.

We begin by understanding the acyclic orientations
of $\sAl$. This understanding of acyclic orientations of $\sAl$ serves
double duty providing an understanding of when $\sAl$ has an acyclic
orientation (always) and which galleries $\gall$ of
$(\sAl,\widehat{c})$ are liftings of galleries of $(\sA,c)$. 
Galleries are sequences of acyclic orientations of
$\setof{\pm \aid{i}}$ and by lifting each acyclic orientation into an
appropriately chosen $\sAld$ we can build galleries $\gall$ in
$\sAld$. We rely heavily on the notation introduced in Lemma
~\ref{def:matroid-acyclic} of $\f{i}=\f{}(\ai{i})$.

\begin{lemma} If $\sAl$ is a single-element lifting of $\sA$ and
  $\f{}$ is a generic functional on $\Z{\sA}$ realizing $(\sA,c)$
  there is a lifting $\widehat{c}$ of $c$ and $\fl{}$ of $\f{}$ which
  is generic on $\Z{\sAl}$ and realizes $(\sAl,\widehat{c})$.
\label{l:orientation-lifting}
\end{lemma}

\begin{proof} Since $\sAl$ is a single-element lifting of
  $\sA$, we know that $\sAld=\setof{\aid{1},\ldots,\aid{n}} \cup \setof{\aid{n+1}}$
  is a single-element extension of
  $\sAd=\setof{\aid{1},\ldots,\aid{n}}$. The function $\f{}$ induces
  an acyclic orientation on $\sA$ so $\f{i}>0$ for $1\le i \le n$. Our
  goal is to find $\setof{\fl{i}>0}$ for $1\le i \le n+1$.

  Since $\f{}$ induces an acyclic orientation on $\sA$, we know there
  are strictly positive 
  $\f{i},\ldots,\f{n}>0$ which satisfy
  $\sum \f{i} \aid{i}=0$. Since $\sA$ is essential, we also know
  $\aid{n+1}$ is a linear combination of $\setof{\aid{1},\ldots,\aid{n}}$
  so there exist $\alpha_1,\ldots,\alpha_{n}\in \R{}$, such that 
  $\sum_{i=1}^n \alpha_i a_i^*-a_{n+1}^*=0$. For some
  $N>\max(\setof{\alpha_i/\f{i}} \cup \setof{0})$ define
  \[
  \fl{i} = 
  \begin{cases}
    \f{i}-\frac{\alpha_i}{N}  & \mbox{ if } 1 \le i \le n, \\
    \frac{1}{N}             & \mbox{ when } i=n+1. 
  \end{cases}
  \]
  
We claim that $\fl{i}>0$ come from some functional
  $\fl{}\in \Rd{d+1}$, as
  \[
  \begin{split}
    \sum_{i=1}^{n+1}\fl{i}\aid{i}
    &= 
    \sum_{i=1}^{n}\left(\f{i} -\frac{\alpha_i}{N}\right) \aid{i}* +
    \frac{1}{N}\aid{n+1} \\
  &=
  \sum_{i=1}^{n}\f{i}\aid{i}+\frac{1}{N}\left(\aid{n+1}-\sum_{i=1}^{n}\alpha_i \aid{i}\right)
  \\
  &= 0 + 0 = 0.
  \end{split}
  \]
  Further, as $\alpha_i/\f{i} < N$ for any $i$ we know that
  $\fl{i}=\fl{i}-\alpha_i/N>0$ for $1\le i \le n$ and since $N>0$,
  $\fl{n+1}=1/N>0$. This proves that:
  \begin{itemize}
  \item $\fl{i}=\fl{}(\ali{i})$ is an acyclic orientation of $\sAl$,
  \item $\widehat{c}=(+)^{n+1}$ is a  chamber of $\sAl$, and
  \item $\fl{}$ is generic on $\sAl$. 
\end{itemize}   
\end{proof}

According to Definition ~\ref{def:gallery-1} a gallery
$\gal$ of $(\sA,c)$ is path on $\gone{\sA}$, each vertex of which is a
maximal covector of $\mat{\sA}$. Each maximal
covector is an acyclic orientation of $\sA$ so we can use Lemma
~\ref{l:orientation-lifting} on every covector maximal covector $\gal$ to
build a covector in $\sAl$ and a gallery $\gall$ of $\sAl$. 

\begin{defn} Given a pointed hyperplane arrangement $(\sA,c)$, a
generic functional $\f{}$ on $\Z{\sA}$ realizing $(\sA,c)$ and the
lifting $(\sAl,\widehat{c})$ of Lemma ~\ref{l:orientation-lifting}, we
say a gallery $\gall$ of $(\sAl,\widehat{c})$ is a lifting of gallery
$\gal$ if $\gall \contract (n+1)=\gal$
\end{defn}


\begin{corollary} If $\sAl$ is a single-element lifting of $\sA$ and $\gal$
  is a gallery for $\sA$ then any $\gall$ such that
  $\gall\contract (n+1)=\gal$ is a gallery for  $\sAl$.
\label{c:orientation-lifting-gallery}
\end{corollary}

\begin{proof} Viewed geometrically, a gallery $\gal$ is a path from
  $(-)^n$ to $(+)^n$ on $\gone{\sA}$. Every vertex $\gal^{(i)}$ of
  $\gal$ is a chamber of $\sA$, which gives an acyclic
  orientation of $\setof{\pm \ai{i}}$.
  By repeated application of \ref{l:orientation-lifting} each acyclic
  orientation of $\setof{\pm \ai{i}}$ with $1 \le i \le n$ can be
  lifted to an acyclic orientation of $\setof{\pm \ai{i}}$. Picking
  where to insert $n+1$ is picking we switch from lifting into 
  $\sAd\cup \setof{-\ai{n+1}}$ or $\sAd\cup \setof{\ai{n+1}}$.
\end{proof}

Corollary ~\ref{c:orientation-lifting-gallery} tells us
that liftings of galleries always exist which we illustrate with an example.

\begin{example} We return to the configurations from Examples
  ~\ref{ex:matroid-lifting} and ~\ref{ex:product-zonotope}, which we
  specify in the dual.
\[
\begin{split}
\sAd&=\begin{pmatrix}
-1 & 1 & 1 \\
\end{pmatrix}\\
\sAld&=\begin{pmatrix}
-1 & 1 & 1 & 1 \\
\end{pmatrix}
\end{split}
\]
We can check that $\Gamma(\sA)=\setof{213,312}$ (e.g. Example
~\ref{ex:hyperplane-ex}). Corollary
~\ref{c:orientation-lifting-gallery} produces $8$ galleries of $\sAl$,
\[
\setof{
4213, 4312,
2413, 3412,
2143, 3142,
2134, 3124
}\subseteq \Gamma(\sAl).
\]
We can list the galleries of $\sAl$ by hand using $\sAld$
\[
\Gamma(\sAl)=\setof{
\begin{tabular}{ccc}
4213, &4312, &4123, \\
2413, &3412, &4132, \\
2143, &3142, &2314, \\
2134, &3124, &3214 \\
\end{tabular}
}.
\label{ex:lifting-labels}
\]

\begin{figure}
\input{figures/mpg-n4-k1-lifting.tex}
\caption{Graph $\gtwo{\sA,c}$ from Example ~\ref{ex:lifting-labels}
with non-lifted nodes highlighted in red.}
\label{fig:lifting-labels}
\end{figure}

We see that all the galleries predicted by
Corollary ~\ref{c:orientation-lifting-gallery} are present in
$\sAl$ along with $4$ additional galleries. The galleries 
$2314, 3214, 4123$, and $4132$ are not liftings of any galleries of
$\sA$ as they do not contain either $213$ or $312$ as subwords.
\label{ex:orientation-lifting}
\end{example}

Corollary ~\ref{c:orientation-lifting-gallery} guarantees the lifting
of galleries, however Example ~\ref{ex:orientation-lifting}
illustrates that there is no converse in general. There is a partial
converse which relies only on the acyclically oriented matroid
$(\mat{\sA},(+)^n)$ realized by $(\sA,c)$. 

\begin{lemma} For $\sAl$ a single-element lifting of $\sA$ and 
an acyclically oriented matroid $(\mat{\sAl},(+)^{n+1})$, if both $(n+1,\gal)$ and $(\gal,n+1)$ are
galleries of $(\mat{\sAl},(+)^n)$, then 
\begin{itemize}
\item $(+)^n$ is a maximal covector of $\mat{\sA}$ so
$(\mat{\sA},(+)^n)$ is an acyclically oriented matroid, 
\item and $\gal$ is a gallery of $(\mat{\sA},(+)^n)$.
\end{itemize}
\end{lemma}

\begin{proof}
We assume we have ordered $\sAl$ so that 
$\sAld=\sAl \cup \setof{\ali{n+1}}$. 
We show that $\sA$ has an acyclic orientation and
$\gal=(1,2,\ldots,n)$ is a gallery of $\sA$ by repeatedly applying the
elimination property of oriented matroids to
$\gallg$ and $\gallgp$. 

Both $\gallg$ and $\gallgp$ are galleries of $\sAl$ and we know each
covector of $\gall^{(i)}$ and $\gallgp^{(i)}$. Figure
~\ref{fig:incoherent-lifting} illustrates the covectors of
$\gallg^{(i)}$ and $\gallgp^{(i)}$. In particular,
$\gallg^{(0)}(0)=\gallgp^{(0)}=-^{n+1}$ and $\gallg^{(1)}=-^{n}+$ so
the elimination axiom forces $-^n0 \in \mat{\sAl}$ and thus
$-^n \in \mat{\sA}$.  We then also know, using the symmetry axiom,
that $(+)^n\in\mat{\sA}$ so $(\sA,(+)^n)$ is an acyclically oriented
matroid.

Likewise for $1 \le k \le n$ we know
\[
(+)^k(-)^{n-k}-=\gallgp^{(k)} \text{ and }
(+)^k(-)^{n-k}+=\gallg^{(k+1)} 
\]
are covectors of $\mat{\sAl}$, so $(+)^k(-)^{n-k}0$ is a covector of
$\mat{\sAl}$ also and $(+)^k(-)^{n-k}$ is a covector of
$\mat{\sA}$. We have constructed $\gal^{(k)}$ for any $k$ so
$(1,2,\ldots,n)$ is a gallery of $\sA$.

%% We know that 
\end{proof}

\begin{figure}
\begin{minipage}{.45\linewidth}
\centering
$\displaystyle
\begin{pmatrix}
- & - & \ldots & - & - \\

- & - & \ldots & - & + \\
+ & - & \ldots & - & + \\
\vdots & \vdots & \ddots & \vdots & \vdots \\
+ & + & \ldots & + & + \\
\end{pmatrix}
$
\end{minipage}
\begin{minipage}{.45\linewidth}
\centering
$
\displaystyle
\begin{pmatrix}
- & - & \ldots & - & - \\
+ & - & \ldots & - & - \\
+ & + & \ldots & - & - \\
\vdots & \vdots & \ddots & \vdots & \vdots \\
+ & + & \ldots & + & + \\
\end{pmatrix}
$
\end{minipage}
%\caption{Covectors of $\gallg$ and $\gallgp$}
\caption{$\gallg$ and $\gallgp$ as a column of covectors}
\label{fig:incoherent-lifting}
\end{figure}

This converse will be useful for Lemma 
~\ref{l:incoherent-lifting}.  For now we have an adequate
understanding of lifting of galleries and move on to ask ``Which
galleries are coherent?''





