Our third lemma is a labor-saving device for classifying
all-coherent monotone path graphs of generic functionals on
zonotopes. Our
goal is showing that, if every $\f{}$-monotone path of $(\Z{},\f{})$ is
coherent, then $(\Z{},\f{})$ is all-coherent. This result is a
surprise because of examples in which two coherent
triangulations are adjacent by an incoherent bistellar flip. Our result will have implications
for Section ~\ref{ss:cr1-zonotopal} when we find functionals for any
cellular string, even though describing galleries alone would be
enough. We will rely on this result in Sections
~\ref{ss:cr2-zonotopal-1} and ~\ref{ss:cr2-zonotopal-2} where 
it is arduous to describe the functionals of an arbitrary cellular
strings. The labor-saving aspect of this lemma allows us to describe
functionals which select any $\f{}$-monotone path, then extend the
functionals to any cellular string using the results of this section.

We recall that a cellular string
$\cell{}=\cell{1}\big|\cell{2}\big|\ldots\big|\cell{n-m}$ of
$(\Z{\sA},\f{})$, a generic functionals on a zonotope, is a disjoint
union of the vectors of $\sA{}$ satisfying conditions spelled out in
Definition ~\ref{def:gallery-cellular-string}. A cellular string is
coherent when selected by a functional; proving coherence of cellular
strings means finding a linear dependence $\setof{\g{i}}$
on $\sAd$ and satisfying
\[
\underbrace{
\frac{\g{1}}{\f{1}}=\cdots=
\frac{\g{\abs{\cell{1}}}}{\f{\abs{\cell{1}}}}
}_{\cell{1}} 
<
\cdots
<
\underbrace{
\frac{\g{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)}}
{\f{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)}}
=
\ldots
=
\frac{\g{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}
{\f{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}
}_{\cell{k}}
<
\cdots
<
\underbrace{
\frac{\g{n-\abs{\cell{{n-m}}}+1}}{\f{n-\abs{\cell{{n-m}}}+1}}=\cdots=
\frac{\g{n}}{\f{n}}
}_{\cell{n-m}}.
\]
%~\ref{def:gallery-cellular-string-coherent}

\begin{lemma} Let $\f{}$ be a generic functional on a
zonotope $\Z{\sA}$ and $\cell{}$ a cellular string of
$(\Z{\sA},\f{})$. If every
$\f{}$-monotone path refining $\cell{}$ is coherent then $\cell{}$ is
coherent.
\label{l:coherent-strings}
\end{lemma}

\begin{proof} 
%General hypothesis. 
Without loss of generality, we assume that $\ai{i}$ have been scaled
so $\f{i}=1$ for all $i$. Let $\cellp{}=\cellp{1}\big|\cellp{2}\big|\ldots\big|\cellp{n-m'}$ be a cellular string of
$(\Z{\sA},\f{})$  which refines $\cell{}$.

We will work by induction on
$m$. When $m'=0$, $\cellp{}$ is a gallery of $\sA$ and coherent for
$\f{}$ by hypothesis.
%State Inductive Hypothesis, and careful statement of what we want to
%prove.
We now assume that all cellular strings
$\cellp{}=\cellp{1}\big|\cellp{2}\big|\ldots\big|\cellp{n-m^\prime}$
refining $\cell{}$ are coherent, so $m^\prime<m$. Assume that $\sA$ is ordered so 
\[
\begin{split}
\cell{}&=
\underbrace{\setof{\ai{1},\ldots,\ai{\abs{\cell{1}}}}}_{\cell{1}}
\bigg|
\ldots
\bigg|
\underbrace{\setof{\ai{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)},\ldots,\ai{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}}_{\cell{k}}
\bigg|
\ldots
\bigg|
\underbrace{\setof{\ai{1+n-\abs{\cell{n-m}}},\ldots,\ai{n}}}_{\cell{n-m}}.
\end{split}
\]
To make $\cellp{}$ coherent we must find a linear dependence
$\setof{x_i}$ of $\sAd$ ordered so that
\[
\underbrace{x_1=\cdots=x_{\abs{\cell{1}}}}_{\cell{1}} 
<
\cdots
<
\underbrace{x_{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)}=\cdots=x_{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}_{\cell{k}}
<
\cdots
<
\underbrace{x_{n-\abs{\cell{{n-m}}}+1}=\cdots=x_n}_{\cell{{n-m}}}.
%\label{eq:coh-str-coh}
\]
For $m>1$ there is a cell $\cellp{k}$ which is zonotopal face
$\Z{\cellp{k}}$ of $\Z{\sA}$ having $\abs{\cell{k}}>1$. We take
$\cell{k}^\prime=\scell{k,1}\big|\scell{k,2}\big|\ldots\big|\scell{k,\ell}$,
a maximal, proper cellular string of $(\Z{\cell{k}},f)$. The
refinement $\cell{k}^\prime$ exists because $\abs{\cell{k}}>1$ . We
build two longer cellular strings using $\cell{}$ with the cell
$\cell{k}$ replaced by $\cell{k}^\prime$ and its reversal:
\[
\begin{split}
\cell{}^{\g{}}&=
\cell{1}
\big|
\ldots
\big|
\cell{k-1}
\big|
\underbrace{\scell{k,1} \big| \ldots \big|
\scell{k,\ell}}_{\cell{k}, \text{ refined to } \cell{k}^\prime}
\big|
\cell{k+1}
\big|
\ldots
\bigg|
\cell{n-m}, \\
\cell{}^{\gp{}}&=
\cell{1}
\big|
\ldots
\big|
\cell{k-1}
\big|
\underbrace{\scell{k,\ell} \big| \ldots \big|
\scell{k,1}}_{\cell{k}^\prime  \text{ reversed } }
\big|
\cell{k+1}
\big|
\ldots
\bigg|
\cell{n-m}. \\
\end{split}
\]
The cellular strings $\cell{}^{\g{}}$ and $\cell{}^{\gp{}}$ are of
length $n-m+\ell-1>n-m$ and coherent for $(\Z{\sA},\f{})$. Let
$\ai{\alpha_1} \in \scell{k,1}$ and $\ai{\alpha_2} \in \scell{k,2}$
and use Lemma ~\ref{l:gallery-change-g} to pick $\setof{\g{i}}$ and
$\setof{\gp{i}}$ making $\cell{}^{\g{}}$ and $\cell{}^{\gp{}}$
coherent to satisfy
$\g{\alpha_2}-\g{\alpha_1}=\gp{\alpha_1}-\gp{\alpha_2}$ guaranteeing
that $\g{\alpha_1}+\gp{\alpha_1} = \g{\alpha_2}+\gp{\alpha_2}$. 
We define 
\[
x_{i}=\g{i}+\gp{i} \qquad \text{ for } 1 \le i \le n.\\
\]
We check that $\setof{x_i}$ are a linear dependence of $\sAd$ and thus
$x_i=x(\ai{i})$ with the computation:
\[
\begin{split}
\sum_{i=1}^n x_i \aid{i}&=\sum_{i=1}^n \left(\g{i}+\gp{i} \right)\aid{i} \\
                  &=\sum_{i=1}^n \g{i}\aid{i} + \sum_{i=1}^n\gp{i}\aid{i} \\
&= 0+0=0.
\end{split}
\]
Since $\cell{}^{\g{}}$ and $\cell{}^{\gp{}}$ agree outside of $\cell{k}$ we
 know that $x$ selects a cellular string 
that agrees with $\cell{}$ outside of $\cell{k}$. We must show
  that the constants $x_{(\cell{k,i})}$ are equal for all 
  $1 \le i \le \ell$. From our choice of $\setof{\g{i}}$ and
  $\setof{\gp{i}}$ we know 
\[
\begin{split}
x_{(\cell{k,1})}&=x_{\alpha_i} \\
&=\g{\alpha_1}+\gp{\alpha_1} =\g{\alpha_2}+\gp{\alpha_2}\\
&=x_{\alpha_2} = x_{(\cell{k,2})}.
\end{split}
\]
The function $x$ then selects a cellular string
through $\Z{\cell{k}}$ which is coarser than the maximally course, proper $\cell{k}^\prime$ cellular string $\Z{\cell{k}}$, so $x$ must be
constant on all $\cell{k}$. We now know that
\[
\underbrace{x_1=\cdots=x_{\abs{\cell{1}}}}_{\cell{1}} 
<
\cdots
<
\underbrace{x_{\left(1+\sum_{i=1}^{k-1}\abs{\cell{i}}\right)}=\cdots=x_{\left(\sum_{i=1}^{k}\abs{\cell{i}}\right)}}_{\cell{k}}
<
\cdots
<
\underbrace{x_{n-\abs{\cell{{n-m}}}+1}=\cdots=x_n}_{\cell{{n-m}}},
\]
so $\cell{}$ is coherent for $\f{}$. We conclude that any cellular
string $\cellp{}$ refining $\cell{}$, including $\cell{}$ itself, is coherent for $(\Z{\sA},\f{})$ by induction.
\end{proof}

The key idea here was to use Lemma ~\ref{l:gallery-change-g} and
divide by a positive number. We remark that this lemma is surprising
in light of ~\cite[Example 5.3.4]{DeLoera10} which exhibits a pair of
coherent triangulations of a point configuration having an incoherent
bistellar flip between them. The central symmetry of
$\Z{\sA}$ distinguishes $\f{}$-monotone path graphs from coherent
triangulations. When $\Z{\sA}$ is centrally symmetric there is symmetry
which gives an involution $\cell{}\to -\cell{}$ on cellular strings of
$(\Z{\sA},\f{})$. This involution is missing for triangulations and
fiber polytopes in general.

%This lemma is labor-saving. If every gallery of


The final theorem of this section was pointed out by V. Reiner. 
\begin{theorem} Fix an acyclically oriented matroid $(\mat{}, ++\ldots+)$,
and consider its various realizations via pairs $(\sA,f)$. Then every
realization $(\sA,f)$ of $(\mat{},++\ldots+)$ will be all-coherent if and only
if there exists at least one such realization $(\sA,f)$ which is
all-coherent. In particular, the pair $(\sA,c)$ is universally
all-coherent if and only if there exists some generic $\f{}$ on $\sA$
inducing $c$ for which $(\sA,\f{})$ is all-coherent.
\label{l:thm-vic}
\end{theorem} 

\begin{proof} The forward implication is trivial. For the reverse,
suppose that $(\sA,\f{})$ is an all-coherent realization of 
$(\mat{},++\ldots+)$ and let $(\sAp,f')$ be another realization of
$(\mat{},++\ldots+)$. Let $Z$ and $Z'$ be the fiber zonotopes of
$(\sA,f)$ and $(\sAp,f')$ respectively, both of dimension
$d-1$. Denote by $P$ and $P'$ the face posets of $Z$ and $Z'$, after
omitting the empty face $\emptyset$ from each. Hence, the order
complexes $\Delta P$ and $\Delta P'$ both triangulate $d-2$ spheres,
namely the barycentric subdivisions of the boundaries of $Z$ and $Z'$

Since $(A,f)$ is all-coherent every cellular string $\cell{}$ of
$(\mat{},++\ldots,+)$ appears in $L$ and hence one has an inclusion
$P' \xhookrightarrow{} P$ as an induced subposet. Then $P'=P$ using 
~\cite[Lemma 3.3]{Athanasiadis00}, so we conclude $(\sA,f')$ is also
all-coherent.
\end{proof}

This insightful Theorem was understood only after we gave several
proofs of universal all-coherent explicitly for every $\f{}$. We leave
our proofs intact to illustrate the remarkable nature of universal
all-coherence and gladly include this theorem as a labor-saving
device for future researchers. 
