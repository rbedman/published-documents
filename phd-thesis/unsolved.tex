We now turn our attention to topics of ongoing investigation. Some of
these questions, such as the main question of ~\cite{Reiner12} we
asked at the outset of our research and have remained
elusive. Others are question that came up during our work and deserve
further investigation.

\section{When does the diameter equal $\abs{L_2}$?}
\label{ss:unsolved-diameter}
With Theorem ~\ref{thm:cr1-diameter} we have an answer to the diameter
question of ~\cite{Reiner12} in corank $1$, obtained by find an
$L_2$-accessible node for any graph $\gtwo{\sA,c}$. Such methods
cannot give a complete answer in corank $2$, as Example
~\ref{ex:unsolved-noL2} has no $L_2$-accessible galleries.

\begin{example} A hyperplane arrangement we have looked at but have not
featured prominently is the arrangement:

\begin{tabular}{ccc}
\begin{minipage}{.35\linewidth}
\[
\sA=\bordermatrix{
& \ai{1} & \ai{2} & \ai{3} & \ai{4} & \ai{5} \cr
& 1 & 0 & 0 & 1  & 1 \cr
& 0 & 1 & 0 & -1 & -2 \cr
& 0 & 0 & 1 & 1  & 3 \cr
}
\]
\end{minipage}

\begin{minipage}{.6\linewidth}
,with Gale dual:
\begin{tabular}{cc}
\includegraphics[scale=.5]{data/img/g2-class-3} 
& 
\begin{tikzpicture}[scale=.55]
\draw[->,very thick] (0,0) -- (-1,2);
\draw[->,very thick] (0,0) -- (1,-1);
\draw[->,very thick,red] (0,0) -- (-1,0);
\draw[->,very thick] (0,0) -- (1,1);
\draw[->,very thick] (0,0) -- (-1,-2);
\end{tikzpicture}
\end{tabular}
%\raisebox{-.45\totalheight}{\includegraphics[scale=.5]{data/img/g2-class-3}}.
\end{minipage}

\end{tabular}

%Which we recognize from or classification as $5-3$. 
This example has diameter equal to $\abs{L_2}$ automatically since
$\sA \subset \R{3}$ ~\cite[Theorem 2.5]{Cordovil93}, ~\cite[Remark 2.6]{Reiner12} and is a minimal obstruction
to universal all-coherence in corank 2. A careful inspection of the
graph $\gtwo{\sA,c}$, illustrated in Figure ~\ref{fig:unsolved-noL2},
where we have labeled each flip with the pair of hyperplanes that intersect
at its non-trivial cell, 
reveals $\gtwo{\sA,c}$ has no $L_2$-accessible galleries.
\label{ex:unsolved-noL2}
\end{example}

\begin{figure}
\input{figures/mpg-n5-noL2.tex}
\caption{The monotone path graph with no $L_2$-accessible nodes
discussed in Example ~\ref{ex:unsolved-noL2}.}
\label{fig:unsolved-noL2}
\end{figure}

%% \begin{question} Does the diameter of $\gtwo{\sA,c}$ equal $\abs{L_2}$ in corank 2?
%% \end{question}
\section{What are the universally all-coherent families in higher corank?}

We have classified all universally all-coherent pointed hyperplane
arrangements in coranks 1 and 2. Corank 3 is what Sturmfels calls
``the threshold for counterexamples'' ~\cite{Sturmfels88} and poses
additional challenges over coranks $1$ and $2$. In particular, a
finite enumeration of classes in corank $3$ is impossible. Our main
tools of classification Lemma ~\ref{l:coherent-strings} and Lemma
~\ref{l:incoherent-lifting} are valid for arbitrary corank and our
computational tools are powerful enough to look at examples.

Computational results indicate that there are pointed hyperplane
arrangements which are all-coherent for certain $\f{}$ in corank 3 and
above and therefore universally all-coherent by Theorem
~\ref{l:thm-vic}. These families extend the type I family of Section
~\ref{ss:cr2-zonotopal-1} in corank $k$ and take the form
$\sAd=\setof{e_1,\ldots,e_k,-\sum_{i=1}^k e_i,v,v,\ldots,v}$ for
generic $v$. We have listed gallery counts for these arrangements when
$n$ is small in 
Table ~\ref{tab:allcoh-type1}. We strongly suspect that this family is
universally all-coherent and that a proof of coherence for these
families is directly analogous to the proof given in Section
~\ref{ss:cr2-zonotopal-1}. Table ~\ref{tab:allcoh-type1} suggests that
the number of galleries in galleries will be counted by
\[
\Gamma(\sA,c)=k!(d-1)!\binom{d+1}{k+1},
\]
and we expect an enumeration of the galleries to be similar to the
enumeration in Lemma ~\ref{l:cr2-allcoh-1-enum}. We have not, however,
attempted to give functionals selecting galleries for an arbitrary
$\f{}$, preferring to keep our focus on corank 2. 
\input{table-allcoh-type1.tex}

In addition, we expect to find corank $k$ analogs of the type II universally
all-coherent family. Proving universal all-coherence is important but
is not the most exciting task. The main reason to further study these
examples is to try to find all possible, all-coherent pointed hyperplane
arrangements. After finding \emph{all} all-coherent pointed hyperplane
arrangements and removing them from the list of all hyperplane
arrangements, you know where to look for minimal obstructions. 

\section{What are the minimal obstructions in corank $k>2$?}

In addition to our suspected family of universally all-coherent
pointed hyperplane arrangements in corank $k$, we can ask about
minimal obstructions in corank $k$. Corank $1$ has a single
minimal obstruction. Corank $2$ has $9$ minimal obstructions
but $8$ of them were extensions of the corank $1$ minimal
obstruction. Given the difficulty of classifying monotone path graphs
in corank $3$ and above we have no good intuition into the
question of ``what are the  minimal obstructions?''. We are
specifically interested in hyperplane arrangements analogous to the
purple arrangements of Figure ~\ref{fig:unsolved-big-picture} which
are minimal obstructions to all-coherence, but extensions only of 
universally all-coherent hyperplane arrangements in corank $2$.
To help us, the
results of Chapter ~\ref{s:methods} are independent of corank and our
computational tools are available to generate examples. 

\section{Cyclic hyperplane arrangements?}

Since a classification is challenging in corank $3$ and above we can
direct our efforts at particularly nice families of pointed hyperplane
arrangements. Methods of single-element liftings and single-element extensions
have proven useful in the analysis of \emph{cyclic}
\emph{polytopes} ~\cite{Edelman00}, ~\cite{Athanasiadis00} and we have
a very similar story for cyclic hyperplane arrangements. We define 
the family of \emph{cyclic} hyperplane arrangements:
\[
\sA(n,d)=\bordermatrix{
& \ai{1} & \ai{2} & \cdots & \ai{n} \cr
& 1 & 1 &\cdots & 1 \cr
& t_1 & t_2 &\cdots & t_n \cr
& \vdots & \vdots & & \vdots \cr
& t_1^{d-1} & t_2^{d-1} & \cdots & t_n^{d-1}
},
\]
with $t_1 < t_2 < \ldots < t_n$. Based on our results, we claim
$(\sA(n,d),c)$ has is universally all-coherent when $n=d+1$ and $c$ is
a reorientation of the $k=1$ corank $1$ hyperplane arrangement and all
has incoherent $\f{}$-monotone paths for all other $c$. Further, we
claim that when $n-d\ge 2$ the cyclic hyperplane arrangement
$(\sA(n,d),c)$ has incoherent $\f{}$-monotone paths for any $\f{}$
realizing $(\sA(d+1,d),c)$ and for every $c$.

\begin{proposition} Given a pointed, cyclic hyperplane arrangement
$(\sA(n,d),c)$ with $d > 2$ and $\f{}$ realizing $c$, the monotone path graph 
\begin{itemize}
\item When $n-d=1$, the pointed hyperplane arrangement $(\sA(n,d),c)$ 
is universally all-coherent when $c$ is a reorientation of the
universally all-coherent corank 1 pointed hyperplane arrangement
(e.g., $c=--+-+\cdots-+$), and has incoherent $\f{}$-monotone paths
for all other $c$.

\item When $n-d\ge 2$, the pointed hyperplane arrangement $(\sA(n,d),c)$ has incoherent galleries for every $\f{}$.
\end{itemize}
\label{prop:unsolved-cyclic}
\end{proposition}

\begin{proof}
$\sA(n,d)$ is irreducible and when $n=d+1$ we compute the dual as
$\sAd=\left(\aid{1},\ldots,\aid{d+1}\right)$ using the Vandermonde
determinant, so
\[
\aid{i}=(-1)^{(i+1)}
\prod_{\substack{1 \le j < k \le d+1\\ j,k \ne i}}\left(t_k-t_j\right)/t_i.
\]
We know that $t_k-t_j>0$ for $1 \le j < k \le d+1$ so the sign of
$\aid{i}$ depends only on $i$. When $c=--+-+\cdots-+$,
we have $\aid{1}$ as negative dual vector, and all other
$\setof{\aid{i}}$ are positive dual vectors. The pointed hyperplane
configuration $(\sA,c)$ then realizes the unique universally
all-coherent cyclic hyperplane arrangement is that of Section
~\ref{ss:cr1-zonotopal}.

For all other corank 1 cyclic
hyperplane Section ~\ref{ss:cr1-non-zonotopal} proves that there is an
incoherent $\f{}$-monotone path for every $\f{}$ by giving a minimal
obstruction when $d=3$ and realizing $\sA(d+1,d)$ a
single-element lifting of $\sA(d,d-1)$ which has incoherent
$\f{}$-monotone paths for every $\f{}$, by Lemma ~\ref{l:incoherent-lifting}.

When $d>3$ and $n-d = 2$, the pointed cyclic hyperplane arrangements $\sA(n,d)$
have no parallelism in the dual and are single-element liftings of the
parallelism free classes in Table ~\ref{tab:cr2-extensions}. These minimal
obstructions arrangements all contain incoherent $\f{}$-monotone paths according to Theorem
~\ref{thm:crank2-main-theorem} so they form a minimal obstruction set
and any $\sA(d+2,d)$ with $d>2$ contains incoherent $\f{}$-monotone
paths by Lemma ~\ref{l:incoherent-lifting}. For $n-d \ge 2$ we know that
$\sA(n,d)$ is a single element extension of a corank $2$ cyclic
hyperplane arrangement and will have incoherent $\f{}$-monotone paths
for every $\f{}$ according to Lemma ~\ref{l:coherent-projection}. 
\end{proof}

Existence of incoherent $\f{}$-monotone paths nodes do not determine
the diameter $\gtwo{C(n,d),c}$ so the main question of Reiner and
Roichman remains unsolved for cyclic hyperplane arrangements. Cyclic
hyperplane arrangements do give insight into the diameter of
$\gtwo{\sA(n,d),c}$. The cyclic arrangement $\sA(8,4)$ is the first
realized example we know of, in which the diameter of $\gtwo{\sA,c}$ is
strictly greater than $\abs{L_2}$. Non-realized examples previously
demonstrated that the diameter is strictly greater than $\abs{L_2}$
for acyclically oriented matroids in rank $4$
~\cite[\S 3]{Richter_Gebert93}, ~\cite[p.2785-2786]{Reiner12}. In
Example ~\ref{ex:unsolved-dbound} we 
improve this result by demonstrating that the cyclic hyperplane
arrangements, $\sA(8,4)$ realize an example in which the diameter bound strictly
greater than $\abs{L_2}$. We feel our methods are only scratching the
surface of this rich family and it is worth studying in further
detail.

\begin{example} The cyclic hyperplane arrangement $\sA(8,4)$ has $8$ vectors
  in $\R{4}$ and the chamber $c=(-)^4(+)^4$ specifies an
  acyclic orientation. Making use of
  our computer we can check that 
\[
\begin{split}
  \abs{\Gamma(\sA,c)}&=4896,\\
  \diam{\gtwo{\sA,c}}&=30,\quad \text{ and}  \\
  \abs{L_2}&=28. \\
\end{split}
\]
Yet the shortest path between $\gal = 34127856$ and $-\gal=65872143$
has length $30$ and crosses the $L_2$ intersection $X_{1,2}$ multiple
times. 

In fact of the $128$ chambers of $\sA$ the diameter equals $\abs{L_2}$
for $96$ chambers and is strictly greater for remaining $32$
chambers. At this time we know of nothing special about the $32$
chambers of $\sA$ which make the diameter greater than $\abs{L_2}=28$.
\label{ex:unsolved-dbound}
\end{example}

We know that the diameter of $\gtwo{\sA(n,3)}$ equals $\abs{L_2}$ and
also that the diameter of $\gtwo{\sA(d+1,d)}$ equals $\abs{L_2}$, yet
by $d=4$, and 
$n=8$, we have a cyclic arrangement $\sA(8,4)$, whose diameter is
greater than $\abs{L_2}$. We believe that Example
~\ref{ex:unsolved-dbound} is the smallest realized case in which
diameter exceeds $\abs{L_2}$ but we do not yet understand why, nor are
our methods powerful enough to guess when the diameter will exceed
$\abs{L_2}$. 

\section{Computational Results and Applications to other Fields?}

Our computational tools have provided new results for the number of
reduced words in reflection arrangements. A simple modification of
Dijkstra's algorithm provides a powerful tool to count paths on a
pointed zonotope ~\cite{Dijkstra59}. We used this method to compute
the number of galleries in pointed hyperplane arrangements without
producing the monotone path graph. This allowed us to compute the
number of reduced words for some reflection arrangements which were
previously unknown, as well as provide confirmation of results
obtained in other ways. We highlight these computations in Table
~\ref{tab:unsolved-summary}.

The simplex method ~\cite{Murty83} of optimization is deeply concerned with
understanding $\f{}$-monotone paths on \emph{polytopes}
~\cite{Avis08} and its worst case run time proportional to the
diameter $1$-skeleton of polytopes. 

\begin{conjecture}[Hirsch] For $n>d>2$ the diameter of a $polytope$ in
$\R{d}$ with $m$ facets is no larger than $m-d$.
\end{conjecture}

Our results of Theorem
~\ref{thm:cr1-diameter} and Example ~\ref{ex:unsolved-dbound} fit into
this context as geometrically motivated graphs with unexpected
diameters. Diameter is known for zonotopes is known and well
understood, however the diameter of monotone path graphs remains an
interesting question.

\input{table-results-summary}
\newpage
\begin{sidewaysfigure}
\vspace{6in}
\includegraphics[width=.9\linewidth]{figures/unsolved.pdf}
\caption{Summary of results and unsolved questions.}
\label{fig:unsolved-big-picture}
\end{sidewaysfigure}
